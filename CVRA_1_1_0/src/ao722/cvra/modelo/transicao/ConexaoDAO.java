package ao722.cvra.modelo.transicao;

import ao722.cvra.contrato.fornecedor.IntegracaoWebPeer;
import ao722.cvra.modelo.estado.ConexaoVO;
import ao722.cvra.modelo.estado.ConfiguracaoVO;

public class ConexaoDAO {

	// Propriedades
	private IntegracaoWebPeer integracaoWebPeer;

	// Encapsulamento
	public IntegracaoWebPeer getIntegracaoWebPeer() {
		return integracaoWebPeer;
	}
	public void setIntegracaoWebPeer(IntegracaoWebPeer integracaoWebPeer) {
		this.integracaoWebPeer = integracaoWebPeer;
	}

	// Construtores
	public ConexaoDAO() {
		setIntegracaoWebPeer( new IntegracaoWebPeer());
	}

	// Comportamentos
	public void acessarURL(){
		ConexaoVO.urlConnection = getIntegracaoWebPeer().acessarURL( ConexaoVO.url); //( java.net.HttpURLConnection)ConexaoVO.url.openConnection();
		System.out.println( "ConexaoVO.urlConnection.toString(): "+ ConexaoVO.urlConnection.toString());
	}

	public void criarLink(){
		ConexaoVO.url = getIntegracaoWebPeer().criarLink( ConfiguracaoVO.logURL); //new java.net.URL( ConfiguracaoVO.logURL);
		System.out.println( "ConexaoVO.url.toString(): "+ ConexaoVO.url.toString());
	}

	public void receptarBytes(){
		ConexaoVO.armazem = getIntegracaoWebPeer().receptarBytes( ConexaoVO.urlConnection);
		System.out.println( "ConexaoVO.armazem.length: "+ ConexaoVO.armazem);
	}

//	public void imprimirLog(){
//		getIntegracaoWebPeer().imprimirLog( ConexaoVO.leitor);
//	}

	public void encerrarAcesso(){
		getIntegracaoWebPeer().encerrarAcesso( ConexaoVO.armazem, ConexaoVO.urlConnection, ConexaoVO.canal);
	}

	public void criarCanal(){
		ConexaoVO.canal = getIntegracaoWebPeer().criarCanal( ConexaoVO.urlConnection);
		System.out.println( "ConexaoVO.canal: "+ ConexaoVO.canal);
	}

	public void fecharCanal(){
		ConexaoVO.canal = null;
		System.out.println( "ConexaoVO.canal: "+ ConexaoVO.canal);
	}

}

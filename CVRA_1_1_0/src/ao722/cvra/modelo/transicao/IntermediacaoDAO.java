package ao722.cvra.modelo.transicao;

import javax.telephony.media.MediaTerminalConnection;

import ao722.cvra.contrato.fornecedor.IntegracaoTelefonicaAESPeer;
import ao722.cvra.modelo.estado.ConfiguracaoVO;
import ao722.cvra.modelo.estado.ConstituicaoVO;
import ao722.cvra.modelo.estado.ExecucaoVO;
import ao722.cvra.modelo.estado.IntegracaoTelefonicaVO;
import ao722.cvra.modelo.estado.IntermediacaoVO;


public class IntermediacaoDAO {

	// Propriedades
	private IntegracaoTelefonicaAESPeer integracaoTelefonicaAESPeer;

	// Metodos
	public IntegracaoTelefonicaAESPeer getIntegracaoTelefonicaAESPeer() {
		return integracaoTelefonicaAESPeer;
	}
	public void setIntegracaoTelefonicaAESPeer( IntegracaoTelefonicaAESPeer integracaoTelefonicaAESPeer) {
		this.integracaoTelefonicaAESPeer = integracaoTelefonicaAESPeer;
	}

	public void desprover(){
		IntegracaoTelefonicaVO.aparelhagem = null;
		IntegracaoTelefonicaVO.chamador = null;
		IntegracaoTelefonicaVO.integrador = null;
		IntegracaoTelefonicaVO.catalogo = null;
		IntegracaoTelefonicaVO.peer = null;
	}

	public void definirDTMF(){
		IntegracaoTelefonicaVO.dTMF = ExecucaoVO.digito;
		System.out.println( "IntegracaoTelefonicaVO.dtmf: "+ IntegracaoTelefonicaVO.dTMF);
	}

//	public void definirProcedimento(){
//		IntermediacaoVO.procedimento = IntermediacaoVO.processo[ IntermediacaoVO.numeroProcedimento];
//		System.out.println( "IntermediacaoVO.procedimento: "+ IntermediacaoVO.procedimento);
//	}

//	public void definirPassos(){
//		IntermediacaoVO.passos = IntermediacaoVO.processo.length;
//		System.out.println( "IntermediacaoVO.passos: "+ IntermediacaoVO.passos);
//	}

//	public void definirProcesso(){
//		IntermediacaoVO.processo = ExecucaoVO.linha.split("\\.");
//		System.out.println( "IntermediacaoVO.processo: "+ IntermediacaoVO.processo);
//	}

	public void definirLigado(){
		IntermediacaoVO.ligado = IntegracaoTelefonicaVO.ligacao[ IntegracaoTelefonicaVO.numeroLigado];
		System.out.println( "IntermediacaoVO.ligado: "+ IntermediacaoVO.ligado);
	}
	
	public void definirLigador(){
		IntegracaoTelefonicaVO.ligador = IntegracaoTelefonicaVO.ligacao[ IntegracaoTelefonicaVO.numeroLigado];
		System.out.println( "IntegracaoTelefonicaVO.ligador: "+ IntegracaoTelefonicaVO.ligador);
	}

	public void definirLigacoes(){
		IntermediacaoVO.ligacoes = IntegracaoTelefonicaVO.ligacao.length;
		System.out.println( "IntermediacaoVO.ligacoes: "+ IntermediacaoVO.ligacoes);
	}

	public void definirAparelho(){
		IntegracaoTelefonicaVO.origem = getIntegracaoTelefonicaAESPeer().definirAparelho( IntegracaoTelefonicaVO.integrador, ConfiguracaoVO.extensao);
		System.out.println( "IntegracaoTelefonicaVO.origem.getName(): "+ IntegracaoTelefonicaVO.origem.getName());
	}

	public void escolherAparelho(){
		IntegracaoTelefonicaVO.origem = IntegracaoTelefonicaVO.aparelhagem[ IntermediacaoVO.numeroAparelho];
		System.out.println( "IntegracaoTelefonicaVO.origem.getName(): "+ IntegracaoTelefonicaVO.origem.getName());
	}
	
//	public void definirAparelhos(){
//		IntermediacaoVO.aparelhos = IntegracaoTelefonicaVO.aparelhagem.length;
//		System.out.println( "IntermediacaoVO.aparelhos: "+ IntermediacaoVO.aparelhos);
//	}

	public void definirExpressaoAcesso(){
		IntegracaoTelefonicaVO.expressaoIntegrador = IntermediacaoVO.nomeServico.concat(";passwd=").concat( ConstituicaoVO.password).concat( ";login=").concat( ConstituicaoVO.login);
		System.out.println( "IntegracaoTelefonicaVO.expressaoIntegrador: "+ IntegracaoTelefonicaVO.expressaoIntegrador);
	}

	public void definirServico(){
		System.out.println( "ConstituicaoVO.ctiLink: "+ ConstituicaoVO.ctiLink);
		IntermediacaoVO.nomeServico = IntegracaoTelefonicaVO.catalogo[ IntermediacaoVO.numeroServico];
		System.out.println( "IntermediacaoVO.nomeServico: "+ IntermediacaoVO.nomeServico);
	}

	public void resgatarServico(){
		System.out.println( "ConstituicaoVO.ctiLink: "+ ConstituicaoVO.ctiLink);
		IntermediacaoVO.nomeServico = ConstituicaoVO.ctiLink;
		System.out.println( "IntermediacaoVO.nomeServico: "+ IntermediacaoVO.nomeServico);
	}

	public void definirServicos(){
		System.out.println( "IntermediacaoVO.servicos: "+ IntermediacaoVO.servicos);
		IntermediacaoVO.servicos = IntegracaoTelefonicaVO.catalogo.length;
	}

	public void zerarServico(){
		IntermediacaoVO.numeroServico = 0;
		System.out.println( "IntermediacaoVO.numeroServico: "+ IntermediacaoVO.numeroServico );
	}

	public void saltarServico(){
		IntermediacaoVO.numeroServico = IntermediacaoVO.numeroServico+1;
		System.out.println( "IntermediacaoVO.numeroServico: "+ IntermediacaoVO.numeroServico );
	}

	public void zerarAparelho(){
		IntermediacaoVO.numeroAparelho = 0;
		System.out.println( "IntermediacaoVO.numeroAparelho: "+ IntermediacaoVO.numeroAparelho);
	}

	public void saltarAparelho(){
		IntermediacaoVO.numeroAparelho = IntermediacaoVO.numeroAparelho+1;
		System.out.println( "IntermediacaoVO.numeroAparelho: "+ IntermediacaoVO.numeroAparelho);
	}

	public void zerarLigado(){
		IntegracaoTelefonicaVO.numeroLigado = 0;	
		System.out.println( "IntegracaoTelefonicaVO.numeroLigado: "+ IntegracaoTelefonicaVO.numeroLigado);
	}
	
	public void saltarLigado(){
		IntegracaoTelefonicaVO.numeroLigado = IntegracaoTelefonicaVO.numeroLigado+1;
		System.out.println( "IntegracaoTelefonicaVO.numeroLigado: "+ IntegracaoTelefonicaVO.numeroLigado);
	}

	public void zerarProcedimento(){
		IntermediacaoVO.numeroProcedimento = 0;
		System.out.println( "IntermediacaoVO.numeroProcedimento: "+ IntermediacaoVO.numeroProcedimento);
	}
	
//	public void saltarProcedimento(){
//		IntermediacaoVO.numeroProcedimento = IntermediacaoVO.numeroProcedimento+1;
//		System.out.println( "IntermediacaoVO.numeroProcedimento: "+ IntermediacaoVO.numeroProcedimento);
//	}

	public void zerarAlerta(){
		IntermediacaoVO.numeroAlerta = 0;
		System.out.println( "IntermediacaoVO.numeroAlerta: "+ IntermediacaoVO.numeroAlerta);
	}
	
	public void saltarAlerta(){
		IntermediacaoVO.numeroAlerta = IntermediacaoVO.numeroAlerta+1;
		System.out.println( "IntermediacaoVO.numeroAlerta: "+ IntermediacaoVO.numeroAlerta);
	}

	public void aguardarToque(){

		try {
			Thread.sleep( ConfiguracaoVO.intervalo);
		} 
		catch (InterruptedException e) {
			System.err.println( "IntermediacaoDAO.aguardarToque(): "+ e);
		}

	}

	public void definirChamador(){
		IntegracaoTelefonicaVO.chamador = getIntegracaoTelefonicaAESPeer().definirChamador( IntegracaoTelefonicaVO.integrador, ConfiguracaoVO.extensao);
		System.out.println( "IntegracaoTelefonicaVO.chamador: "+ IntegracaoTelefonicaVO.chamador);
	}

	public void definirAparelhagem(){
		IntegracaoTelefonicaVO.aparelhagem = getIntegracaoTelefonicaAESPeer().definirAparelhagem( IntegracaoTelefonicaVO.integrador);
		System.out.println( "IntegracaoTelefonicaVO.aparelhagem: "+ IntegracaoTelefonicaVO.aparelhagem );
	}

	public void definirLigacao(){
		IntegracaoTelefonicaVO.ligacao = getIntegracaoTelefonicaAESPeer().definirLigacao( IntegracaoTelefonicaVO.chamada);
		System.out.println("IntermediacaoDAO.definirLigacao(): "+ IntegracaoTelefonicaVO.ligacao);
	}

	public void definirVinculacao(){
		IntegracaoTelefonicaVO.vinculacao = (javax.telephony.TerminalConnection[])getIntegracaoTelefonicaAESPeer().definirVinculacao( IntegracaoTelefonicaVO.ligador);
		System.out.println( "IntegracaoTelefonicaVO.vinculacao: "+ IntegracaoTelefonicaVO.vinculacao);
	}

	public void definirMidia(){
		IntegracaoTelefonicaVO.mediaTerminalConnection = (MediaTerminalConnection)getIntegracaoTelefonicaAESPeer().definirMidia( IntegracaoTelefonicaVO.ligador);
		System.out.println( "IntegracaoTelefonicaVO.mediaTerminalConnection: "+ IntegracaoTelefonicaVO.mediaTerminalConnection);
	}

	public void desligar(){
		System.out.println( getIntegracaoTelefonicaAESPeer());
		System.out.println( IntegracaoTelefonicaVO.chamada);
		getIntegracaoTelefonicaAESPeer().desligar( IntegracaoTelefonicaVO.chamada);
		System.out.println( "Chamada encerrada");
	}

	public void definirInstancia(){
		IntegracaoTelefonicaVO.peer = getIntegracaoTelefonicaAESPeer().definirInstancia( ConstituicaoVO.peerName);		
		System.out.println( "IntegracaoTelefonicaVO.peer: "+ IntegracaoTelefonicaVO.peer);
	}

	public void definirCatalogo(){
		IntegracaoTelefonicaVO.catalogo = getIntegracaoTelefonicaAESPeer().definirCatalogo( IntegracaoTelefonicaVO.peer);
		System.out.println( "IntegracaoTelefonicaVO.catalogo: "+ IntegracaoTelefonicaVO.catalogo);
	}

	public void definirIntegrador(){
		IntegracaoTelefonicaVO.integrador = getIntegracaoTelefonicaAESPeer().definirIntegrador( IntegracaoTelefonicaVO.peer, IntegracaoTelefonicaVO.expressaoIntegrador);
		System.out.println( "IntegracaoTelefonicaVO.integrador: "+ IntegracaoTelefonicaVO.integrador);
	}

	public void gerarTom(){
		getIntegracaoTelefonicaAESPeer().gerarTom( IntegracaoTelefonicaVO.mediaTerminalConnection, IntegracaoTelefonicaVO.dTMF);
	}

	public void definirNumeroOrigem(){
		IntegracaoTelefonicaVO.numeroOrigem = getIntegracaoTelefonicaAESPeer().definirNumeroVinculado( IntegracaoTelefonicaVO.ligacao, IntegracaoTelefonicaVO.numeroLigado);
		System.out.println( "IntegracaoTelefonicaVO.numeroOrigem: "+ IntegracaoTelefonicaVO.numeroOrigem);
	}

	public void definirNumeroDestino(){
		IntegracaoTelefonicaVO.numeroDestino = getIntegracaoTelefonicaAESPeer().definirNumeroVinculado( IntegracaoTelefonicaVO.ligacao, IntegracaoTelefonicaVO.numeroLigado);
		System.out.println( "IntegracaoTelefonicaVO.numeroDestino: "+ IntegracaoTelefonicaVO.numeroDestino);
	}

	public void definirChamada(){
		IntegracaoTelefonicaVO.chamada = getIntegracaoTelefonicaAESPeer().definirChamada( IntegracaoTelefonicaVO.integrador);
		System.out.println( "IntegracaoTelefonicaVO.call: "+ IntegracaoTelefonicaVO.chamada);
	}

	public void ligar(){
		System.out.println( IntegracaoTelefonicaVO.chamada +", "+ IntegracaoTelefonicaVO.chamador +", "+ IntegracaoTelefonicaVO.origem +", "+ ConfiguracaoVO.destino);
		getIntegracaoTelefonicaAESPeer().ligar( IntegracaoTelefonicaVO.chamada, IntegracaoTelefonicaVO.chamador, IntegracaoTelefonicaVO.origem, ConfiguracaoVO.destino);
		System.out.println( "IntegracaoTelefonicaVO.call: "+ IntegracaoTelefonicaVO.chamada);
	}

	public void ligarExterno(){
		System.out.println( IntegracaoTelefonicaVO.chamada +", "+ IntegracaoTelefonicaVO.chamador +", "+ IntegracaoTelefonicaVO.origem +", "+ ConfiguracaoVO.destino +"#"+ ConfiguracaoVO.aCode);
		getIntegracaoTelefonicaAESPeer().ligar( IntegracaoTelefonicaVO.chamada, IntegracaoTelefonicaVO.chamador, IntegracaoTelefonicaVO.origem, ConfiguracaoVO.destino +"#"+ ConfiguracaoVO.aCode);
		System.out.println( "IntegracaoTelefonicaVO.call: "+ IntegracaoTelefonicaVO.chamada);
	}

}

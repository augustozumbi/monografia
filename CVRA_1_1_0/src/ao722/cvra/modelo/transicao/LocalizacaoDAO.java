package ao722.cvra.modelo.transicao;

import ao722.cvra.modelo.estado.ConstituicaoVO;
import ao722.cvra.modelo.estado.LocalizacaoVO;
import ao722.cvra.modelo.estado.MensagemVO;
import ao722.cvra.modelo.estado.NavegacaoVO;

public class LocalizacaoDAO {

	public LocalizacaoDAO() {
		// TODO Auto-generated constructor stub
	}
	
	public void definirEntrou(){
		LocalizacaoVO.trajetoria = ConstituicaoVO.entrada;
	}

	public void definirNavegou(){
		LocalizacaoVO.trajetoria = NavegacaoVO.navegacao;
	}
	
	public void definirTransferido(){
		LocalizacaoVO.trajetoria = NavegacaoVO.navegacao.concat(".").concat( ConstituicaoVO.transferido);
	}

	public void definirExcedeu(){
		LocalizacaoVO.trajetoria = LocalizacaoVO.trajetoria.concat(".").concat( ConstituicaoVO.excedido);
	}

	public void definirAbandonou(){
		LocalizacaoVO.trajetoria = LocalizacaoVO.trajetoria.concat(".").concat( ConstituicaoVO.abandonou);
	}
	
	public void definirDesconectado(){
		LocalizacaoVO.trajetoria = NavegacaoVO.navegacao.concat(".").concat( ConstituicaoVO.desconectado);
	}

	public void definirAssuntoDesconectado(){
		MensagemVO.assunto = "[".concat( ConstituicaoVO.desconexao).concat("] ").concat( LocalizacaoVO.trajetoria);
	}

}

package ao722.cvra.modelo.transicao;

import ao722.cvra.contrato.fornecedor.ControleMaterialPeer;
import ao722.cvra.modelo.estado.ConfiguracaoVO;

public class ConfiguracaoDAO {
	
	// Atributos
	private ControleMaterialPeer controleMaterialPeer;
	
	// Encapsulamento
	public ControleMaterialPeer getControleMaterialPeer() {
		return controleMaterialPeer;
	}
	public void setControleMaterialPeer(ControleMaterialPeer controleMaterialPeer) {
		this.controleMaterialPeer = controleMaterialPeer;
	}

	// Construtores
	public ConfiguracaoDAO() {	
		setControleMaterialPeer( new ControleMaterialPeer());
	}

	// Comportamentos
	public void definirConfiguracao(){
		ConfiguracaoVO.local = this.getClass().getClassLoader().getResource(".").getPath().replace( "%20", " ").concat("../");
		System.out.println( "ConfiguracaoVO.local: ".concat( ConfiguracaoVO.local));
		ConfiguracaoVO.padrao = "CVRA.properties";
		ConfiguracaoVO.quadrado = Integer.parseInt( getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "quadrado"));
		ConfiguracaoVO.extensaoSumario = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "extensaoSumario");
		ConfiguracaoVO.extensaoProduto = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "extensaoProduto");
		ConfiguracaoVO.extensaoMassa = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "extensaoMassa");
		ConfiguracaoVO.extensaoHistorico = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "extensaoHistorico");
		ConfiguracaoVO.extensaoScript = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "extensaoScript");
		ConfiguracaoVO.extensaoRelatorio = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "extensaoRelatorio");
		ConfiguracaoVO.extensaoExportacao = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "extensaoExportacao");
		ConfiguracaoVO.dimensaoLimiteLog = Integer.parseInt( getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "dimensaoLimiteLog"));

	}

//	public void resgatarPadroes(){
//		ConfiguracaoVO.script = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "script");
//		ConfiguracaoVO.limiteTentativas = Integer.parseInt( getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "limiteTentativas"));
//		ConfiguracaoVO.tempoEntreDigitos = Integer.parseInt( getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "tempoEntreDigitos"));
//		ConfiguracaoVO.caminho = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "caminho");
//		ConfiguracaoVO.intervalo = Integer.parseInt( getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "intervalo"));
//		ConfiguracaoVO.peerName = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "peerName");
//		ConfiguracaoVO.ctiLink = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "ctiLink");
//		ConfiguracaoVO.password = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "password");
//		ConfiguracaoVO.extensao = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "extensao");
//		ConfiguracaoVO.destino = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "destino");
//		ConfiguracaoVO.logURL = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "logURL");
//		ConfiguracaoVO.sumario = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "sumario");
//		ConfiguracaoVO.login = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "login");
//		ConfiguracaoVO.fimValor = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "fimValor");
//		ConfiguracaoVO.fimMenu = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "fimMenu");
//		ConfiguracaoVO.chaveAlocacao = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "chaveAlocacao");
//		ConfiguracaoVO.chaveDado = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "chaveDado");
//		ConfiguracaoVO.chaveMenu = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "chaveMenu");
//		ConfiguracaoVO.chaveOmissao = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "chaveOmissao");
//		ConfiguracaoVO.chaveEngano = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "chaveEngano");
//		ConfiguracaoVO.padraoDataHora = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "padraoDataHora");
//		ConfiguracaoVO.chaveTransferencia = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "chaveTransferencia");
//		ConfiguracaoVO.separadorData = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao), "separadorData");
//	}
	
//	public void contingenciarPadroes(){
//		ConfiguracaoVO.limiteTentativas = 3;
//		ConfiguracaoVO.quadrado = 18;
//		ConfiguracaoVO.intervalo = 8000;
//		ConfiguracaoVO.tempoEntreDigitos = 1000;
//		ConfiguracaoVO.peerName = "com.avaya.jtapi.tsapi.TsapiPeer";
//		ConfiguracaoVO.fimValor = "]";
//		ConfiguracaoVO.fimMenu = ":";
//		ConfiguracaoVO.chaveAlocacao = "Storing";
//		ConfiguracaoVO.chaveDado = ":utterance  as [";
//		ConfiguracaoVO.chaveMenu = "noinputcount to complex:";
//		ConfiguracaoVO.chaveOmissao = ":noinputcount  as [";
//		ConfiguracaoVO.chaveEngano = ":nomatchcount  as [";
//		ConfiguracaoVO.padraoDataHora = "dd/MM/yyyy HH:mm:ss";
//		ConfiguracaoVO.chaveTransferencia = "transferOnRing";
//		ConfiguracaoVO.separadorData = "/";
//		ConfiguracaoVO.caminho = "../";
//		ConfiguracaoVO.script = "";
//		ConfiguracaoVO.destino = "";
//		ConfiguracaoVO.logURL = "";
//		ConfiguracaoVO.ctiLink = "";
//		ConfiguracaoVO.sumario = "";
//		ConfiguracaoVO.login = "";
//		ConfiguracaoVO.password = "";
//		ConfiguracaoVO.extensao = "";
//	}

	public void configurarSimulacao(){
		ConfiguracaoVO.script = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "script");
		ConfiguracaoVO.limiteTentativas = Integer.parseInt( getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "limiteTentativas"));
		ConfiguracaoVO.intervalo = Integer.parseInt( getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "intervalo"));
	}
	
	public void configurarCertificacao(){
		ConfiguracaoVO.extensao = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "extensao");
		ConfiguracaoVO.destino = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "destino");
		ConfiguracaoVO.logURL = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "logURL");
		ConfiguracaoVO.sumario = ConfiguracaoVO.logURL.split("/")[3].concat( ConfiguracaoVO.extensaoSumario);
		ConfiguracaoVO.tempoEntreDigitos = Integer.parseInt(getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "tempoEntreDigitos"));
		ConfiguracaoVO.aCode = getControleMaterialPeer().obterConfiguracao( ConfiguracaoVO.padrao, "aCode");
	}

}

package ao722.cvra.modelo.transicao;

import java.util.HashMap;

import ao722.cvra.contrato.fornecedor.ControleMaterialPeer;
import ao722.cvra.modelo.estado.ConfiguracaoVO;
import ao722.cvra.modelo.estado.ConstituicaoVO;
import ao722.cvra.modelo.estado.ReportagemVO;
import ao722.cvra.modelo.estado.ValidacaoVO;

public class ReportagemDAO {

	// Atributos
	private ControleMaterialPeer controleMaterialPeer;

	// Contrutores
	public ReportagemDAO() {
		setControleMaterialPeer( new ControleMaterialPeer());
		constituirCampos();
		// TODO Auto-generated constructor stub
	}
	// Encapsulamento
	public ControleMaterialPeer getControleMaterialPeer() {
		return controleMaterialPeer;
	}
	public void setControleMaterialPeer(ControleMaterialPeer controleMaterialPeer) {
		this.controleMaterialPeer = controleMaterialPeer;
	}

	public void constituirCampos(){
		ReportagemVO.campoDataHoraInicial = ConstituicaoVO.campos.split(";")[0];
		ReportagemVO.campoDataHoraFinal = ConstituicaoVO.campos.split(";")[1];
		ReportagemVO.campoDuracao = ConstituicaoVO.campos.split(";")[2];
		ReportagemVO.campoIdentificacaoAutomatica = ConstituicaoVO.campos.split(";")[3];
		ReportagemVO.campoVDNURA = ConstituicaoVO.campos.split(";")[4];
		ReportagemVO.campoCanal = ConstituicaoVO.campos.split(";")[5];
		ReportagemVO.campoSessaoAppServer = ConstituicaoVO.campos.split(";")[6];
		ReportagemVO.campoSessaoMPP = ConstituicaoVO.campos.split(";")[7];
		ReportagemVO.campoSinistro = ConstituicaoVO.campos.split(";")[8];
		ReportagemVO.campoTrajetoria = ConstituicaoVO.campos.split(";")[9];
		ReportagemVO.campoDesfecho = ConstituicaoVO.campos.split(";")[10];
		ReportagemVO.campoRelato = ConstituicaoVO.campos.split(";")[11];
		ReportagemVO.campoDelato = ConstituicaoVO.campos.split(";")[12];
		ReportagemVO.campoResumo = ConstituicaoVO.campos.split(";")[13];
		ReportagemVO.campoPlataforma = ConstituicaoVO.campos.split(";")[14];
		ReportagemVO.campoDerivacao = ConstituicaoVO.campos.split(";")[15];
	}

	public void complementarRelato(){
		// ReportagemVO.relato = ReportagemVO.relato +","+ ReportagemVO.trajetoria +"="+ ValidacaoVO.menu;
		ReportagemVO.relato = ReportagemVO.relato +"<tr><th>"+ ReportagemVO.trajetoria +"</th><td>"+ ValidacaoVO.menu +"</td></tr>";
		ReportagemVO.relatoMap.put( ReportagemVO.trajetoria, ValidacaoVO.menu);
		System.out.println( "ReportagemVO.relato (complementar): "+ ReportagemVO.relato);
	}
																																																																																																																																																																																																							
	public void iniciarRelatoEntradaValida(){
		// ReportagemVO.relato = "ENTRADA="+ ValidacaoVO.menu;
		// ReportagemVO.relato = "ENTRADA="+ ValidacaoVO.menu;
		ReportagemVO.relato = "<tr><th>ENTRADA</th><td>"+ ValidacaoVO.menu +"</td></tr>";
		ReportagemVO.relatoMap.put("ENTRADA", ValidacaoVO.menu);
		System.out.println( "ReportagemVO.relato (entrada valida): "+ ReportagemVO.relato);
	}

	public void iniciarRelatoEntradaInvalida(){
		// ReportagemVO.relato = "ENTRADA="+ ValidacaoVO.menu;
		ReportagemVO.relato = "<tr><th>ENTRADA</th><td>"+ ValidacaoVO.menu +"</td></tr>";
		ReportagemVO.relatoMap.put("ENTRADA", ValidacaoVO.menu);
		System.out.println( "ReportagemVO.relato (entrada valida): "+ ReportagemVO.relato);
	}

	public void iniciarRelatoEntradaAbandono(){
		// ReportagemVO.relato = "ENTRADA=ABANDONO";
		ReportagemVO.relato = "<tr><th>ENTRADA</th><td>ABANDONO</td></tr>";
		ReportagemVO.relatoMap.put("ENTRADA", "ABANDONO");
		System.out.println( "ReportagemVO.relatorio (entrada abandono): "+ ReportagemVO.relatorio);
	}

	public void iniciarRelatoEntradaQuebra(){
		// ReportagemVO.relato = "ENTRADA=QUEBRA";
		ReportagemVO.relato = "<tr><th>ENTRADA</th></td>QUEBRA</td></tr>";
		ReportagemVO.relatoMap.put("ENTRADA", "QUEBRA");
		System.out.println( "ReportagemVO.relatorio (entrada quebra): "+ ReportagemVO.relatorio);
	}

	public void complementarRelatoEquivoco(){
		// ReportagemVO.relato = ReportagemVO.relato +","+ ReportagemVO.trajetoria +"=EQUIVOCO";
		ReportagemVO.relato = ReportagemVO.relato +"<tr><th>"+ ReportagemVO.trajetoria +"</th><td>EQUIVOCO</td></tr>";
		ReportagemVO.relatoMap.put( ReportagemVO.trajetoria, "EQUIVOCO");
		System.out.println( "ReportagemVO.relato (complementar): "+ ReportagemVO.relato);		
	}

	public void complementarRelatoTransferido(){
		// ReportagemVO.relato = ReportagemVO.relato +","+ ReportagemVO.trajetoria +"=TRANSFERIDO";
		ReportagemVO.relato = ReportagemVO.relato +"<tr><th>"+ ReportagemVO.trajetoria +"</th><td>TRANSFERIDO</td></tr>";
		ReportagemVO.relatoMap.put( ReportagemVO.trajetoria, "TRANSFERIDO");
		System.out.println( "ReportagemVO.relato (transferido): "+ ReportagemVO.relato);		
	}

	public void complementarRelatoRetido(){
		// ReportagemVO.relato = ReportagemVO.relato +","+ ReportagemVO.trajetoria +"=RETIDO";
		ReportagemVO.relato = ReportagemVO.relato +"<tr><th>"+ ReportagemVO.trajetoria +"</th><td>RETIDO</td></tr>";
		ReportagemVO.relatoMap.put( ReportagemVO.trajetoria, "RETIDO");
		System.out.println( "ReportagemVO.relato (retido): "+ ReportagemVO.relato);		
	}

	public void complementarRelatoExcedeu(){
		// ReportagemVO.relato = ReportagemVO.relato +","+ ReportagemVO.trajetoria +"=EXCEDEU";
		ReportagemVO.relato = ReportagemVO.relato +"<tr><th>"+ ReportagemVO.trajetoria +"</th><td>EXCEDEU</td></tr>";
		ReportagemVO.relatoMap.put( ReportagemVO.trajetoria, "EXCEDEU");
		System.out.println( "ReportagemVO.relato (excedido): "+ ReportagemVO.relato);		
	}
	
	public void complementarRelatoAbandonou(){
		// ReportagemVO.relato = ReportagemVO.relato +","+ ReportagemVO.trajetoria +"=ABANDONOU";
		ReportagemVO.relato = ReportagemVO.relato +"<tr><th>"+ ReportagemVO.trajetoria +"</th><td>ABANDONOU</td></tr>";
		ReportagemVO.relatoMap.put( ReportagemVO.trajetoria, "ABANDONOU");
		System.out.println( "ReportagemVO.relato (abandonou): "+ ReportagemVO.relato);		
	}

	public void complementarRelatorioRelato(){
		//ReportagemVO.relatorio = ReportagemVO.relatorio +","+ ReportagemVO.campoRelato +"={"+ ReportagemVO.relato +"}";
		ReportagemVO.relatorio = ReportagemVO.relatorio +"<table border='1' width='100%'><tr><th>"+ ReportagemVO.campoRelato +"</th><td><table border='1' width='100%'>"+ ReportagemVO.relato +"</table></td></tr></table>";
		ReportagemVO.relatorioMap.put( ReportagemVO.campoRelato, ReportagemVO.relatoMap);
		System.out.println( "ReportagemVO.relatorio (relato): "+ ReportagemVO.relatorio);
	}

	public void complementarRelatorioDelacao(){
		// ReportagemVO.relatorio = ReportagemVO.relatorio +","+ ReportagemVO.campoDelato +"={"+ ReportagemVO.delato +"}";
		ReportagemVO.relatorio = ReportagemVO.relatorio +"<table border='1' width='100%'><tr><th>"+ ReportagemVO.campoDelato +"</th><td><table border='1' width='100%'><tr><td>"+ ReportagemVO.delato +"</td></tr></table></td></tr></table>";
		ReportagemVO.relatorioMap.put( ReportagemVO.campoDelato, ReportagemVO.delatoMap);
		System.out.println( "ReportagemVO.relatorio (excecao): "+ ReportagemVO.relatorio);		
	}

//	public void complementarRelatorioTrajetoria(){
//		ReportagemVO.relatorio = ReportagemVO.relatorio +","+ ReportagemVO.campoTrajetoria +"="+ ReportagemVO.trajetoria +"";
//		System.out.println( "ReportagemVO.relatorio (abandonou): "+ ReportagemVO.relatorio);		
//	}

//	public void complementarRelatorioVDNTransferencia(){
//		ReportagemVO.relatorio = ReportagemVO.relatorio +","+ ReportagemVO.campoVDNTransferencia +"="+ ReportagemVO.vdnTransferencia +"";
//		System.out.println( "ReportagemVO.relatorio (VDN transferencia): "+ ReportagemVO.relatorio);		
//	}

//	public void complementarRelatorioBilheteSaida(){
//		ReportagemVO.relatorio = ReportagemVO.relatorio +","+ ReportagemVO.campoBilheteSaida +"="+ ReportagemVO.uUISaida +"";
//		System.out.println( "ReportagemVO.relatorio (bilhete saida): "+ ReportagemVO.relatorio);		
//	}

	public void complementarRelatorioBilheteEntrada(){
		// ReportagemVO.relatorio = ReportagemVO.relatorio +"<tr><th>"+ ReportagemVO.campoBilheteEntrada +"</th><td>"+ ReportagemVO.uUIEntrada +"</td></tr>";
		ReportagemVO.relatorio = ReportagemVO.relatorio +"<tr><th>"+ ReportagemVO.campoBilheteEntrada +"</th><td>"+ ReportagemVO.uUIEntrada +"</td></tr>";
		//ReportagemVO.relatorioMap.put( ReportagemVO.campoBilheteEntrada, ReportagemVO.uUIEntrada);
		System.out.println( "ReportagemVO.relatorio (bilhete entrada): "+ ReportagemVO.relatorio);		
	}

	public void completarRelatorio(){
		// ReportagemVO.relatorio = ReportagemVO.relatorio +"</td></tr></table>\n";
		ReportagemVO.relatorio = ReportagemVO.relatorio +"</td></tr></table><br clear='all'>\n";
		System.out.println( "ReportagemVO.relatorio (completar): "+ ReportagemVO.relatorio +", ReportagemVO.relatorioMap: "+ ReportagemVO.relatorioMap +", produtoMap");		
	}

	public void zerarRelatorio(){
		ReportagemVO.relatorio = "";
		ReportagemVO.relatorioMap = null;
		System.out.println( "ReportagemVO.relatorio (zerar): "+ ReportagemVO.relatorio);		
	}

	public void zerarRelato(){
		ReportagemVO.relatoMap = null;
	}

	public void zerarDelato(){
		ReportagemVO.delatoMap = null;
	}

	public void zerarPlataforma(){
		ReportagemVO.plataformaMap = null;
	}

	public void zerarResumo(){
		ReportagemVO.resumoMap = null;
	}

	public void zerarDerivacao(){
		ReportagemVO.derivacaoMap = null;
	}

	public void iniciarRelatorio(){
		// ReportagemVO.relatorio = ValidacaoVO.linha +"={"+ ReportagemVO.resumo + ReportagemVO.campoTrajetoria +"="+ ReportagemVO.trajetoria +","+ ReportagemVO.desfecho +"},"+ ReportagemVO.plataforma ;
		ReportagemVO.relatorio = "<table border='1'><tr><th>"+ ValidacaoVO.linha +"</th><td>"+ ReportagemVO.plataforma + ReportagemVO.resumo +"<tr><th>"+ ReportagemVO.campoTrajetoria +"</th><td>"+ ReportagemVO.trajetoria +"</td></tr><tr>"+ ReportagemVO.desfecho +"</tr></table></td></tr></table>";
		ReportagemVO.relatorioMap.put( ReportagemVO.campoResumo, ReportagemVO.resumoMap);
		ReportagemVO.relatorioMap.put( ReportagemVO.campoPlataforma, ReportagemVO.plataformaMap);
		System.out.println( "ReportagemVO.relatorio (iniciar): "+ ReportagemVO.relatorio);		 
	}

//	public void complementarRelatorioDesfecho(){
//		ReportagemVO.relatorio = ReportagemVO.relatorio +","+ ReportagemVO.desfecho;
//		System.out.println( "ReportagemVO.relatorio (abandonou): "+ ReportagemVO.relatorio);		
//	}

	public void complementarRelatorioExcecao(){
		// ReportagemVO.relatorio = ReportagemVO.relatorio +","+ ReportagemVO.excecao;
		ReportagemVO.relatorio = ReportagemVO.relatorio +"<tr>"+ ReportagemVO.excecao +"</tr>";
		System.out.println( "ReportagemVO.relatorio (excecao): "+ ReportagemVO.relatorio);		
	}

	public void complementarRelatorioDerivacao(){
		ReportagemVO.relatorio += ReportagemVO.derivacao +"</table></td></tr></table>";
		ReportagemVO.relatorioMap.put( ReportagemVO.campoDerivacao, ReportagemVO.derivacaoMap);
		System.out.println( "ReportagemVO.relatorio complementar): "+ ReportagemVO.relatorio);
	}

	public void relatarExcecao(){
		// ReportagemVO.excecao = ReportagemVO.campoSinistro +"="+ ReportagemVO.mensagemExcecao +""; 
		ReportagemVO.excecao = "<th>"+ ReportagemVO.campoSinistro +"</th><td>"+ ReportagemVO.mensagemExcecao +"</td>"; 
		System.out.println( "ReportagemVO.excecao: "+ ReportagemVO.excecao);		 
	}

	public void concatenarRelato(){
		ReportagemVO.relato = ReportagemVO.relato + ValidacaoVO.digito;
		ReportagemVO.relatoMap.put( ReportagemVO.trajetoria, ReportagemVO.relatoMap.get(ReportagemVO.trajetoria).concat( ValidacaoVO.digito));
		System.out.println( "ReportagemVO.relato (concatenar): "+ ReportagemVO.relato);		
	}

	public void iniciarDelatoExcecao(){
		// ReportagemVO.delato = ReportagemVO.tipoExecao +"={";
		ReportagemVO.delato = "<table border='1' width='100%'><tr><th>"+ ReportagemVO.tipoExecao +"</th><td><table border='1' width='100%'><tr>";
		ReportagemVO.delatoMap.put( ReportagemVO.tipoExecao, null); 
		System.out.println( "ReportagemVO.delato (iniciada delacao): "+ ReportagemVO.delato);
	}

	public void finalizarDelacao(){
		// ReportagemVO.delato = ReportagemVO.delato +"}";
		ReportagemVO.delato = ReportagemVO.delato +"</tr></table>";
		System.out.println( "ReportagemVO.delato (finalizada delacao): "+ ReportagemVO.delato);
	}

	public void iniciarDelatoMensagemExcecao(){
		// ReportagemVO.delato = ReportagemVO.delato +"mensagem="+ ReportagemVO.mensagemExcecao;
		ReportagemVO.delato = ReportagemVO.delato +"<th>mensagem</th><td>"+ ReportagemVO.mensagemExcecao +"</td>";
		HashMap<String,String> tmpHashMap = new HashMap<String,String>();
		tmpHashMap.put( "mensagem", ReportagemVO.mensagemExcecao);
		ReportagemVO.delatoMap.put( ReportagemVO.tipoExecao, tmpHashMap.toString());
		System.out.println( "ReportagemVO.delato (local excecao): "+ ReportagemVO.delato);
	}

	public void complementarDelatoLocalExcecao(){
		// ReportagemVO.delato = ReportagemVO.delato +",local="+ ReportagemVO.localExcecao;
		ReportagemVO.delato = ReportagemVO.delato +"<th>local</th><td>"+ ReportagemVO.localExcecao +"</td>";
		ReportagemVO.delatoMap.put( ReportagemVO.tipoExecao, new HashMap<String,String>().put( "local", ReportagemVO.localExcecao).toString());
		System.out.println( "ReportagemVO.delato (local excecao): "+ ReportagemVO.delato);
	}

	public void complementarDelatoMensagemExcecao(){
		// ReportagemVO.delato = ReportagemVO.delato +",mensagem="+ ReportagemVO.mensagemExcecao;
		ReportagemVO.delato = ReportagemVO.delato +"<th>mensagem</th><td>"+ ReportagemVO.mensagemExcecao +"</td>";
		HashMap<String,String> tmpHashMap = new HashMap<String,String>();
		tmpHashMap.put( "mensagem", ReportagemVO.mensagemExcecao);
		ReportagemVO.delatoMap.put( ReportagemVO.tipoExecao, tmpHashMap.toString());
		System.out.println( "ReportagemVO.delato (local excecao): "+ ReportagemVO.delato);
	}

	public void complementarDelatoLinhaExcecao(){
		// ReportagemVO.delato = ReportagemVO.delato +",linha="+ ReportagemVO.LinhaExcecao;
		ReportagemVO.delato = ReportagemVO.delato +"<th>linha</th><td>"+ ReportagemVO.linhaExcecao +"</td>";
		System.out.println( "ReportagemVO.delato (linha excecao): "+ ReportagemVO.delato);
	}

	public void relatarDataTeste(){
		// ReportagemVO.dataHoraInicio = ReportagemVO.campoDataHoraInicial +"="+ ValidacaoVO.momentoInicial +""; 
		ReportagemVO.dataHoraInicio = "<th>"+ ReportagemVO.campoDataHoraInicial +"</th><td>"+ ValidacaoVO.momentoInicial +"</td>"; 
		ReportagemVO.resumoMap.put(ReportagemVO.campoDataHoraInicial , ValidacaoVO.momentoInicial);
		System.out.println( "ReportagemVO.dataHoraInicio: "+ ReportagemVO.dataHoraInicio );
	}

	public void relatarTerminoTeste(){
		//ReportagemVO.dataHoraTermino = ReportagemVO.campoDataHoraFinal +"="+ ValidacaoVO.momentoFinal +""; 
		ReportagemVO.dataHoraTermino = "<th>"+ ReportagemVO.campoDataHoraFinal +"</th><td>"+ ValidacaoVO.momentoFinal +"</td>"; 
		ReportagemVO.resumoMap.put(ReportagemVO.campoDataHoraFinal , ValidacaoVO.momentoFinal);
		System.out.println( "ReportagemVO.periodo: "+ ReportagemVO.dataHoraTermino);
	}

	public void relatarRamalAlocado(){
		// ReportagemVO.ramalAlocado = ReportagemVO.campoCanal +"="+ ReportagemVO.canal +"";
		ReportagemVO.ramalAlocado = "<th>"+ ReportagemVO.campoCanal +"</th><td>"+ ReportagemVO.canal +"</td>";
		ReportagemVO.plataformaMap.put( ReportagemVO.campoCanal, ReportagemVO.canal);
		System.out.println( "ReportagemVO.ramalAlocado: "+ ReportagemVO.ramalAlocado);
	}

	public void relatarDuracao(){
		System.out.println( "ReportagemVO.campoDuracao: "+ ReportagemVO.campoDuracao +", ValidacaoVO.momentoFinal: "+ ValidacaoVO.momentoFinal +", ConstituicaoVO.padraoDataHora: "+ ConstituicaoVO.padraoDataHora +", ValidacaoVO.momentoInicial: "+ ValidacaoVO.momentoInicial);
		// ReportagemVO.duracao = ReportagemVO.campoDuracao +"="+ (getControleMaterialPeer().converterHorario( ValidacaoVO.momentoFinal, ConstituicaoVO.padraoDataHora).getTime() - getControleMaterialPeer().converterHorario( ValidacaoVO.momentoInicial, ConstituicaoVO.padraoDataHora).getTime()) / 1000 +"";
		ReportagemVO.duracao = "<th>"+ ReportagemVO.campoDuracao +"</th><td>"+ (getControleMaterialPeer().converterHorario( ValidacaoVO.momentoFinal, ConstituicaoVO.padraoDataHora).getTime() - getControleMaterialPeer().converterHorario( ValidacaoVO.momentoInicial, ConstituicaoVO.padraoDataHora).getTime()) / 1000 +"</td>";
		ReportagemVO.resumoMap.put(ReportagemVO.campoDuracao, (getControleMaterialPeer().converterHorario( ValidacaoVO.momentoFinal, ConstituicaoVO.padraoDataHora).getTime() - getControleMaterialPeer().converterHorario( ValidacaoVO.momentoInicial, ConstituicaoVO.padraoDataHora).getTime()) / 1000 +"");
		System.out.println( "ReportagemVO.duracao: "+ ReportagemVO.duracao);
	}

	public void relatarSessao(){
		// ReportagemVO.sessao = ReportagemVO.campoSessaoAppServer +"="+ ValidacaoVO.identificadorSessao +"";
		ReportagemVO.sessao = "<th>"+ ReportagemVO.campoSessaoAppServer +"</th><td>"+ ValidacaoVO.identificadorSessao +"</td>";
		ReportagemVO.plataformaMap.put( ReportagemVO.campoSessaoAppServer , ValidacaoVO.identificadorSessao);
		System.out.println( "ReportagemVO.sessao: "+ ReportagemVO.sessao);
	}

	public void relatarMPP(){
		// ReportagemVO.sessaoMPP = ReportagemVO.campoSessaoMPP +"="+ ReportagemVO.mPP +""; 
		ReportagemVO.sessaoMPP = "<th>"+ ReportagemVO.campoSessaoMPP +"</th><td>"+ ReportagemVO.mPP +"</td>"; 
		ReportagemVO.plataformaMap.put( ReportagemVO.campoSessaoMPP, ReportagemVO.mPP);
		System.out.println( "ReportagemVO.sessaoMPP: "+ ReportagemVO.sessaoMPP);		 
	}

	public void relatarChamador(){
		// ReportagemVO.chamador = ReportagemVO.campoIdentificacaoAutomatica +"="+ ReportagemVO.aNI +""; 
		ReportagemVO.chamador = "<th>"+ ReportagemVO.campoIdentificacaoAutomatica +"</th><td>"+ ReportagemVO.aNI +"</td>"; 
		ReportagemVO.resumoMap.put(ReportagemVO.campoIdentificacaoAutomatica, ReportagemVO.aNI);
		System.out.println( "ReportagemVO.chamador: "+ ReportagemVO.chamador);		 
	}

	public void relatarNumeroChamado(){
		// ReportagemVO.numeroChamado = ReportagemVO.campoVDNURA +"="+ ReportagemVO.dNIS +""; 
		ReportagemVO.numeroChamado = "<th>"+ ReportagemVO.campoVDNURA +"</th><td>"+ ReportagemVO.dNIS +"</td>"; 
		System.out.println( "ReportagemVO.numeroChamado: "+ ReportagemVO.numeroChamado);		 
	}

	public void relatarDesfechoAbandono(){
		ReportagemVO.desfecho = "<th>"+ ReportagemVO.campoDesfecho +"</th><td>ABANDONO</td>";
		System.out.println( "ReportagemVO.desfecho (abandonou): "+ ReportagemVO.desfecho);
	}

	public void relatarDesfechoTransferencia(){
		ReportagemVO.desfecho = "<th>"+ ReportagemVO.campoDesfecho +"</th><td>TRANSFERENCIA</td>";
		System.out.println( "ReportagemVO.desfecho (transferencia): "+ ReportagemVO.desfecho);
	}

	public void relatarDesfechoDesconexao(){
		ReportagemVO.desfecho = "<th>"+ ReportagemVO.campoDesfecho +"</th><td>DESCONEXAO</td>";
		System.out.println( "ReportagemVO.desfecho (desconexao): "+ ReportagemVO.desfecho);
	}
 
	public void relatarDesfechoExcesso(){
		ReportagemVO.desfecho = "<th>"+ ReportagemVO.campoDesfecho +"</th><td>EXCESSO</td>";
		System.out.println( "ReportagemVO.desfecho (excedeu): "+ ReportagemVO.desfecho);
	}

	public void relatarDesfechoQuebra(){
		ReportagemVO.desfecho = "<th>"+ ReportagemVO.campoDesfecho +"</th><td>QUEBRA</td>";
		System.out.println( "ReportagemVO.desfecho (quebrou): "+ ReportagemVO.desfecho);
	}

	public void iniciarTrajetoria(){
		ReportagemVO.trajetoria = "ENTRADA." + ValidacaoVO.digito;
		System.out.println( "ReportagemVO.trajetoria (iniciar): "+ ReportagemVO.trajetoria);
	}

	public void iniciarTrajetoriaEquivoco(){
		ReportagemVO.trajetoria = "ENTRADA.EQUIVOCO";
		System.out.println( "ReportagemVO.trajetoria (iniciar equivoco): "+ ReportagemVO.trajetoria);
	}

	public void complementarTrajetoriaEquivoco(){
		ReportagemVO.trajetoria += ".EQUIVOCO";
		System.out.println( "ReportagemVO.trajetoria (iniciar equivoco): "+ ReportagemVO.trajetoria);
	}

	public void complementarTrajetoriaOmissao(){
		ReportagemVO.trajetoria += ".OMISSAO";
		System.out.println( "ReportagemVO.trajetoria (iniciar equivoco): "+ ReportagemVO.trajetoria);
	}

	public void iniciarTrajetoriaAbandono(){
		ReportagemVO.trajetoria = "ENTRADA." + ConstituicaoVO.chaveAbandono;
		System.out.println( "ReportagemVO.trajetoria (iniciar equivoco): "+ ReportagemVO.trajetoria);
	}

	public void complementarTrajetoria(){
		ReportagemVO.trajetoria += "." + ValidacaoVO.digito;
		System.out.println( "ReportagemVO.trajetoria (complementar): "+ ReportagemVO.trajetoria);
	}

	public void complementarTrajetoriaAbandono(){
		ReportagemVO.trajetoria += "." + ConstituicaoVO.chaveAbandono;
		System.out.println( "ReportagemVO.trajetoria (abandono): "+ ReportagemVO.trajetoria);
	}
	
	public void complementarTrajetoriaQuebra(){
		ReportagemVO.trajetoria = "ENTRADA.QUEBRA";
		System.out.println( "ReportagemVO.trajetoria (quebra): "+ ReportagemVO.trajetoria);
	}
	
	public void concatenarTrajetoria(){
		ReportagemVO.trajetoria += ValidacaoVO.digito;
		System.out.println( "ReportagemVO.trajetoria (concatenar): "+ ReportagemVO.trajetoria);
	}
	
	public void zerarTrajetoria(){
		ReportagemVO.trajetoria = null;
		System.out.println( "ReportagemVO.trajetoria (zerar): "+ ReportagemVO.trajetoria);
	}

	public void definirCanal(){
		ReportagemVO.canal = ValidacaoVO.dado;
		System.out.println( "ReportagemVO.canal: "+ ReportagemVO.canal);
	}

	public void definirMPP(){
		ReportagemVO.mPP = ValidacaoVO.dado;
		System.out.println( "ReportagemVO.mPP: "+ ReportagemVO.mPP);
	}

	public void definirANI(){
		ReportagemVO.aNI = ValidacaoVO.dado;
		System.out.println( "ReportagemVO.aNI: "+ ReportagemVO.aNI);
	}

	public void definirDNIS(){
		ReportagemVO.dNIS = ValidacaoVO.dado;
		System.out.println( "ReportagemVO.dNIS: "+ ReportagemVO.dNIS);
	}

	public void definirUUIEnvio(){
		ReportagemVO.uUISaida = ReportagemVO.uUI;
		System.out.println( "ReportagemVO.uUISaida: "+ ReportagemVO.uUISaida);
	}

	public void definirUUIRecepcao(){
		ReportagemVO.uUIEntrada = ReportagemVO.uUI;
		System.out.println( "ReportagemVO.uUIEntrada: "+ ReportagemVO.uUIEntrada);
	}

	public void resgatarExcecao(){
		ReportagemVO.tipoExecao = ValidacaoVO.dossie.substring( ValidacaoVO.posicao, ValidacaoVO.dossie.indexOf( ConstituicaoVO.fimValor, ValidacaoVO.posicao));
		System.out.println( "ValidacaoVO.tipoExecao (resgatado): "+ ReportagemVO.tipoExecao);
	}
	
	public void resgatarLocalExcecao(){
		ReportagemVO.localExcecao = ValidacaoVO.dossie.substring( ValidacaoVO.posicao, ValidacaoVO.dossie.indexOf( "(", ValidacaoVO.posicao));
		System.out.println( "ReportagemVO.localExcecao (resgatado): "+ ReportagemVO.localExcecao);
	}
	
	public void resgatarMensagemExcecao(){
		ReportagemVO.mensagemExcecao = ValidacaoVO.dossie.substring( ValidacaoVO.posicao, ValidacaoVO.dossie.indexOf( ConstituicaoVO.fimMensagemExcecao, ValidacaoVO.posicao));
		System.out.println( "ReportagemVO.mensagemExcecao (resgatado): "+ ReportagemVO.mensagemExcecao);
	}

	public void resgatarVDNTransferencia(){
		ReportagemVO.vDNTransferencia = ValidacaoVO.dossie.substring( ValidacaoVO.posicao, ValidacaoVO.dossie.indexOf( ConstituicaoVO.fimVDNTransferencia, ValidacaoVO.posicao));
		System.out.println( "ReportagemVO.vdnTransferencia (VDN transferencia): "+ ReportagemVO.vDNTransferencia);
	}

	public void resgatarUUISaida(){
		ReportagemVO.uUI = ValidacaoVO.dossie.substring( ValidacaoVO.posicao, ValidacaoVO.dossie.indexOf( ConstituicaoVO.fimUUISaida, ValidacaoVO.posicao));
		System.out.println( "ReportagemVO.uUI (UUI saida): "+ ReportagemVO.uUI);
	}

	public void resgatarUUIEntrada(){
		ReportagemVO.uUI = ValidacaoVO.dossie.substring( ValidacaoVO.posicao, ValidacaoVO.dossie.indexOf( ConstituicaoVO.fimUUIEntrada, ValidacaoVO.posicao));
		System.out.println( "ReportagemVO.uUI (UUI entrada): "+ ReportagemVO.uUI);
	}

	public void resgatarLinhaExcecao(){
		ReportagemVO.linhaExcecao = ValidacaoVO.dossie.substring( ValidacaoVO.posicao, ValidacaoVO.dossie.indexOf( ")", ValidacaoVO.posicao));
		System.out.println( "ReportagemVO.LinhaExcecao(resgatado): "+ ReportagemVO.linhaExcecao);
	}

	public void escreverRelatorio(){
		// getControleMaterialPeer().escreverConteudo( ValidacaoVO.fwtEscritor, ConfiguracaoVO.local.concat( SimuladorVO.subsistema).concat("/").concat( ConfiguracaoVO.relatorio), ReportagemVO.relatorio);
		getControleMaterialPeer().escreverConteudo( ValidacaoVO.fwtEscritor, ConfiguracaoVO.caminho.concat( ConfiguracaoVO.relatorio), ReportagemVO.relatorio);
	}

	public void relatarResumo(){
		//ReportagemVO.resumo = ReportagemVO.campoResumo +"={"+ ReportagemVO.dataHoraInicio +","+ ReportagemVO.dataHoraTermino +","+ ReportagemVO.duracao +","+ ReportagemVO.chamador +",";
		ReportagemVO.resumo = "<table border='1' width='100%'><tr><th>"+ ReportagemVO.campoResumo +"</th><td><table border='1' width='100%'><tr>"+ ReportagemVO.dataHoraInicio +"</tr><tr>"+ ReportagemVO.dataHoraTermino +"</tr><tr>"+ ReportagemVO.duracao +"</tr><tr>"+ ReportagemVO.chamador +"</tr>";
		System.out.println( "ReportagemVO.resumo: "+ ReportagemVO.resumo);
	}

	public void relatarPlataforma(){
		// ReportagemVO.plataforma = ReportagemVO.campoPlataforma +"={"+ ReportagemVO.ramalAlocado +","+ ReportagemVO.sessao +","+ ReportagemVO.sessaoMPP +"}";
		ReportagemVO.plataforma = "<table border='1' width='100%'><tr><th>"+ ReportagemVO.campoPlataforma +"</th><td><table border='1' width='100%'><tr>"+ ReportagemVO.ramalAlocado +"</tr><tr>"+ ReportagemVO.sessao +"</tr><tr>"+ ReportagemVO.sessaoMPP +"</tr></table></td></tr></table>";
		//ReportagemVO.plataformaMap.put( ReportagemVO.campoPlataforma, ReportagemVO.ramalAlocado);
		System.out.println("ReportagemVO.plataforma : "+ ReportagemVO.plataforma);
	}

	public void iniciarRelatoDerivacao(){
//		ReportagemVO.derivacao = ReportagemVO.campoDerivacao +"={"+ ReportagemVO.numeroChamado;
		ReportagemVO.derivacao = "<table border='1' width='100%'><tr><th>"+ ReportagemVO.campoDerivacao +"</th><td><table border='1' width='100%'><tr>"+ ReportagemVO.numeroChamado +"</tr>";
		ReportagemVO.derivacaoMap.put( ReportagemVO.campoVDNURA, ReportagemVO.dNIS);
		System.out.println("ReportagemVO.derivacao (iniciar relato): "+ ReportagemVO.derivacao);
	}

	public void relatarBilheteSaida(){
		// ReportagemVO.derivacao += ","+ ReportagemVO.campoBilheteSaida +"="+ ReportagemVO.uUISaida;
		ReportagemVO.derivacao += "<tr><th>"+ ReportagemVO.campoBilheteSaida +"</th><td>"+ ReportagemVO.uUISaida +"</td></tr>";
		ReportagemVO.derivacaoMap.put( ReportagemVO.campoBilheteSaida, ReportagemVO.uUISaida);
		System.out.println( "ReportagemVO.derivacao (bilhete saida): "+ ReportagemVO.derivacao);
	}

	public void relatarBilheteEntrada(){
		// ReportagemVO.derivacao += ","+ ReportagemVO.campoBilheteEntrada +"="+ ReportagemVO.uUIEntrada;
		ReportagemVO.derivacao += "<tr><th>"+ ReportagemVO.campoBilheteEntrada +"</th><td>"+ ReportagemVO.uUIEntrada +"</td></tr>";
		ReportagemVO.derivacaoMap.put( ReportagemVO.campoBilheteEntrada, ReportagemVO.uUIEntrada);
		System.out.println( "ReportagemVO.derivacao (bilhete entrada): "+ ReportagemVO.derivacao);
	}

	public void relatarVDNTransferencia(){
		// ReportagemVO.derivacao += ","+ ReportagemVO.campoVDNTransferencia +"="+ ReportagemVO.vdnTransferencia;
		ReportagemVO.derivacao += "<tr><th>"+ ReportagemVO.campoVDNTransferencia +"</th><td>"+ ReportagemVO.vDNTransferencia +"</td></tr>";
		ReportagemVO.derivacaoMap.put( ReportagemVO.campoVDNTransferencia, ReportagemVO.vDNTransferencia);
		System.out.println( "ReportagemVO.derivacao (vdn): "+ ReportagemVO.derivacao);
	}

//	public void complementarRelatorioTrajetoria(){
//		ReportagemVO.relatorio = ReportagemVO.relatorio +","+ ReportagemVO.campoTrajetoria +"="+ ReportagemVO.trajetoria +"";
//		System.out.println( "ReportagemVO.relatorio (abandonou): "+ ReportagemVO.relatorio);		
//	}

	public void iniciarMapas(){
		ReportagemVO.resumoMap = new HashMap<String,String>();
		ReportagemVO.relatoMap = new HashMap<String,String>();
		ReportagemVO.derivacaoMap = new HashMap<String,String>();
		ReportagemVO.plataformaMap = new HashMap<String,String>();
		ReportagemVO.delatoMap = new HashMap<String,String>();
		ReportagemVO.relatorioMap = new HashMap<String,HashMap<String,String>>();
		System.out.println( "ReportagemVO.resumoMap: "+ ReportagemVO.resumoMap +", ReportagemVO.relatoMap: "+ ReportagemVO.relatoMap +", ReportagemVO.derivacaoMap: "+ ReportagemVO.derivacaoMap +", ReportagemVO.plataformaMap: "+ ReportagemVO.plataformaMap +", ReportagemVO.delatoMap: "+ ReportagemVO.delatoMap +", ReportagemVO.relatorioMap: "+ ReportagemVO.relatorioMap);
	}

}

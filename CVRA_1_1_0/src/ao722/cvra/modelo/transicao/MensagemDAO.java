package ao722.cvra.modelo.transicao;

import java.io.FileNotFoundException;
import java.io.IOException;

import ao722.cvra.modelo.estado.ConfiguracaoVO;
import ao722.cvra.modelo.estado.ConstituicaoVO;
import ao722.cvra.modelo.estado.InteracaoVO;
import ao722.cvra.modelo.estado.LocalizacaoVO;
import ao722.cvra.modelo.estado.MensagemVO;
import ao722.cvra.modelo.estado.NavegacaoVO;

public class MensagemDAO {

	public MensagemDAO() {	}
	
	public void definirRepeticao(){
		MensagemVO.assunto = "[".concat( ConstituicaoVO.repeticao).concat("] ").concat( LocalizacaoVO.trajetoria);
	}
	
	public void definirRetorno(){
		MensagemVO.assunto = "[".concat( ConstituicaoVO.retorno).concat("] ").concat( LocalizacaoVO.trajetoria);
	}

	public void definirEntrada(){
		MensagemVO.assunto = "[".concat( ConstituicaoVO.saudacao).concat("] ");
	}
	
	public void definirNavegacao(){
		MensagemVO.assunto = "[NAVEGACAO] ".concat( LocalizacaoVO.trajetoria);
	}
	
	public void definirTransferencia(){
		MensagemVO.assunto = "[".concat( ConstituicaoVO.transferencia).concat("] ").concat( LocalizacaoVO.trajetoria);
	}

	public void definirDesconexao(){
		MensagemVO.assunto = "[".concat( ConstituicaoVO.desconexao).concat("] ").concat( LocalizacaoVO.trajetoria);
	}
	
	public void definirDesacerto(){
		MensagemVO.assunto = "[".concat( ConstituicaoVO.desacerto).concat("] Tentativa ".concat( String.valueOf( NavegacaoVO.tentativas)).concat("ª de ").concat( String.valueOf( ConfiguracaoVO.limiteTentativas).concat(" disponiveis.")));
	}
	
	public void definirInsucesso(){
		MensagemVO.assunto = "[".concat( ConstituicaoVO.insucesso).concat("]");
	}
	
	public void definirEco(){
		MensagemVO.reacao = InteracaoVO.acionador;
	}

//	public void definirAviso(){
//		MensagemVO.reacao = InteracaoVO.acionador;
//	}

	public void obterReacao( ){
		MensagemVO.reacao = obterValor( 
			ConfiguracaoVO.caminho.concat( ConfiguracaoVO.script)
			, "MENSAGEM.".concat( NavegacaoVO.navegacao)
		);

	}

	public void obterFrase( ){
		MensagemVO.frase = obterValor( 
			ConfiguracaoVO.caminho.concat( ConfiguracaoVO.script)
			, "MENSAGEM.".concat( LocalizacaoVO.trajetoria)
		);

	}

	public String obterValor(
			String pCarminhoArquivo				// Endereco completo do arquivo de propriedades java a ser lido
			, String pChave						// Chave da informacao a ser lida
	){ 
		String strValor = "";
		java.util.Properties prpScriptPadrao = new java.util.Properties();
		java.io.InputStream ipsProperties = null;

		try{
			ipsProperties = new java.io.FileInputStream( pCarminhoArquivo);
			prpScriptPadrao.load( ipsProperties);
			strValor = (String)prpScriptPadrao.get( pChave);
		}
		catch( FileNotFoundException e){ 
			strValor = ConstituicaoVO.desacerto;
		}
		catch( IOException e){
			strValor = ConstituicaoVO.desacerto;
		}
		catch( Exception e){
			strValor = ConstituicaoVO.desacerto;
		}

		boolean blnValorEncontrado = (strValor != null); 
		if( blnValorEncontrado ){ }
		else {
			strValor = ConstituicaoVO.desacerto;
		}

		return strValor;
	}

	
}

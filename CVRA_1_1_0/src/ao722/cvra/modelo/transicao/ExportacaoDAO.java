package ao722.cvra.modelo.transicao;

import javax.swing.JOptionPane;

import ao722.cvra.contrato.fornecedor.ControleMaterialPeer;
import ao722.cvra.modelo.estado.ConfiguracaoVO;
import ao722.cvra.modelo.estado.ControleMaterialVO;
import ao722.cvra.modelo.estado.ExportacaoVO;
import ao722.cvra.modelo.estado.IntervencaoVO;
import ao722.cvra.visao.saida.SelecionadorArquivoUI;

public class ExportacaoDAO {

	// Propriedades e atributos
	private ControleMaterialPeer controleMaterialPeer;

	// Construtores e sobrecargas
	public ExportacaoDAO() {
		// TODO Auto-generated constructor stub
		ExportacaoVO.selecionadorArquivo = new SelecionadorArquivoUI( ConfiguracaoVO.caminho);
		setControleMaterialPeer( new ControleMaterialPeer());
	}

	public ControleMaterialPeer getControleMaterialPeer() {
		return controleMaterialPeer;
	}
	public void setControleMaterialPeer(
			ControleMaterialPeer controleMaterialPeer) {
		this.controleMaterialPeer = controleMaterialPeer;
	}

	public void	predefinirArquivoSelecionado(){
		ExportacaoVO.selecionadorArquivo.setSelectedFile( new java.io.File( ConfiguracaoVO.sumario.concat( ConfiguracaoVO.extensaoExportacao)));
		System.out.println( "ExportacaoVO.selecionadorArquivo.getSelectedFile(): "+ ExportacaoVO.selecionadorArquivo.getSelectedFile());
	}

	public void mostrarSelecionadorArquivo(){
		ExportacaoVO.resposta = ExportacaoVO.selecionadorArquivo.showSaveDialog( null);
		System.out.println( "ExportacaoVO.resposta: "+ ExportacaoVO.resposta);
	}

	public void obterArquivoSelecionado(){
		ControleMaterialVO.relatorioExportacao = (java.io.File)getControleMaterialPeer().obterArquivoEscolhido( ExportacaoVO.selecionadorArquivo);
		System.out.println( "ControleMaterialVO.relatorioExportacao: "+ ControleMaterialVO.relatorioExportacao);
	}

	public void pegarRelatorioArquivado(){
		ControleMaterialVO.relatorioArquivado = (java.io.File)getControleMaterialPeer().abrirArquivo( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.relatorio));
		System.out.println( "ControleMaterialVO.relatorioArquivado: "+ ControleMaterialVO.leitor);
	}

	public void medirTamanhoRelatorioArquivado(){
		ExportacaoVO.armazem = new char[(int)ControleMaterialVO.relatorioArquivado.length()];
		System.out.println( "ExportacaoVO.armazem: "+ String.valueOf( ExportacaoVO.armazem));
	}

	public void lerRelatorioArquivado(){
		ExportacaoVO.armazem = getControleMaterialPeer().lerConteudo( ControleMaterialVO.relatorioArquivado);
		System.out.println( "ExportacaoVO.armazem: "+ String.valueOf(ExportacaoVO.armazem));
	}

	public void escreverRelatorioExportacao(){
		getControleMaterialPeer().escreverConteudo( ControleMaterialVO.fwtEscritor, ControleMaterialVO.relatorioExportacao.getAbsolutePath(), String.valueOf( ExportacaoVO.armazem));
	}

	public void limparRelatorioArquivado(){
		getControleMaterialPeer().limparArquivo( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.relatorio));
	}

	public void limparDossieArquivado(){
		getControleMaterialPeer().limparArquivo( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.produto));
	}

	public void limparHistoricoArquivado(){
		getControleMaterialPeer().limparArquivo( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.historico));
	}

	public void mostrarConfirmadorLimpeza(){
		ExportacaoVO.resposta = JOptionPane.showConfirmDialog( null, ( Object) "Limparei os arquivos de historico, evidencia e relatorio.", IntervencaoVO.acionador, JOptionPane.OK_CANCEL_OPTION);
	}

}

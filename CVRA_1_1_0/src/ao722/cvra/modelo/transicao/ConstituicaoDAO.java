package ao722.cvra.modelo.transicao;

import ao722.cvra.modelo.estado.ConfiguracaoVO;
import ao722.cvra.modelo.estado.ConstituicaoVO;

public class ConstituicaoDAO {
	
	// Construtores
	public ConstituicaoDAO(){	}

	// Metodos
	public void obterChaveamentoSimulacao(){
		ConstituicaoVO.chaves = obterValor( ConfiguracaoVO.script, "CONSTITUICAO.CHAVES");
	}

	public void obterChaveamentoCertificacao(){
		ConstituicaoVO.chaves = obterValor( ConfiguracaoVO.sumario, "CONSTITUICAO.CHAVES");
		System.out.println(ConstituicaoVO.chaves);
	}

	public void definirChaveamentoSimulacao(){
		ConstituicaoVO.entrada = ConstituicaoVO.chaves.split(";")[0];
		ConstituicaoVO.saudacao = ConstituicaoVO.chaves.split(";")[1];
		ConstituicaoVO.repeticao = ConstituicaoVO.chaves.split(";")[2];
		ConstituicaoVO.omissao = ConstituicaoVO.chaves.split(";")[3];
		ConstituicaoVO.desacerto= ConstituicaoVO.chaves.split(";")[4];
		ConstituicaoVO.retorno = ConstituicaoVO.chaves.split(";")[5];
		ConstituicaoVO.insucesso = ConstituicaoVO.chaves.split(";")[6];
		ConstituicaoVO.transferencia = ConstituicaoVO.chaves.split(";")[7];
		ConstituicaoVO.desconexao  = ConstituicaoVO.chaves.split(";")[8];
	}

	public void definirChaveamentoCertificacao(){
		ConstituicaoVO.fimValor = ConstituicaoVO.chaves.split(";")[0];
		ConstituicaoVO.fimMenu = ConstituicaoVO.chaves.split(";")[1];
		ConstituicaoVO.chaveAlocacao = ConstituicaoVO.chaves.split(";")[2];
		ConstituicaoVO.chaveDado = ConstituicaoVO.chaves.split(";")[3];
		ConstituicaoVO.chaveMenu = ConstituicaoVO.chaves.split(";")[4];
		ConstituicaoVO.chaveOmissao = ConstituicaoVO.chaves.split(";")[5];
		ConstituicaoVO.chaveEngano = ConstituicaoVO.chaves.split(";")[6];
		ConstituicaoVO.chaveTransferencia = ConstituicaoVO.chaves.split(";")[7];
		ConstituicaoVO.padraoDataHora = ConstituicaoVO.chaves.split(";")[8];
		ConstituicaoVO.separadorData = ConstituicaoVO.chaves.split(";")[9];
		ConstituicaoVO.chaveCanal = ConstituicaoVO.chaves.split(";")[10];
		ConstituicaoVO.chaveAbandono = ConstituicaoVO.chaves.split(";")[11];
		ConstituicaoVO.chaveMPP = ConstituicaoVO.chaves.split(";")[12];
		ConstituicaoVO.chaveANI = ConstituicaoVO.chaves.split(";")[13];
		ConstituicaoVO.chaveDNIS = ConstituicaoVO.chaves.split(";")[14];
		ConstituicaoVO.chaveExcecao = ConstituicaoVO.chaves.split(";")[15];
		ConstituicaoVO.chaveCausaExcecao = ConstituicaoVO.chaves.split(";")[16];
		ConstituicaoVO.chaveLocalExcecao = ConstituicaoVO.chaves.split(";")[17];
		ConstituicaoVO.chaveMensagemExcecao = ConstituicaoVO.chaves.split(";")[18];
		ConstituicaoVO.chaveServlet = ConstituicaoVO.chaves.split(";")[19];
		ConstituicaoVO.fimMensagemExcecao = ConstituicaoVO.chaves.split(";")[20];
		ConstituicaoVO.chaveComunicacaoCTI = ConstituicaoVO.chaves.split(";")[21];
		ConstituicaoVO.fimVDNTransferencia = ConstituicaoVO.chaves.split(";")[22];
		ConstituicaoVO.chaveVDNTransferencia = ConstituicaoVO.chaves.split(";")[23];
		ConstituicaoVO.chaveBilheteSaida = ConstituicaoVO.chaves.split(";")[24];
		ConstituicaoVO.chaveUUI = ConstituicaoVO.chaves.split(";")[25];
		ConstituicaoVO.chaveBilheteEntrada = ConstituicaoVO.chaves.split(";")[26];
		ConstituicaoVO.fimUUISaida = ConstituicaoVO.chaves.split(";")[27];
		ConstituicaoVO.fimUUIEntrada = ConstituicaoVO.chaves.split(";")[28];
		ConstituicaoVO.chaveLinhaTransferencia = ConstituicaoVO.chaves.split(";")[29];
	}

	public void contingenciarChaveamentoSimulacao(){
		ConstituicaoVO.entrada = "ENTRADA";
		ConstituicaoVO.saudacao = "SAUDACAO";
		ConstituicaoVO.repeticao = "REPETICAO";
		ConstituicaoVO.omissao = "OMISSAO";
		ConstituicaoVO.desacerto= "DESACERTO";
		ConstituicaoVO.retorno = "RETORNO";
		ConstituicaoVO.insucesso = "INSUCESSO";
		ConstituicaoVO.transferencia = "TRANSFERENCIA";
		ConstituicaoVO.desconexao  = "DESCONEXAO";
	}

	public void obterDesfechos(){
		ConstituicaoVO.desfechos = obterValor( ConfiguracaoVO.script, "CONSTITUICAO.DESFECHOS");
	}

	public void obterIntegracao(){
		ConstituicaoVO.chaves = obterValor( ConfiguracaoVO.sumario, "CONSTITUICAO.INTEGRACAO");
		System.out.println( "ConstituicaoVO.chaves: ".concat(ConstituicaoVO.chaves));
	}

	public void definirDesfechos(){
		ConstituicaoVO.excedido = ConstituicaoVO.desfechos.split(";")[0]; 
		ConstituicaoVO.transferido = ConstituicaoVO.desfechos.split(";")[1]; 
		ConstituicaoVO.abandonou = ConstituicaoVO.desfechos.split(";")[2]; 
		ConstituicaoVO.desconectado = ConstituicaoVO.desfechos.split(";")[3]; 	
	}

	public void definirIntegracao(){
		ConstituicaoVO.peerName = ConstituicaoVO.chaves.split(";")[0];
		ConstituicaoVO.ctiLink = ConstituicaoVO.chaves.split(";")[1];
		ConstituicaoVO.login = ConstituicaoVO.chaves.split(";")[2];
		ConstituicaoVO.password	 = ConstituicaoVO.chaves.split(";")[3];
	}

	public void contingenciarDesfechos(){
		ConstituicaoVO.excedido = "EXCEDEU"; 
		ConstituicaoVO.transferido = "TRANSFERIDO"; 
		ConstituicaoVO.abandonou = "ABANDONOU"; 
		ConstituicaoVO.desconectado = "DESCONECTADO"; 	
	}

	public void obterComandos(){
		ConstituicaoVO.comandos = obterValor( ConfiguracaoVO.script, "CONSTITUICAO.COMANDOS");
	}

	public void obterCampos(){
		ConstituicaoVO.campos = obterValor( ConfiguracaoVO.sumario, "CONSTITUICAO.CAMPOS");
	}

	public String obterValor(
			String pCarminhoArquivo				// Endereco completo do arquivo de propriedades java a ser lido
			, String pChave						// Chave da informacao a ser lida
	) {
		String strValor = "";
		java.util.Properties prpScriptPadrao = new java.util.Properties();
		java.io.InputStream ipsProperties = null;

		try{
			// ipsProperties = new java.io.FileInputStream( ConfiguracaoVO.local.concat(SimuladorVO.subsistema).concat("/").concat( pCarminhoArquivo));
			ipsProperties = new java.io.FileInputStream( ConfiguracaoVO.caminho.concat( pCarminhoArquivo));
			prpScriptPadrao.load( ipsProperties);
			strValor = (String)prpScriptPadrao.get( pChave);
		}
		catch( Exception e){

			boolean blnDesacertoExiste = (ConstituicaoVO.desacerto != null);
			if(blnDesacertoExiste ){
				strValor = ConstituicaoVO.desacerto;
			}
			else {
				strValor = "DESACERTO";
			}

		}

		boolean blnValorEncontrado = (strValor != null); 
		if( blnValorEncontrado ){ }
		else {
			strValor = ConstituicaoVO.desacerto;
		}

		return strValor;
	}
	
}

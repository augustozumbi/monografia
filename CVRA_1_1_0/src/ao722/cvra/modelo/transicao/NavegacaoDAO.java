package ao722.cvra.modelo.transicao;

import java.io.FileNotFoundException;
import java.io.IOException;

import ao722.cvra.modelo.estado.ConfiguracaoVO;
import ao722.cvra.modelo.estado.ConstituicaoVO;
import ao722.cvra.modelo.estado.InteracaoVO;
import ao722.cvra.modelo.estado.LocalizacaoVO;
import ao722.cvra.modelo.estado.NavegacaoVO;

public class NavegacaoDAO {

	// Construtores
	public NavegacaoDAO() { 
	}

	// Metodos
	public void iniciar(){
		reiniciarTentativas();
		definirInicio();
	}

	public void reiniciarTentativas(){
		NavegacaoVO.tentativas = 0;
	}

	public void incrementarTentativas(){
		NavegacaoVO.tentativas = NavegacaoVO.tentativas+1;
	}

	public void definirInicio(){
		NavegacaoVO.navegacao = ConstituicaoVO.saudacao;
	}

	public void definirRepeticao(){
		NavegacaoVO.navegacao = LocalizacaoVO.trajetoria;
	}
	
	public void definirRetorno(){
		NavegacaoVO.navegacao = NavegacaoVO.destino.split(";")[1];
	}
	
	public void definirDesacerto(){
		NavegacaoVO.navegacao = LocalizacaoVO.trajetoria.concat(".").concat( ConstituicaoVO.desacerto);
	}

	public void definirInsucesso(){
		NavegacaoVO.navegacao = LocalizacaoVO.trajetoria.concat(".").concat( ConstituicaoVO.insucesso);
	}

//	public void definirEntrada(){
//		NavegacaoVO.navegacao = ConstituicaoVO.entrada;
//	}

	public void definirNavegacao(){
		NavegacaoVO.navegacao = LocalizacaoVO.trajetoria.concat(".").concat( InteracaoVO.acionador);
	}

	public void obterNavegacao( ){
		NavegacaoVO.destino = obterValor( 
			ConfiguracaoVO.caminho.concat( ConfiguracaoVO.script)
			, "NAVEGACAO.".concat( NavegacaoVO.navegacao)
		);

	}

	public String obterValor(
			String pCarminhoArquivo				// Endereco completo do arquivo de propriedades java a ser lido
			, String pChave						// Chave da informacao a ser lida
	){ 
		String strValor = "";
		java.util.Properties prpScriptPadrao = new java.util.Properties();
		java.io.InputStream ipsProperties = null;

		try{
			ipsProperties = new java.io.FileInputStream( pCarminhoArquivo);
			prpScriptPadrao.load( ipsProperties);
			strValor = (String)prpScriptPadrao.get( pChave);
		}
		catch( FileNotFoundException e){ 
			strValor = ConstituicaoVO.desacerto;
		}
		catch( IOException e){
			strValor = ConstituicaoVO.desacerto;
		}
		catch( Exception e){
			strValor = ConstituicaoVO.desacerto;
		}

		boolean blnValorEncontrado = (strValor != null); 
		if( blnValorEncontrado ){ }
		else {
			strValor = ConstituicaoVO.desacerto;
		}

		return strValor;
	}

}

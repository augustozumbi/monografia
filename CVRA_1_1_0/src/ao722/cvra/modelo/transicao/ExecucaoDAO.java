package ao722.cvra.modelo.transicao;

import ao722.cvra.contrato.fornecedor.ControleMaterialPeer;
import ao722.cvra.modelo.estado.ConfiguracaoVO;
import ao722.cvra.modelo.estado.ConstituicaoVO;
import ao722.cvra.modelo.estado.ControleMaterialVO;
import ao722.cvra.modelo.estado.ExecucaoVO;

public class ExecucaoDAO {

	// Propriedades
	private ControleMaterialPeer controleMaterialPeer;

	// Construtor
	public ExecucaoDAO() {
		setControleMaterialPeer( new ControleMaterialPeer());
	}

	// Getters and Setters
	public ControleMaterialPeer getControleMaterialPeer() {
		return controleMaterialPeer;
	}
	public void setControleMaterialPeer(ControleMaterialPeer controleMaterialPeer) {
		this.controleMaterialPeer = controleMaterialPeer;
	}

	public void obterSumario(){
		ExecucaoVO.casos = getControleMaterialPeer().obterChaves( ConfiguracaoVO.sumario);
		ExecucaoVO.tarefas = getControleMaterialPeer().obterValores( ConfiguracaoVO.sumario, ExecucaoVO.casos);
		ExecucaoVO.titulos = new String[]{ "codigo do teste", "script de teste"};
		System.out.println( "ExecucaoVO.casos: "+ ExecucaoVO.casos);
	}

	public void definirCarga(){
		ExecucaoVO.carga = ExecucaoVO.casos.length;
		System.out.println( "ExecucaoVO.carga: "+ ExecucaoVO.carga);
	}

//	public void subtrairCarga(){
//		ExecucaoVO.carga = ExecucaoVO.carga-1;
//		System.out.println( "ExecucaoVO.carga(subtraida): "+ ExecucaoVO.carga);
//	}

	public void definirLinha(){
		ExecucaoVO.linha = ExecucaoVO.casos[ ExecucaoVO.teste];
		System.out.println( "ExecucaoVO.Linha: "+ ExecucaoVO.linha);
	}

	public void iniciarRegistros(){
		ExecucaoVO.historico = "";
		System.out.println( "ExecucaoVO.historico: "+ ExecucaoVO.historico);
	}

	public void registrarTeste(){
		ExecucaoVO.historico = ExecucaoVO.historico.concat(ExecucaoVO.linha);
		System.out.println( "ExecucaoVO.historico: "+ ExecucaoVO.historico);
	}

	public void registrarHorarioInicio(){
		ExecucaoVO.historico += "=".concat( new java.text.SimpleDateFormat( ConstituicaoVO.padraoDataHora).format( new java.util.Date()));
		System.out.println( "ExecucaoVO.historico: "+ ExecucaoVO.historico);
	}

	public void registrarHorarioFim(){
		ExecucaoVO.historico += ",".concat( new java.text.SimpleDateFormat( ConstituicaoVO.padraoDataHora).format( new java.util.Date())).concat("\n");
		System.out.println( "ExecucaoVO.historico: "+ ExecucaoVO.historico);
	}

	public void gravarHistorico(){
		// getControleMaterialPeer().escreverConteudo( ControleMaterialVO.fwtEscritor, ConfiguracaoVO.local.concat( SimuladorVO.subsistema).concat("/").concat( ConfiguracaoVO.historico), ExecucaoVO.historico);
		getControleMaterialPeer().escreverConteudo( ControleMaterialVO.fwtEscritor, ConfiguracaoVO.caminho.concat( ConfiguracaoVO.historico), ExecucaoVO.historico);
		System.out.println("Historico registrado");
	}

	public void definirCronograma(){
		ExecucaoVO.cronograma = ExecucaoVO.linha.split("\\.");
		System.out.println( "ExecucaoVO.cronograma: "+ ExecucaoVO.cronograma);
	}

	public void definirRoteiro(){
		ExecucaoVO.roteiroTeste = getControleMaterialPeer().obterValor( ConfiguracaoVO.sumario, ExecucaoVO.linha);
		System.out.println( "ExecucaoVO.roteiroTeste: "+ ExecucaoVO.roteiroTeste);
	}

	public void definirProcesso(){
		ExecucaoVO.processo = ExecucaoVO.roteiroTeste.split(";");
		System.out.println( "ExecucaoVO.processo: "+ ExecucaoVO.processo);
	}

	public void definirPassos(){
		ExecucaoVO.passos = ExecucaoVO.processo.length;
		System.out.println( "ExecucaoVO.passos: "+ ExecucaoVO.passos);
	}

	public void definirProcedimento(){
		ExecucaoVO.procedimento = ExecucaoVO.processo[ ExecucaoVO.etapa].split(",")[1];
		System.out.println( "ExecucaoVO.procedimento: "+ ExecucaoVO.procedimento);
	}

	public void definirAguardo(){
		ExecucaoVO.aguardo = Integer.valueOf( ExecucaoVO.processo[ ExecucaoVO.etapa].split(",")[0]) - ExecucaoVO.ultimoAguardo; 
		System.out.println( "ExecucaoVO.aguardo: "+ ExecucaoVO.aguardo +"(definido)");
	}

	public void definirUltimoAguardo(){
		ExecucaoVO.ultimoAguardo = Integer.valueOf( ExecucaoVO.processo[ ExecucaoVO.etapa].split(",")[0]);
		System.out.println( "ExecucaoVO.ultimoAguardo: "+ ExecucaoVO.ultimoAguardo);
	}

	public void definirAguardoCompensado(){
		ExecucaoVO.aguardo = ExecucaoVO.aguardo - ExecucaoVO.encadeados*(ConfiguracaoVO.tempoEntreDigitos/1000); 
		//ExecucaoVO.aguardo = ConfiguracaoVO.tempoEntreDigitos/1000; 
		System.out.println( "ExecucaoVO.aguardo (compensado): "+ ExecucaoVO.aguardo);
	}

	public void definirComplexidade(){
		ExecucaoVO.complexidade = ExecucaoVO.procedimento.length();
		System.out.println( "ExecucaoVO.complexidade: "+ ExecucaoVO.complexidade);
	}

	public void definirDigito(){
		ExecucaoVO.digito = ExecucaoVO.procedimento;
		System.out.println( "ExecucaoVO.digito: "+ ExecucaoVO.digito);
	}

	public void desencadearDigito(){
		ExecucaoVO.digito = String.valueOf( ExecucaoVO.cadeia.charAt( ExecucaoVO.indicador));
		System.out.println( "ExecucaoVO.digito: "+ ExecucaoVO.digito);
	}

	public void definirEncadeados(){
		ExecucaoVO.encadeados = ExecucaoVO.cadeia.length();
		System.out.println( "ExecucaoVO.encadeados: "+ ExecucaoVO.encadeados);
	}

	public void obterCadeia(){
		ExecucaoVO.cadeia = ExecucaoVO.suprimento[ ExecucaoVO.encadeado];
		System.out.println( "ExecucaoVO.cadeia: "+ ExecucaoVO.cadeia);
	}

	public void obterMassa(){
		ExecucaoVO.suprimento = getControleMaterialPeer().obterValor( ConfiguracaoVO.massa, ExecucaoVO.procedimento).split(",");
		System.out.println( "ExecucaoVO.massa.length: "+ ExecucaoVO.suprimento.length);
	}

	public void zerarIndicador(){
		ExecucaoVO.indicador = 0;
		System.out.println( "ExecucaoVO.indicador: "+ ExecucaoVO.indicador);
	}

	public void saltarIndicador(){
		ExecucaoVO.indicador = ExecucaoVO.indicador+1;
		System.out.println( "ExecucaoVO.indicador: "+ ExecucaoVO.indicador);
	}

	public void aguardarMomento(){
		System.out.println("*** COMENTARIO EXCEPCIONAL: ExecucaoVO.aguardo = "+ ExecucaoVO.aguardo);

		try {
			Thread.sleep( ExecucaoVO.aguardo*1000);
		} 
		catch ( InterruptedException e) {
			System.err.println("ExecucaoDAO.aguardarMomento(): "+ e);
		}

	}
	
	public void saltarEncadeado(){
		ExecucaoVO.encadeado = ExecucaoVO.encadeado+1;
		System.out.println( "ExecucaoVO.encadeado: "+ ExecucaoVO.encadeado);
	}

	public void aguardarTom(){

		try {
			System.out.println( "ConfiguracaoVO.tempoEntreDigitos: "+ ConfiguracaoVO.tempoEntreDigitos);
			Thread.sleep( ConfiguracaoVO.tempoEntreDigitos);
		} 
		catch ( InterruptedException e) {
			System.err.println("ExecucaoDAO.aguardarMomento(): "+ e);
		}

	}

	public void zerarTeste(){
		ExecucaoVO.teste = 0;
		System.out.println( "ExecucaoVO.teste: "+ ExecucaoVO.teste);
	}

	public void saltarLinha(){
		ExecucaoVO.teste = ExecucaoVO.teste+1;
		System.out.println( "ExecucaoVO.teste: "+ ExecucaoVO.teste);
	}

	public void zerarEtapa(){
		ExecucaoVO.etapa = 0;
		System.out.println( "ExecucaoVO.etapa : "+ ExecucaoVO.etapa);
	}

	public void saltarEtapa(){
		ExecucaoVO.etapa = ExecucaoVO.etapa+1;
		System.out.println( "ExecucaoVO.etapa : "+ ExecucaoVO.etapa);
	}

	public void zerarAguardo(){
		ExecucaoVO.aguardo = 0;
		ExecucaoVO.ultimoAguardo = 0;
		System.out.println( "ExecucaoVO.aguardo: "+ ExecucaoVO.aguardo);
	}

	public void contarPulo(){
		ExecucaoVO.pulos = ExecucaoVO.pulos+1;
		System.out.println( "ExecucaoVO.pulos: "+ ExecucaoVO.pulos);
	}

	public void sinalizarAtivo(){
		ExecucaoVO.estagio = ConstituicaoVO.INICIADO;
		System.out.println( "ExecucaoVO.estagio: "+ ExecucaoVO.estagio);
	}
	
	public void sinalizarSuspenso(){
		ExecucaoVO.estagio = ConstituicaoVO.SUSPENSO;
		System.out.println( "ExecucaoVO.estagio: "+ ExecucaoVO.estagio);
	}

	public void zerarSumario(){
		ExecucaoVO.casos = null;
		System.out.println( "ExecucaoVO.casos: "+ ExecucaoVO.casos);
	}

	public void zerarCarga(){
		ExecucaoVO.carga = 0;
		System.out.println( "ExecucaoVO.carga: "+ ExecucaoVO.carga);
	}

	public void zerarLinha(){
		ExecucaoVO.linha = null;
		System.out.println( "ExecucaoVO.linha: "+ ExecucaoVO.linha);
	}

	public void zerarPulos(){
		ExecucaoVO.pulos = 0;
		System.out.println( "ExecucaoVO.pulos: "+ ExecucaoVO.pulos);
	}

	public void zerarHistorico(){
		ExecucaoVO.historico = null;
		System.out.println( "ExecucaoVO.historico: "+ ExecucaoVO.historico);
	}

	public void zerarRoteiro(){
		ExecucaoVO.roteiroTeste = null;
		System.out.println( "ExecucaoVO.roteiroTeste: "+ ExecucaoVO.roteiroTeste);
	}

	public void zerarCronograma(){
		ExecucaoVO.cronograma = null;
		System.out.println( "ExecucaoVO.cronograma: "+ ExecucaoVO.cronograma);
	}
	
	public void zerarProcesso(){
		ExecucaoVO.processo = null;
		System.out.println( "ExecucaoVO.processo: "+ ExecucaoVO.processo);
	}
	
	public void zerarPassos(){
		ExecucaoVO.passos = 0;
		System.out.println( "ExecucaoVO.passos: "+ ExecucaoVO.passos);
	}

	public void zerarUltimoAguardo(){
		ExecucaoVO.ultimoAguardo = 0;
		System.out.println("ExecucaoVO.ultimoAguardo: "+ ExecucaoVO.ultimoAguardo);
	}
	
	public void zerarProcedimento(){
		ExecucaoVO.procedimento = null;
		System.out.println("ExecucaoVO.procedimento: "+ ExecucaoVO.procedimento);
	}
	
	public void zerarComplexidade(){
		ExecucaoVO.complexidade = 0;
		System.out.println( "ExecucaoVO.complexidade: "+ ExecucaoVO.complexidade);
	}
	
	public void zerarDigito(){
		ExecucaoVO.digito = null;
		System.out.println( "ExecucaoVO.digito: "+ ExecucaoVO.digito);
	}

	public void zerarMassa(){
		ExecucaoVO.suprimento = null;
		System.out.println( "ExecucaoVO.massa: "+ ExecucaoVO.suprimento);
	}
	
	public void zerarCadeia(){
		ExecucaoVO.cadeia = null;
		System.out.println("ExecucaoVO.cadeia: "+ ExecucaoVO.cadeia);
	}
	
	public void zerarEncadeados(){
		ExecucaoVO.encadeados = 0;
		System.out.println("ExecucaoVO.encadeados: "+ ExecucaoVO.encadeados);
	}

	public void sinalizarInterrompido(){
		ExecucaoVO.estagio = ConstituicaoVO.INTERROMPIDO;
		System.out.println( "ExecucaoVO.estagio: "+ ExecucaoVO.estagio);
	}

}

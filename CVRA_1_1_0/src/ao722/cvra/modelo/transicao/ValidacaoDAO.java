package ao722.cvra.modelo.transicao;

import java.util.HashMap;

import ao722.cvra.contrato.fornecedor.ControleMaterialPeer;
import ao722.cvra.modelo.estado.ConexaoVO;
import ao722.cvra.modelo.estado.ConfiguracaoVO;
import ao722.cvra.modelo.estado.ConstituicaoVO;
import ao722.cvra.modelo.estado.ControleMaterialVO;
import ao722.cvra.modelo.estado.ReportagemVO;
import ao722.cvra.modelo.estado.ValidacaoVO;

public class ValidacaoDAO {

	// Atributos
	private ControleMaterialPeer controleMaterialPeer;

	// Construtores
	public ValidacaoDAO(){
		setControleMaterialPeer( new ControleMaterialPeer());
	}
	// Encapsulamento
	public ControleMaterialPeer getControleMaterialPeer() {
		return controleMaterialPeer;
	}
	public void setControleMaterialPeer(ControleMaterialPeer controleMaterialPeer) {
		this.controleMaterialPeer = controleMaterialPeer;
	}

	// Comportamento
	public void marcarInicioArmazem(){
		 getControleMaterialPeer().marcarInicio( ConexaoVO.armazem, ConfiguracaoVO.dimensaoLimiteLog);
	}

	public void retornarInicioArmazem(){
		getControleMaterialPeer().retomarInicio( ConexaoVO.armazem);
	}

	public void lerLinha(){
		ControleMaterialVO.linha = getControleMaterialPeer().lerLinha( ConexaoVO.armazem);
		// System.out.println( "ControleMaterialVO.linha: "+ ControleMaterialVO.linha);
	}

	public void subtrairHorario() {
		ControleMaterialVO.horario = getControleMaterialPeer().subtrairHorario( ControleMaterialVO.linha, ConstituicaoVO.padraoDataHora);
		//System.out.println( "ControleMaterialVO.horario: "+ ControleMaterialVO.horario);
	}

	public void recuperarRelacao(){
		ValidacaoVO.relacao = getControleMaterialPeer().obterChaves( ConfiguracaoVO.historico);
		// System.out.println( "ValidacaoVO.relacao.length: "+ ValidacaoVO.relacao.length);
	}

	public void definirMomento(){
		System.out.println( "ValidacaoVO.relacao.length: ".concat( String.valueOf( ValidacaoVO.relacao.length)));
		ValidacaoVO.linha = ValidacaoVO.relacao[ ValidacaoVO.teste];
		System.out.println( "ValidacaoVO.linha: "+ ValidacaoVO.linha);
	}

	public void definirMomentoInicial(){
		ValidacaoVO.momentoInicial = getControleMaterialPeer().obterValor( ConfiguracaoVO.historico, ValidacaoVO.relacao[ ValidacaoVO.teste]).split(",")[0];
		System.out.println( "ValidacaoVO.momentoInicial: "+ ValidacaoVO.momentoInicial);
	}

	public void definirMomentoFinal(){
		ValidacaoVO.momentoFinal = getControleMaterialPeer().obterValor( ConfiguracaoVO.historico, ValidacaoVO.relacao[ ValidacaoVO.teste]).split(",")[1];
		System.out.println( "ValidacaoVO.momentoFinal: "+ ValidacaoVO.momentoFinal);
	}

	public void definirHorarioInicial(){
		ValidacaoVO.horario = getControleMaterialPeer().converterHorario( ValidacaoVO.momentoInicial, ConstituicaoVO.padraoDataHora);
		//System.out.println( "ValidacaoVO.horario (inicial): "+ ValidacaoVO.horario);
	}

	public void definirHorarioFinal(){
		ValidacaoVO.horario = getControleMaterialPeer().converterHorario( ValidacaoVO.momentoFinal, ConstituicaoVO.padraoDataHora);
		System.out.println( "ValidacaoVO.horario (final): "+ ValidacaoVO.horario);
	}

//	public void guardarMomento(){
//		ValidacaoVO.momentoGuardado = null; 
//	}

//	public void encontrarPeriodo(){	
//		ValidacaoVO.identificadorSessao = getControleMaterialPeer().encontrarPeriodo( ExecucaoVO.historico.split(",")[ ValidacaoVO.teste]);
//		//System.out.println( "ValidacaoVO.identificadorSessao: "+ ValidacaoVO.identificadorSessao);
//	}

	public void saltarTeste(){
		ValidacaoVO.teste = ValidacaoVO.teste+1;
		System.out.println( "ValidacaoVO.teste: "+ ValidacaoVO.teste);
	}

	public void zerarTeste(){
		ValidacaoVO.teste = 0;
		System.out.println( "ValidacaoVO.teste: "+ ValidacaoVO.teste);
	}

	public void definirIdSessao(){
		ValidacaoVO.identificadorSessao = ControleMaterialVO.linha.substring( ControleMaterialVO.linha.indexOf(" - ") + " - ".length(), ControleMaterialVO.linha.indexOf(" - ") + " - ".length()+32);
		System.out.println( "ValidacaoVO.identificadorSessao: {"+ ValidacaoVO.identificadorSessao +"}, ValidacaoVO.posicao: "+ ValidacaoVO.posicao);
	}

//	public void iniciarRelato(){
//		ValidacaoVO.relato = ControleMaterialVO.linha;
//		//System.out.println( "ValidacaoVO.log: "+ ValidacaoVO.log);
//	}

	public void definirStoring(){
		ConfiguracaoVO.evidenciador = ConstituicaoVO.chaveAlocacao;
		// System.out.println( "ConfiguracaoVO.evidenciador: "+ ConfiguracaoVO.evidenciador);
	}

	public void definirTransferido(){
		ConfiguracaoVO.evidenciador = ConstituicaoVO.chaveTransferencia;
		//System.out.println( "ConfiguracaoVO.evidenciador: "+ ConfiguracaoVO.evidenciador);
	}

	public void definirExcecao(){
		ConfiguracaoVO.evidenciador = ConstituicaoVO.chaveExcecao;
		//System.out.println( "ConfiguracaoVO.evidenciador: "+ ConfiguracaoVO.evidenciador);
	}

	public void definirComunicacaoCTI(){
		ConfiguracaoVO.evidenciador = ConstituicaoVO.chaveComunicacaoCTI;
		//System.out.println( "ConfiguracaoVO.evidenciador: "+ ConfiguracaoVO.evidenciador);
	}

	public void definirMensagemExcecao(){
		ConfiguracaoVO.evidenciador = ConstituicaoVO.chaveMensagemExcecao;
		//System.out.println( "ConfiguracaoVO.evidenciador: "+ ConfiguracaoVO.evidenciador);
	}

	public void definirCausaExcecao(){
		ConfiguracaoVO.evidenciador = ConstituicaoVO.chaveCausaExcecao;
		//System.out.println( "ConfiguracaoVO.evidenciador: "+ ConfiguracaoVO.evidenciador);
	}

	public void definirLocalExcecao(){
		ConfiguracaoVO.evidenciador = ConstituicaoVO.chaveLocalExcecao;
		//System.out.println( "ConfiguracaoVO.evidenciador: "+ ConfiguracaoVO.evidenciador);
	}

//	public void definirNoMatchCount(){
//		ConfiguracaoVO.evidenciador = ConstituicaoVO.chaveEngano;
//		System.out.println( "ConfiguracaoVO.evidenciador: "+ ConfiguracaoVO.evidenciador);
//	}

//	public void definirInterpretation(){
//		ConfiguracaoVO.evidenciador = ConstituicaoVO.chaveDado;
//		//System.out.println( "ConfiguracaoVO.evidenciador: "+ ConfiguracaoVO.evidenciador);
//	}

//	public void completarRelato(){
//		ValidacaoVO.relato = ValidacaoVO.relato + ControleMaterialVO.linha;
//		//System.out.println( "ValidacaoVO.log: "+ ValidacaoVO.log);
//
//	}

//	public void complementarRelatorio(){
//		ValidacaoVO.relatorio = ValidacaoVO.relatorio +","+ ValidacaoVO.trajetoria +"="+ ValidacaoVO.menu;
//		System.out.println( "ValidacaoVO.relatorio (complementar): "+ ValidacaoVO.relatorio);		
//	}

//	public void complementarRelatoNeutro(){
//		ValidacaoVO.relato = ValidacaoVO.relato +","+ ValidacaoVO.trajetoria +"=";
//		System.out.println( "ValidacaoVO.relato (complementar neutro): "+ ValidacaoVO.relato);
//	}

//	public void complementarRelatorioEntradaValida(){
//		ValidacaoVO.relatorio = ValidacaoVO.relatorio +",rastro={ENTRADA="+ ValidacaoVO.menu;
//		System.out.println( "ValidacaoVO.relatorio (entrada valida): "+ ValidacaoVO.relatorio);
//	}


//	public void iniciarDelatoLocalExcecao(){
//		ValidacaoVO.delato = ValidacaoVO.delato + ",local="+ ValidacaoVO.localExcecao;
//		System.out.println( "ValidacaoVO.delato (local excecao): "+ ValidacaoVO.delato);
//	}

//	public void delatarMensagemExcecao(){
//		ValidacaoVO.delato = ValidacaoVO.tipoExecao +"="+ ValidacaoVO.mensagemExcecao;
//		System.out.println( "ValidacaoVO.delato (local excecao): "+ ValidacaoVO.delato);
//	}

//	public void iniciarDelatoLinhaExcecao(){
//		ValidacaoVO.delato = ValidacaoVO.delato  +",linha="+ ValidacaoVO.LinhaExcecao;
//		System.out.println( "ValidacaoVO.delato (linha excecao): "+ ValidacaoVO.delato);
//	}

//	public void complementarDelatoExcecao(){
//		ValidacaoVO.delato = ValidacaoVO.delato +","+ ValidacaoVO.tipoExecao +"={";
//		System.out.println( "ValidacaoVO.delato (continuado delato execao): "+ ValidacaoVO.delato);
//	}

//	public void complementarRelatorioEntradaAbandono(){
//		ValidacaoVO.relatorio = ValidacaoVO.relatorio +",relato={ENTRADA=ABANDONO";
//		System.out.println( "ValidacaoVO.relatorio (entrada abandono): "+ ValidacaoVO.relatorio);
//	}

//	public void complementarRelatorioEquivoco(){
//		ValidacaoVO.relatorio = ValidacaoVO.relatorio +","+ ValidacaoVO.trajetoria +"=EQUIVOCO";
//		System.out.println( "ValidacaoVO.relatorio (complementar): "+ ValidacaoVO.relatorio);		
//	}

//	public void iniciarDelacaoExcecao(){
//		ValidacaoVO.delato = "posicao"+ ValidacaoVO.posicao +"="+ ValidacaoVO.mensagemExcecao;
//		System.out.println( "ValidacaoVO.delacao (entrada quebra): "+ ValidacaoVO.delato);
//	}
//	
//	public void complementarDelacaoExcecao(){
//		ValidacaoVO.delato = ValidacaoVO.delato +",posicao"+ ValidacaoVO.posicao +"="+ ValidacaoVO.mensagemExcecao;
//		System.out.println( "ValidacaoVO.delacao (entrada quebra): "+ ValidacaoVO.delato);
//	}
	
//	public void complementarRelatorioTransferido(){
//		ValidacaoVO.relatorio = ValidacaoVO.relatorio +","+ ValidacaoVO.trajetoria +"=TRANSFERIDO";
//		System.out.println( "ValidacaoVO.relatorio (transferido): "+ ValidacaoVO.relatorio);		
//	}

//	public void complementarRelatorioDelato(){
//		ValidacaoVO.relatorio = ValidacaoVO.relatorio +","+ ValidacaoVO.campoDelato +"={"+ ValidacaoVO.delato +"}";
//		System.out.println( "ValidacaoVO.relatorio (delato): "+ ValidacaoVO.relatorio);
//	}

//	public void complementarRelatorioRetido(){
//		ValidacaoVO.relatorio = ValidacaoVO.relatorio +","+ ValidacaoVO.trajetoria +"=RETIDO";
//		System.out.println( "ValidacaoVO.relatorio (retido): "+ ValidacaoVO.relatorio);		
//	}

//	public void complementarRelatorioExcedeu(){
//		ValidacaoVO.relatorio = ValidacaoVO.relatorio +","+ ValidacaoVO.trajetoria +"=EXCEDEU";
//		System.out.println( "ValidacaoVO.relatorio (excedido): "+ ValidacaoVO.relatorio);		
//	}
	
//	public void complementarRelatorioAbandonou(){
//		ValidacaoVO.relatorio = ValidacaoVO.relatorio +","+ ValidacaoVO.trajetoria +"=ABANDONOU";
//		System.out.println( "ValidacaoVO.relatorio (abandonou): "+ ValidacaoVO.relatorio);		
//	}
	
//	public void concatenarRelatorio(){
//		ValidacaoVO.relatorio = ValidacaoVO.relatorio + ValidacaoVO.digito;
//		System.out.println( "ValidacaoVO.relatorio (concatenar): "+ ValidacaoVO.relatorio);		
//	}

	public void prepararExpressaoDado(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveDado;
		System.out.println( "ValidacaoVO.expressaoRegular(Dado): "+ ValidacaoVO.expressaoRegular);
	}

	public void prepararExpressaoMenu(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveMenu;
		System.out.println( "ValidacaoVO.expressaoRegular(Menu): "+ ValidacaoVO.expressaoRegular);
	}

	public void prepararExpressaoDoisPontos(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.fimMenu;
		System.out.println( "ValidacaoVO.expressaoRegular(DoisPontos): "+ ValidacaoVO.expressaoRegular);
	}

	public void prepararExpressaoEngano(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveEngano;
		System.out.println( "ValidacaoVO.expressaoRegular(Engano): "+ ValidacaoVO.expressaoRegular);
	}

	public void prepararExpressaoOmissao(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveOmissao;
		System.out.println( "ValidacaoVO.expressaoRegular(Omissao): "+ ValidacaoVO.expressaoRegular);
	}

	public void prepararExpressaoVDNTransferencia(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveVDNTransferencia;
		System.out.println( "ValidacaoVO.expressaoRegular(VDN Transferencia): "+ ValidacaoVO.expressaoRegular);
	}

	public void prepararExpressaoLinhaTransferencia(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveLinhaTransferencia;
		System.out.println( "ValidacaoVO.expressaoRegular(VDN Linha Transferencia): "+ ValidacaoVO.expressaoRegular);
	}

	public void prepararExpressaoUUI(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveUUI;
		System.out.println( "ValidacaoVO.expressaoRegular(chave UUI): "+ ValidacaoVO.expressaoRegular);
	}

	public void prepararExpressaoCanal(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveCanal;
		System.out.println( "ValidacaoVO.expressaoRegular(canal): "+ ValidacaoVO.expressaoRegular);
	}
	
	public void prepararExpressaoMPP(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveMPP;
		System.out.println( "ValidacaoVO.expressaoRegular(mpp): "+ ValidacaoVO.expressaoRegular);
	}
	
	public void prepararExpressaoANI(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveANI;
		System.out.println( "ValidacaoVO.expressaoRegular(ANI): "+ ValidacaoVO.expressaoRegular);
	}
	
	public void prepararExpressaoDNIS(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveDNIS;
		System.out.println( "ValidacaoVO.expressaoRegular(DNIS): "+ ValidacaoVO.expressaoRegular);
	}
	
//	public void prepararExpressaoSeparador(){
//		ValidacaoVO.expressaoRegular = ConstituicaoVO.separadorData;
//		System.out.println( "ValidacaoVO.expressaoRegular(separdor): "+ ValidacaoVO.expressaoRegular);
//	}

	public void prepararExpressaoTransferencia(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveTransferencia;
		System.out.println( "ValidacaoVO.expressaoRegular(transferencia): "+ ValidacaoVO.expressaoRegular);
	}

	public void prepararExpressaoBilheteEnvio(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveBilheteSaida;
		System.out.println( "ValidacaoVO.expressaoRegular(bilhete saida): "+ ValidacaoVO.expressaoRegular);
	}

//	public void prepararExpressaoUUIEntrada(){
//		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveBilheteEntrada;
//		System.out.println( "ValidacaoVO.expressaoRegular(chave UUI Entrada): "+ ValidacaoVO.expressaoRegular);
//	}

	public void prepararExpressaoBilheteRecepcao(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveBilheteEntrada;
		System.out.println( "ValidacaoVO.expressaoRegular(bilhete entrada): "+ ValidacaoVO.expressaoRegular);
	}

	public void prepararExpressaoExcecao(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveExcecao;
		System.out.println( "ValidacaoVO.expressaoRegular(excecao): "+ ValidacaoVO.expressaoRegular);
	}

	public void prepararExpressaoCausaExcecao(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveCausaExcecao;
		System.out.println( "ValidacaoVO.expressaoRegular(causa excecao): "+ ValidacaoVO.expressaoRegular);
	}

	public void prepararExpressaoLocalExcecao(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveLocalExcecao;
		System.out.println( "ValidacaoVO.expressaoRegular(local excecao): "+ ValidacaoVO.expressaoRegular);
	}

	public void prepararExpressaoExcecaoServlet(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveServlet;
		System.out.println( "ValidacaoVO.expressaoRegular(servlet excecao): "+ ValidacaoVO.expressaoRegular);
	}

	public void prepararExpressaoMensagemExcecao(){
		ValidacaoVO.expressaoRegular = ConstituicaoVO.chaveMensagemExcecao;
		System.out.println( "ValidacaoVO.expressaoRegular(mensagem excecao): "+ ValidacaoVO.expressaoRegular);
	}

//	public void prepararExpressaoProximo(){
//		ValidacaoVO.expressaoRegular = ValidacaoVO.mPP;
//		System.out.println( "ValidacaoVO.expressaoRegular(proximo): "+ ValidacaoVO.expressaoRegular);
//	}

	public void guardarPosicaoInicial(){
		ValidacaoVO.posicaoInicial = ValidacaoVO.posicao;
		//System.out.println( "ValidacaoVO.posicaoInicial: "+ ValidacaoVO.posicaoInicial);
	}

	public void definirPosicaoDado(){
		ValidacaoVO.posicao = ValidacaoVO.posicao + ValidacaoVO.expressaoRegular.length();
		//System.out.println( "ValidacaoVO.posicao(definida): "+ ValidacaoVO.posicao);
	}

	public void restaurarPosicaoInicial(){
		ValidacaoVO.posicao = ValidacaoVO.posicaoInicial;
		System.out.println( "ValidacaoVO.posicao(restaurada): "+ ValidacaoVO.posicao);
	}

	public void restaurarUltimaPosicao(){
		ValidacaoVO.posicao = ValidacaoVO.ultimaPosicao;
		System.out.println( "ValidacaoVO.posicao(restaurada): "+ ValidacaoVO.posicao);
	}

	public void restaurarUltimaPosicaoDado(){
		ValidacaoVO.posicao = ValidacaoVO.ultimaPosicaoDado;
		System.out.println( "ValidacaoVO.posicao(restaurada dado): "+ ValidacaoVO.posicao);
	}

	public void restaurarUltimaPosicaoEngano(){
		ValidacaoVO.posicao = ValidacaoVO.ultimaPosicaoEngano;
		System.out.println( "ValidacaoVO.posicao(restaurada engano): "+ ValidacaoVO.posicao);
	}

	public void restaurarUltimaPosicaoOmissao(){
		ValidacaoVO.posicao = ValidacaoVO.ultimaPosicaoOmissao;
		System.out.println( "ValidacaoVO.posicao(restaurada omissao): "+ ValidacaoVO.posicao);
	}

	public void resgatarPosicaoLocalizada(){
		ValidacaoVO.posicao = ValidacaoVO.dossie.indexOf( ValidacaoVO.expressaoRegular, ValidacaoVO.posicao);
		System.out.println( "ValidacaoVO.posicao(resgatada): "+ ValidacaoVO.posicao);
	}

	public void zerarPosicao(){
		ValidacaoVO.posicao = 0;
		//System.out.println( "ValidacaoVO.posicao(zerada): "+ ValidacaoVO.posicao);
	}

	public void definirPosicaoMenu(){
		ValidacaoVO.posicaoMenu = ValidacaoVO.posicao;
		System.out.println( "ValidacaoVO.posicaoMenu: "+ ValidacaoVO.posicaoMenu);
	}

	public void resguardarPosicaoUltimoMenu(){
		ValidacaoVO.posicaoUltimoMenu = ValidacaoVO.posicaoMenu;
		System.out.println( "ValidacaoVO.posicaoUltimoMenu: "+ ValidacaoVO.posicaoUltimoMenu);
	}
	
	public void resguardarUltimaPosicao(){
		ValidacaoVO.ultimaPosicao = ValidacaoVO.posicao;
		System.out.println( "ValidacaoVO.ultimaPosicao (resguardar): "+ ValidacaoVO.ultimaPosicao);
	}

	public void zerarUltimaPosicao(){
		ValidacaoVO.ultimaPosicao = 0;
		//System.out.println( "ValidacaoVO.posicao(zerada): "+ ValidacaoVO.posicao);
	}

	public void resguardarUltimaPosicaoOmissao(){
		ValidacaoVO.ultimaPosicaoOmissao = ValidacaoVO.posicao;
		System.out.println( "ValidacaoVO.ultimaPosicaoOmissao (resguardar): "+ ValidacaoVO.ultimaPosicaoOmissao);
	}
	public void zerarUltimaPosicaoOmissao(){
		ValidacaoVO.ultimaPosicaoOmissao = 0;
		//System.out.println( "ValidacaoVO.posicao(zerada): "+ ValidacaoVO.posicao);
	}

	public void resguardarUltimaPosicaoEngano(){
		ValidacaoVO.ultimaPosicaoEngano = ValidacaoVO.posicao;
		System.out.println( "ValidacaoVO.ultimaPosicaoEngano (resguardar): "+ ValidacaoVO.ultimaPosicaoEngano);
	}

	public void zerarUltimaPosicaoEngano(){
		ValidacaoVO.ultimaPosicaoEngano = 0;
		//System.out.println( "ValidacaoVO.posicao(zerada): "+ ValidacaoVO.posicao);
	}

	public void resguardarUltimaPosicaoDado(){
		ValidacaoVO.ultimaPosicaoDado = ValidacaoVO.posicao;
		System.out.println( "ValidacaoVO.ultimaPosicaoDado (resguardar): "+ ValidacaoVO.ultimaPosicaoDado);
	}

	public void zerarUltimaPosicaoDado(){
		ValidacaoVO.ultimaPosicaoDado = 0;
		//System.out.println( "ValidacaoVO.posicao(zerada): "+ ValidacaoVO.posicao);
	}

	public void definirDigito(){
		ValidacaoVO.digito = ValidacaoVO.dado;
		System.out.println( "ValidacaoVO.digito: "+ ValidacaoVO.digito);
	}

	public void definirExcesso(){
		ValidacaoVO.digito = "E";
		// System.out.println( "ValidacaoVO.digito: "+ ValidacaoVO.digito);
	}

	public void zerarDigito(){
		ValidacaoVO.digito = null;
		System.out.println( "ValidacaoVO.digito (zerar): "+ ValidacaoVO.digito);
	}
	
//	public void definirMensagem(){
//		ValidacaoVO.mensagemExcecao = ValidacaoVO.dado;
//		System.out.println( "ValidacaoVO.mensagem: "+ ValidacaoVO.mensagemExcecao);
//	}

	public void definirMenu(){
		ValidacaoVO.menu = ValidacaoVO.dossie.substring( ValidacaoVO.ultimaPosicao+1, ValidacaoVO.posicao);
		System.out.println( "ValidacaoVO.menu: "+ ValidacaoVO.menu);
	}

	public void zerarMenu(){
		ValidacaoVO.menu = null;
		System.out.println( "ValidacaoVO.menu (zerar): "+ ValidacaoVO.menu);
	}

	public void resguardarUltimoMenu(){
		ValidacaoVO.ultimoMenu = ValidacaoVO.menu;
		System.out.println( "ValidacaoVO.ultimoMenu: "+ ValidacaoVO.ultimoMenu);
	}

	public void zerarUltimoMenu(){
		ValidacaoVO.ultimoMenu = null;
		System.out.println( "ValidacaoVO.ultimoMenu (zerar): "+ ValidacaoVO.ultimoMenu);
	}

	public void definirValidador(){
		ValidacaoVO.validador = ValidacaoVO.dado;
		System.out.println( "ValidacaoVO.validador: "+ ValidacaoVO.validador);
	}

	public void zerarValidador(){
		ValidacaoVO.validador = null;
		System.out.println( "ValidacaoVO.validador(zerar): "+ ValidacaoVO.validador);
	}

	public void resgatarDado(){
		ValidacaoVO.dado = ValidacaoVO.dossie.substring( ValidacaoVO.posicao, ValidacaoVO.dossie.indexOf( ConstituicaoVO.fimValor, ValidacaoVO.posicao));
		System.out.println( "ValidacaoVO.dado (resgatado): "+ ValidacaoVO.dado);
	}

//	public void restaurarUltimaPosicaoMenu(){
//		ValidacaoVO.posicao = ValidacaoVO.posicaoUltimoMenu;
//		System.out.println( "ValidacaoVO.posicao(restaurada menu): "+ ValidacaoVO.posicao);
//	}
	
//
//	public void resguardarUltimaPosicaoOpcaoFornecida(){
//		ValidacaoVO.ultimaPosicaoOpcaoFornecida = ValidacaoVO.posicao;
//		System.out.println( "ValidacaoVO.ultimaPosicaoOpcaoFornecida (resguardar): "+ ValidacaoVO.ultimaPosicaoOpcaoFornecida);
//	}

//	public void restaurarUltimaPosicaoOpcaoFornecida(){
//		ValidacaoVO.posicao = ValidacaoVO.ultimaPosicaoOpcaoFornecida;
//		System.out.println( "ValidacaoVO.posicao(restaurada opcao fornecida): "+ ValidacaoVO.posicao);
//	}

	public void iniciarDossie(){
		ValidacaoVO.dossie = ControleMaterialVO.linha + "\n";
		//System.out.println( "ValidacaoVO.dossie (iniciar): "+ ValidacaoVO.dossie);
	}

	public void completarDossie(){
		ValidacaoVO.dossie = ValidacaoVO.dossie + ControleMaterialVO.linha + "\n";
		//System.out.println( "ConfiguracaoVO.evidenciador: "+ ConfiguracaoVO.evidenciador);
		//System.out.println( "ValidacaoVO.dossie (completar): "+ ValidacaoVO.dossie);		
		//System.out.println( "ControleMaterialVO.linha: "+ ControleMaterialVO.linha);
	}

	public void escreverDossie(){
		// getControleMaterialPeer().escreverConteudo( ValidacaoVO.fwtEscritor, ConfiguracaoVO.local.concat( SimuladorVO.subsistema).concat("/").concat( ConfiguracaoVO.produto), ValidacaoVO.dossie);
		getControleMaterialPeer().escreverConteudo( ValidacaoVO.fwtEscritor, ConfiguracaoVO.caminho.concat( ConfiguracaoVO.produto), ValidacaoVO.dossie);
	}

	public void iniciarProduto(){
		ValidacaoVO.produtoMap = new HashMap<String,HashMap<String,HashMap<String,String>>>();
		System.out.println( "ValidacaoVO.produtoMap (iniciado): "+ ValidacaoVO.produtoMap);
	}
	
	public void complementarProduto(){
		ValidacaoVO.produtoMap.put( ValidacaoVO.linha, ReportagemVO.relatorioMap);
		System.out.println( "ValidacaoVO.produtoMap (complementado): "+ ValidacaoVO.produtoMap);
	}

}

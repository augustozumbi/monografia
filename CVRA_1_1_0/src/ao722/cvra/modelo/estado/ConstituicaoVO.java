package ao722.cvra.modelo.estado;

public class ConstituicaoVO {

	// Propriedades
	public static final String INICIADO = "iniciado", SUSPENSO = "suspenso", INTERROMPIDO = "interrompido"; // estado
	public static String saudacao, entrada, omissao, desacerto, repeticao, retorno; // rodada
	public static String insucesso, transferencia, desconexao; // desfecho
	public static String excedido, transferido, abandonou, desconectado; // situacao
	public static String comandos, desfechos, chaves, campos; // composicao
	public static String fimValor, fimMenu;
	public static String chaveAlocacao, chaveDado, chaveMenu, chaveOmissao, chaveEngano, chaveTransferencia, chaveAbandono, chaveCanal, chaveMPP, chaveANI, chaveDNIS, chaveExcecao, chaveCausaExcecao, chaveLocalExcecao, chaveMensagemExcecao, chaveServlet, fimMensagemExcecao, chaveComunicacaoCTI, fimVDNTransferencia, chaveVDNTransferencia, chaveBilheteSaida, chaveUUI, chaveBilheteEntrada, fimUUISaida, fimUUIEntrada, chaveLinhaTransferencia;
	public static String padraoDataHora, separadorData;
	public static String peerName, ctiLink, login, password;

}

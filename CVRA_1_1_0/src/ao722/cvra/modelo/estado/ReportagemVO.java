package ao722.cvra.modelo.estado;

import java.util.HashMap;

public class ReportagemVO {

	// Atributos
	public static String campoDataHoraInicial="dataHoraComeco", campoDataHoraFinal="dataHoraFim", campoDuracao="duracao", campoIdentificacaoAutomatica="identificacao", campoVDNURA="DNIS", campoCanal="canal", campoSessaoAppServer="sessaoAppServer", campoSessaoMPP="sessaoMPP", campoSinistro="sinistro", campoTrajetoria="trajetoria", campoDesfecho="desfecho", campoRelato="relato", campoDelato="delato", campoVDNTransferencia="VDNTransferencia", campoBilheteSaida="bilheteSaida", campoBilheteEntrada="bilheteEntrada", campoResumo="resumo", campoPlataforma="plataforma", campoDerivacao="derivacao"; 
	public static String dataHoraInicio, dataHoraTermino, duracao, ramalAlocado, sessao, sessaoMPP, desfecho, chamador, numeroChamado, excecao;
	public static String relato, relatorio, delato, resumo, plataforma, derivacao;
	public static String trajetoria, canal, mPP, aNI, dNIS, mensagemExcecao, tipoExecao, localExcecao, linhaExcecao, vDNTransferencia, uUI, uUISaida, uUIEntrada;
	public static HashMap<String,String> resumoMap, relatoMap, derivacaoMap, plataformaMap, delatoMap;
	public static HashMap<String,HashMap<String,String>> relatorioMap;

}

package ao722.cvra.modelo.estado;

import javax.telephony.Address;
import javax.telephony.Call;
import javax.telephony.Connection;
import javax.telephony.JtapiPeer;
import javax.telephony.Provider;
import javax.telephony.Terminal;
import javax.telephony.TerminalConnection;
import javax.telephony.media.MediaTerminalConnection;

public class IntegracaoTelefonicaVO {

	// Propriedades/Atributos
	public static int numeroLigado;
	public static Call chamada;
	public static MediaTerminalConnection mediaTerminalConnection;
	public static TerminalConnection[] vinculacao;
	public static String numeroDestino, numeroOrigem = ConfiguracaoVO.extensao, dTMF, expressaoIntegrador;
	public static Connection[] ligacao;
	public static Address origem;
	public static Address[] aparelhagem;
	public static Terminal chamador;
	public static Provider integrador;
	public static String[] catalogo;
	public static JtapiPeer peer;	
	public static Connection ligador;

}

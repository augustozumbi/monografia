package ao722.cvra.contrato.fornecedor;

import javax.telephony.Address;
import javax.telephony.Call;
import javax.telephony.Connection;
import javax.telephony.InvalidArgumentException;
import javax.telephony.InvalidPartyException;
import javax.telephony.InvalidStateException;
import javax.telephony.JtapiPeer;
import javax.telephony.JtapiPeerUnavailableException;
import javax.telephony.MethodNotSupportedException;
import javax.telephony.PrivilegeViolationException;
import javax.telephony.Provider;
import javax.telephony.ResourceUnavailableException;
import javax.telephony.Terminal;
import javax.telephony.TerminalConnection;
import javax.telephony.media.MediaTerminalConnection;

import ao722.cvra.contrato.servico.IIntegracaoTelefonica;

public class IntegracaoTelefonicaAESPeer
	implements IIntegracaoTelefonica {

	@Override
	public void desligar( Object chamada){
		Call call = (Call)chamada;

		try {
			call.getConnections()[0].disconnect();
		} 
		catch (PrivilegeViolationException e) {
			System.err.println( "IntegracaoTelefonicaPeer.desligar(): "+ e);
		} 
		catch (ResourceUnavailableException e) {
			System.err.println( "IntegracaoTelefonicaPeer.desligar(): "+ e);
		} 
		catch (MethodNotSupportedException e) {
			System.err.println( "IntegracaoTelefonicaPeer.desligar(): "+ e);
		} 
		catch (InvalidStateException e) {
			System.err.println( "IntegracaoTelefonicaPeer.desligar(): "+ e);
		}

	}

	@Override
	public void gerarTom( Object canalAudio, String dtmf){
		MediaTerminalConnection mediaTerminalConnection = (MediaTerminalConnection)canalAudio;

		try {
			mediaTerminalConnection.generateDtmf( dtmf);
		} 
		catch (MethodNotSupportedException e) {
			System.err.println( "IntegracaoTelefonicaPeer.gerarTom(): "+ e);
		} 
		catch (ResourceUnavailableException e) {
			System.err.println( "IntegracaoTelefonicaPeer.gerarTom(): "+ e);
		} 
		catch (InvalidStateException e) {
			System.err.println( "IntegracaoTelefonicaPeer.gerarTom(): "+ e);
		}

	}

	@Override
	public Object definirMidia( Object ligador){
		Connection ligadorLocal = (Connection)ligador;
		return ligadorLocal.getTerminalConnections()[ 0];
	}

	@Override
	public TerminalConnection[] definirVinculacao( Object ligador){
		Connection ligadorLocal = (Connection)ligador; 
		return ligadorLocal.getTerminalConnections();
	}

	@Override
	public String definirNumeroVinculado( Object ligacao, int numeroLigado){
		Connection[] ligacaoLocal = (Connection[])ligacao; 
		return ligacaoLocal[ numeroLigado].getAddress().getName();
	}

	@Override
	public Connection[] definirLigacao( Object chamada){
		Call call = (Call)chamada;
		return call.getConnections();
	}

	@Override
	public Connection[] ligar( Call chamada, Terminal chamador, Address origem, String destino) {

		try{
			return chamada.connect( chamador, origem, destino);
		} 
		catch (ResourceUnavailableException e) {
			System.err.println( "ResourceUnavailableException: "+ e);
			e.printStackTrace();
		} 
		catch (PrivilegeViolationException e) {
			System.err.println( "PrivilegeViolationException: "+ e);
			e.printStackTrace();
		} 
		catch (InvalidPartyException e) {
			System.err.println( "InvalidPartyException: "+ e);
			e.printStackTrace();
		} 
		catch (InvalidArgumentException e) {
			System.err.println( "InvalidArgumentException: "+ e);
			e.printStackTrace();
		}
		catch (InvalidStateException e) {
			System.err.println( "InvalidStateException: "+ e);
			e.printStackTrace();
		} 
		catch (MethodNotSupportedException e) {
			System.err.println( "MethodNotSupportedException: "+ e);
			e.printStackTrace();
		}
		catch(Exception e){
			System.err.println( "MethodNotSupportedException: "+ e);
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public Call definirChamada( Provider integrador){

		try{
			return integrador.createCall();
		} 
		catch (ResourceUnavailableException e) {
			System.err.println( "ResourceUnavailableException: "+ e);
			e.printStackTrace();
		} 
		catch (InvalidStateException e) {
			System.err.println( "InvalidStateException: "+ e);
			e.printStackTrace();
		} 
		catch (PrivilegeViolationException e) {
			System.err.println( "PrivilegeViolationException: "+ e);
			e.printStackTrace();
		} 
		catch (MethodNotSupportedException e) {
			System.err.println( "MethodNotSupportedException: "+ e);
			e.printStackTrace();
		}


		return null;
	}

	@Override
	public Address[] definirAparelhagem( Provider integrador){

		try{
			return integrador.getAddresses();
		} 
		catch (com.avaya.jtapi.tsapi.TsapiPlatformException e){
			System.out.println("IntegracaoTelefonicaPeer.definirAparelhagem(): "+ e);
		}
		catch (ResourceUnavailableException e) {
			System.err.println( "ResourceUnavailableException: "+ e);
			e.printStackTrace();
		}
		

		return null;
	}

	@Override
	public Terminal definirChamador( Provider integrador, String extensao){

		try {
			return integrador.getTerminal( extensao);
		} 
		catch (InvalidArgumentException e) {
			System.err.println( "IntegracaoTelefonicaPeer.definirChamador(): "+ e);
		}

		return null;
	}

	@Override
	public Provider definirIntegrador( JtapiPeer peer, String expressaoIntegrador){
		return peer.getProvider( expressaoIntegrador);
	}

	@Override
	public String[] definirCatalogo( JtapiPeer peer){
		return peer.getServices();
	}

	@Override
	public JtapiPeer definirInstancia( String peerName){

		try{
			return javax.telephony.JtapiPeerFactory.getJtapiPeer( peerName);
		} 
		catch ( JtapiPeerUnavailableException e) {
			System.err.println( "IntegracaoTelefonicaPeer.definirInterface(): "+ e);
			e.printStackTrace();
		}

		return null;

	}

	@Override
	public Address definirAparelho( Provider integrador, String numero) {
		
		try{
			return integrador.getAddress( numero);
		}
		catch( Exception e){
			System.err.println("IntegracaoTelefonicaPeer.definirAparelho(): "+ e);
		}
		
		return null;
	}

}

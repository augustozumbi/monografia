package ao722.cvra.contrato.fornecedor;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import ao722.cvra.contrato.servico.IIntegracaoWeb;

public class IntegracaoWebPeer
	implements IIntegracaoWeb {

	@Override
	public java.io.BufferedReader receptarBytes( java.net.HttpURLConnection urlConnection) {
		//setLeitor( new java.io.BufferedReader( new java.io.InputStreamReader( getUrlConnection().getInputStream())));

		try{
//			IntegracaoWebVO.leitor = new java.io.BufferedReader( new java.io.InputStreamReader( IntegracaoWebVO.urlConnection.getInputStream()));
			return new java.io.BufferedReader( new java.io.InputStreamReader( urlConnection.getInputStream()));
		} 

		catch( IOException e) {
			System.err.println( "IntegracaoWebPeer.receptarBytes(): "+ e);
		}
		catch( Exception e) {
			System.err.println( "IntegracaoWebPeer.receptarBytes(): "+ e);
		} 
		catch( Throwable e) {
			System.err.println( "IntegracaoWebPeer.receptarBytes(): "+ e);
		}

		return null; 

//		String tmpConteudo = null;
//		while( ( tmpConteudo = getLeitor().readLine()) != null){
//			setConteudoReceptado( getConteudoReceptado() + tmpConteudo);
//		}
//		setConteudoReceptado( getLeitor().readLine());
	}

	@Override
	public void encerrarAcesso( java.io.BufferedReader leitor, java.net.HttpURLConnection urlConnection, java.io.InputStream canal) {
		//getLeitor().close();
		try{
			leitor.close();
			urlConnection.disconnect();
		}
		catch(java.io.IOException e){
			System.err.println("ExecucaoDAO.encerrar(): "+ e);
		}

		//getUrlConnection().disconnect();
//		IntegracaoWebVO.urlConnection.disconnect();
	}

	@Override
	public java.net.HttpURLConnection acessarURL( java.net.URL logURL) {
		//setLocalizacao( pUrl);
		//ExecucaoVO.localizacao = ConfiguracaoVO.logURL;
		//setUrlConnection( (java.net.HttpURLConnection)getUrl().openConnection());

		try {
			return ( java.net.HttpURLConnection)logURL.openConnection();
		}
		catch( IOException e) {
			System.err.println( "ExecucaoDAO.acessarURL(): "+ e);
		}
		catch( Exception e){
			System.err.println("IntegracaoWebPeer.acessarURL(): "+ e);
		}

		return null;
	}

	@Override
	public java.net.URL criarLink( String logURL) {
		// setUrl( new java.net.URL( getLocalizacao()));

		try {
			return new java.net.URL( logURL);
		} 
		catch ( MalformedURLException e) {
			System.err.println( "ExecucaoDAO.acessar(): "+ e);
		}

		return null;

	}

//	@Override
//	public String imprimirLog( java.io.BufferedReader brdLeitor, java.io.FileWriter fwtEscritor, String linha, String produto) {

//		String tmpConteudo = null;
//		try {
//			java.io.InputStream ipsProperties = null;
//			ipsProperties = new java.io.FileInputStream( ConfiguracaoVO.caminho.concat( pCaminhoArquivo));
//			
//			while( ( tmpConteudo = IntegracaoWebVO.leitor.readLine()) != null){
//				setConteudoReceptado( getConteudoReceptado() + tmpConteudo);
//				System.out.println( tmpConteudo);
//			}
//
//		} 
//		catch (IOException e) {
// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

//		try{
//			IntegracaoWebVO.fwtEscritor = new java.io.FileWriter( ConfiguracaoVO.local.concat("../teste/").concat( ConfiguracaoVO.produto), true);
//
//			while( ( IntegracaoWebVO.linha = IntegracaoWebVO.armazem.readLine()) != null){
//				IntegracaoWebVO.fwtEscritor.write( IntegracaoWebVO.linha.concat("\n"));
//			}
//
//			IntegracaoWebVO.fwtEscritor.close( );
//		}
//		catch( IOException e){	}
//		catch( Exception e){	}
//
//		return null;
//	}

	@Override
	public InputStream criarCanal(HttpURLConnection urlConnection) {

		try{
			return urlConnection.getInputStream();
		}
		catch( java.io.FileNotFoundException e){
			System.err.println( "IntegracaoWebPeer.criarCanal(): "+ e);
		}
		catch( Exception e){
			System.err.println( "IntegracaoWebPeer.criarCanal(): "+ e);
		}

		return null;
	}

}

package ao722.cvra.contrato.fornecedor;

import java.io.BufferedReader;
//import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

//import javax.swing.JFileChooser;

import ao722.cvra.contrato.servico.IControleMaterial;
import ao722.cvra.modelo.estado.ConfiguracaoVO;

public class ControleMaterialPeer
	implements IControleMaterial {

	@Override
	public String obterValor( String pCaminhoArquivo, String pChave) {
		String strValor = "";
		java.util.Properties prpScriptPadrao = new java.util.Properties();
		java.io.InputStream ipsProperties = null;

		try{
			System.out.println( "ConfiguracaoVO.caminho: ".concat(ConfiguracaoVO.caminho));
			ipsProperties = new java.io.FileInputStream( ConfiguracaoVO.caminho.concat( pCaminhoArquivo));
			prpScriptPadrao.load( ipsProperties);
			strValor = (String)prpScriptPadrao.get( pChave);
		}
		catch( FileNotFoundException e){ 
			System.out.println( e);
			strValor = null;
		}
		catch( IOException e){
			System.out.println( e);
			strValor = null;
		}
		catch( Exception e){
			System.out.println( e);
			strValor = null;
		}

		boolean blnValorEncontrado = (strValor != null); 
		if( blnValorEncontrado ){ }
		else {
			strValor = "nada";
		}

		return strValor;

	}

	@Override
	public String[] obterChaves( String pArquivo){
		java.util.Enumeration<Object> enumeration = null;
		String[] strChaves = null /*, strValores = null */;

		java.util.Properties properties = new java.util.Properties();
		java.io.InputStream inputStream = null;
		
		try{
			inputStream = new java.io.FileInputStream( ConfiguracaoVO.caminho.concat( pArquivo));
			properties.load( inputStream);
			enumeration = properties.keys();
			strChaves = new String[properties.size()];

			int i = 0;
			while( enumeration.hasMoreElements()){
				strChaves[ i] = ( String)enumeration.nextElement();
				System.out.println("strChaves["+ i +"]:"+ strChaves[ i]);
				i = i+1;
			}

		}
		catch( FileNotFoundException e){ 
			System.out.println( e);
			strChaves = null;
		}
		catch( IOException e){
			System.out.println( e);
			strChaves = null;
		}
		catch( Exception e){
			System.out.println( e);
			strChaves = null;
		}

		return strChaves;
	}

	@Override
	public String encontrarPeriodo( String marco) {
		
		try{
			
		}
		catch( Exception e){
			
		}

		return null;
	}

	@Override
	public Object abrirArquivo( String nomeArquivo) {
		return new java.io.File( nomeArquivo);
	}

	@Override
	public String lerLinha( java.io.BufferedReader brdLeitor) {

		try{
			return brdLeitor.readLine();
		}
		catch( java.io.IOException e){
			System.err.println("ControleMaterialPeer.lerLinha(): "+ e);
		}
		catch( Exception e){
			System.err.println("ControleMaterialPeer.lerLinha(): "+ e);
		}

		return null;
	}

	@Override
	public void escreverConteudo( FileWriter fwtEscritor, String arquivoDestino, String conteudo) {
		System.out.println( "escreverConteudo: "+ arquivoDestino);
		
		try{
			fwtEscritor = new java.io.FileWriter( arquivoDestino, true);
			fwtEscritor.write( conteudo);
			fwtEscritor.close();
			fwtEscritor = null;
			System.out.println("Arquivo "+ arquivoDestino +" gravado com sucesso");
		}
		catch( IOException e){
			System.err.println("ControleMaterialPeer.escreverRelatorio(): "+ e);
		}
		catch( Exception e){	
			System.err.println("ControleMaterialPeer.escreverRelatorio(): "+ e);
		}

	}

	public String obterConfiguracao( String pCaminhoArquivo, String pChave) {
		String strValor = "";
		java.util.Properties prpScriptPadrao = new java.util.Properties();
		java.io.InputStream ipsProperties = null;

		try{
			ipsProperties = new java.io.FileInputStream( ConfiguracaoVO.local.concat( pCaminhoArquivo));
			prpScriptPadrao.load( ipsProperties);
			strValor = (String)prpScriptPadrao.get( pChave);
		}
		catch( FileNotFoundException e){ 
			System.out.println( e);
			strValor = null;
		}
		catch( IOException e){
			System.out.println( e);
			strValor = null;
		}
		catch( Exception e){
			System.out.println( e);
			strValor = null;
		}

		boolean blnValorEncontrado = (strValor != null); 
		if( blnValorEncontrado ){ }
		else {
			strValor = "nada";
		}

		return strValor;

	}

	@Override
	public Date subtrairHorario( String linha, String padrao) {

		try {
			return new java.text.SimpleDateFormat( padrao).parse( linha.substring(0,19));
		} 
		catch (ParseException e) {
			System.err.println("ControleMaterialPeer.subtrairHorario(): "+ e);
		}			
		catch( Exception e){
			System.err.println("ControleMaterialPeer.subtrairHorario(): "+ e);
		}
		
		return null;
	}

	@Override
	public java.util.Date converterHorario(String dataHora, String padrao) {

		try {
			return new java.text.SimpleDateFormat( padrao).parse( dataHora);
		} 
		catch (ParseException e) {
			System.err.println("ControleMaterialPeer.converterHorario(): "+ e);
		}
		catch( Exception e){
			System.err.println("ControleMaterialPeer.converterHorario(): "+ e);
		}
		
		return null;
	}

	@Override
	public void marcarInicio( BufferedReader brdLeitor, int intDimensaoLimite) {

		try{
			brdLeitor.mark( intDimensaoLimite);
		}
		catch( IllegalArgumentException e){
			System.err.println("ControleMaterialPeer.marcarInicio(): "+ e);
		}
		catch( IOException e){
			System.err.println("ControleMaterialPeer.marcarInicio(): "+ e);
		}
		catch( Exception e){
			System.err.println("ControleMaterialPeer.marcarInicio(): "+ e);
		}

	}

	@Override
	public void retomarInicio( BufferedReader brdLeitor) {
		
		try{
			brdLeitor.reset();
		}
		catch( IOException e){
			System.err.println("ControleMaterialPeer.retomarInicio(): "+ e);
		}
		catch( Exception e){
			System.err.println("ControleMaterialPeer.retomarInicio(): "+ e);
		}
		
	}

	@Override
	public String[] obterValores( String pArquivo, String[] pArrValores) {
		//java.util.Enumeration<Object> enumeration = null;
		String[] strValores = null /*, strValores = null */;

		java.util.Properties properties = new java.util.Properties();
		java.io.InputStream inputStream = null;
		
		try{
			inputStream = new java.io.FileInputStream( ConfiguracaoVO.caminho.concat( pArquivo));
			properties.load( inputStream);
			strValores = new String[properties.size()];

			for( int i = 0; i< pArrValores.length; i++){
				strValores[i] = properties.getProperty( pArrValores[i]);
				System.out.println( "strValores["+ i +"]: "+ strValores[i]);
			}

//			while( enumeration.hasMoreElements()){
//				strChaves[ i] = ( String)enumeration.nextElement();
//				System.out.println("strChaves["+ i +"]:"+ strChaves[ i]);
//				i = i+1;
//			}

		}
		catch( FileNotFoundException e){ 
			System.out.println( e);
			strValores = null;
		}
		catch( IOException e){
			System.out.println( e);
			strValores = null;
		}
		catch( Exception e){
			System.out.println( e);
			strValores = null;
		}

		return strValores;
	}

	@Override
	public Object obterArquivoEscolhido(javax.swing.JFileChooser selecionadorArquivo) {
		// TODO Auto-generated method stub
		javax.swing.JFileChooser jfc = (javax.swing.JFileChooser)selecionadorArquivo;
		return jfc.getSelectedFile();
	}

	@Override
	public char[] lerConteudo( Object arquivo) {
		// TODO Auto-generated method stub
		java.io.File filArquivo = (java.io.File)arquivo;
		char[] string = new char[(int)filArquivo.length()]; 
		java.io.BufferedReader leitor = null; 
		
		try {
			leitor = new java.io.BufferedReader( new java.io.InputStreamReader( new java.io.FileInputStream( ((java.io.File)filArquivo).getAbsolutePath())));
			leitor.read( string, 0, (int)filArquivo.length());
			leitor.close();
			leitor = null;
			return string;
		} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch( Exception e){
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public boolean limparArquivo( String caminhoArquivo) {
		try{
			System.out.println( caminhoArquivo);
			System.out.println( new java.io.File( caminhoArquivo).delete());
			System.out.println( new java.io.File( caminhoArquivo).createNewFile());
			return true;
		}
		catch( Exception e){
			System.out.println( e);
		}
		
		return false;
	}
	
}

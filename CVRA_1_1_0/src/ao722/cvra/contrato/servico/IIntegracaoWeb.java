package ao722.cvra.contrato.servico;

public interface IIntegracaoWeb {

	/* Descricao: Recepcao e alocacao dos dados do conteudo baixado */
	public java.io.InputStream criarCanal( java.net.HttpURLConnection urlConnection);
	
	/* Descricao: Recepcao e alocacao dos dados do conteudo baixado */
	public java.io.BufferedReader receptarBytes( java.net.HttpURLConnection urlConnection);

	/* Descricao: Desconexao de um localizador padrao de recurso */
	public void encerrarAcesso( java.io.BufferedReader leitor, java.net.HttpURLConnection urlConnection, java.io.InputStream canal);
	/* Descricao: Conexao com um localizador padrao do recurso*/
	public java.net.HttpURLConnection acessarURL( java.net.URL logURL);

	/* Descricao: Criar link de hipertexto para conexao */
	public java.net.URL criarLink( String logURL);

	/* Descricao: Imprime log da chamada */
//	public String imprimirLog(  java.io.BufferedReader brdLeitor, java.io.FileWriter fwtEscritor, String linha, String produto);

}

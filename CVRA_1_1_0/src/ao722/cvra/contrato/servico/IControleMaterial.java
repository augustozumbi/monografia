package ao722.cvra.contrato.servico;

public interface IControleMaterial {

	/* Descricao: Obter valor de propriedade */
	public abstract String obterValor( String pCaminhoArquivo, String pChave);

	/* Descricao: Obter nomes de propriedades */
	public abstract String[] obterChaves( String pArquivo);

	/* Descricao: Obter nomes de propriedades */
	public abstract String[] obterValores( String pArquivo, String[] pArrValores);

	/* Descricao: Log */
	public abstract Object abrirArquivo( String nomeArquivo);
	
	/* Descricao: Encontrar periodo */
	public abstract String encontrarPeriodo( String marco);
	
	/* Descricao: Encontrar Id da ligacao */
	public abstract String lerLinha( java.io.BufferedReader brdLeitor);
	
	/* Descricao: Escrever relatorio de evidencias */
	public abstract void escreverConteudo( java.io.FileWriter fwtEscritor, String arquivoDestino, String conteudo);

	/* Descricao: Obter valor de parametros de configuracao */
	public abstract String obterConfiguracao( String pCaminhoArquivo, String pChave);
	
	/* Descricao: Extrai o valor de data hora de uma linha de log ja com o tipo data  */
	public abstract java.util.Date subtrairHorario( String linha, String padrao);

	/* Descricao: Converte o valor em data hora */
	public abstract java.util.Date converterHorario( String dataHora, String padrao);
	
	/* Descricao: Encontrar Id da ligacao */
	public abstract void marcarInicio( java.io.BufferedReader brdLeitor, int intDimensaoLimite);
	
	/* Descricao: Encontrar Id da ligacao */
	public abstract void retomarInicio( java.io.BufferedReader brdLeitor);

	/* Descricao: Devolver arquivo selecionado */
	public abstract Object obterArquivoEscolhido( javax.swing.JFileChooser selecionadorArquivo);
	
	/* Descricao: Obter o leitor do arquivo */
	public abstract char[] lerConteudo( Object arquivo);

	/* Descricao: Limpeza de arquivos */
	public abstract boolean limparArquivo( String caminhoArquivo);
}

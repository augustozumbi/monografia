package ao722.cvra.contrato.servico;

import javax.telephony.Address;
import javax.telephony.Call;
import javax.telephony.JtapiPeer;
import javax.telephony.Provider;
import javax.telephony.Terminal;

public interface IIntegracaoTelefonica {

	/* Descricao: Realiza a desconexao da chamada */
//	public void desligar( Call call);
	public void desligar( Object chamada);

	/* Descricao: Executar a teclagem de um dos botoes de um teclado telefonico, requerendo ao CTI a transmissao do tom correspondente */
//	public void gerarTom( MediaTerminalConnection mediaTerminalConnection, String dtmf);
	public void gerarTom( Object canalAudio, String dtmf);

	/* Descricao: Definir midia da conexao-terminal */
//	public MediaTerminalConnection definirMidia( Connection ligador);
	public Object definirMidia( Object ligador);

	/* Descricao: Definicao do procedimento de teste a ser executado */
//	public javax.telephony.TerminalConnection[] definirVinculacao( Connection ligador);
	public Object definirVinculacao( Object ligador);

	/* Descricao: Definicao da ligacao do chamador na chamada */
//	public String definirNumeroVinculado( Connection[] ligacao, int numeroLigado);
	public String definirNumeroVinculado( Object ligacao, int numeroLigado);

	/* Descricao: Definicao das ligacoes da chamada */
//	public Connection[] definirLigacao( Call call);
	public Object definirLigacao( Object chamada);

	/* Descricao: Concretizacao da ligacao entre um aparelho telefonico, seu numero e um destino telefonico, contendo os codigos e simbolos necessarios para tanto */
//	public Connection[] ligar( Call call, Terminal chamador, Address origem, String destino);
	public Object ligar( Call chamada, Terminal chamador, Address origem, String destino);

	/* Descricao: Definicao da chamada */
	public Call definirChamada( Provider integrador);

	/* Descricao: Definicao da chamada */
	public Address definirAparelho( Provider integrador, String numero);
	
	/* Descricao: Definicao dos numeros de telefones monitorados que sao associados ao usuario de acesso a integracao computador-telefone */ 
	public Address[] definirAparelhagem( Provider integrador);

	/* Descricao: Definir numero de telefone a ser usado como origem de chamadas */
	public Terminal definirChamador( Provider integrador, String extensao);

	/* Descricao: Definicao do objeto integrador de computacao-telefonia */
	public Provider definirIntegrador( JtapiPeer peer, String expressaoIntegrador);

	/* Descricao: Definicao em acumulo dos servicos de integracao computador-telefone disponives em servidor */
	public String[] definirCatalogo( JtapiPeer peer);

	/* Descricao: Definicao de um objeto da implementacao do pacote javax.telephony */
	public JtapiPeer definirInstancia( String peerName);

}

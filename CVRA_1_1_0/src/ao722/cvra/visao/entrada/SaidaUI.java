package ao722.cvra.visao.entrada;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import ao722.cvra.modelo.estado.CVRAVO;
import ao722.cvra.visao.saida.CVRA;

public class SaidaUI 
	extends javax.swing.AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6604413266882212258L;
	private CVRA simuladorUI;

	public CVRA getSimuladorUI() {
		return simuladorUI;
	}
	public void setSimuladorUI(CVRA simuladorUI) {
		this.simuladorUI = simuladorUI;
	}

	public SaidaUI( CVRA simuladorUI) {
		super( CVRAVO.SAIDA);
		this.putValue( Action.SHORT_DESCRIPTION, "Encerrar e sair");
		setSimuladorUI( simuladorUI);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

}

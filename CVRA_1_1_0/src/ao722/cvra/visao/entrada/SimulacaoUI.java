package ao722.cvra.visao.entrada;

import java.awt.event.ActionEvent;

import javax.swing.Action;
import javax.swing.JOptionPane;

import ao722.cvra.modelo.estado.ConfiguracaoVO;
import ao722.cvra.modelo.estado.ConstituicaoVO;
import ao722.cvra.modelo.estado.CVRAVO;
import ao722.cvra.visao.saida.CVRA;
import ao722.cvra.visao.saida.VisorUI;
//import ao722.simulacao.visao.saida.BarraMenusUI;

public class SimulacaoUI
	extends javax.swing.AbstractAction {

	// Componentes/Propriedades
	private static final long serialVersionUID = 1L;
	private CVRA cVRA;
	private VisorUI monitor;
	private TecladoTelefonicoUI tecladoTelefonicoUI;
	private InteracaoUI interacaoUI;

	// Sobrecarga
	public SimulacaoUI( CVRA cVRA){
		super( CVRAVO.SIMULACAO);
		this.putValue( Action.SHORT_DESCRIPTION, "Abrir um texto-URA para simulacao e prototipacao de URA");
		setCVRA( cVRA);
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CVRA getCVRA() {
		return cVRA;
	}
	public void setCVRA( CVRA cVRA) {
		this.cVRA = cVRA;
	}

	public VisorUI getMonitor() {
		return monitor;
	}
	public void setMonitor( VisorUI monitor) {
		this.monitor = monitor;
	}

	public TecladoTelefonicoUI getTecladoTelefonicoUI() {
		return tecladoTelefonicoUI;
	}
	public void setTecladoTelefonicoUI( TecladoTelefonicoUI tecladoTelefonicoUI) {
		this.tecladoTelefonicoUI = tecladoTelefonicoUI;
	}

	public InteracaoUI getInteracaoUI() {
		return interacaoUI;
	}
	public void setInteracaoUI( InteracaoUI interacaoUI) {
		this.interacaoUI = interacaoUI;
	}

	// Metodos
	public void actionPerformed( ActionEvent evento){
		CVRAVO.subsistema = CVRAVO.simulacao;
		setCVRA( getCVRA());
		getCVRA().getCVRABL().configurar();
		String strScript = ConfiguracaoVO.script, strLimiteTentativas = String.valueOf( ConfiguracaoVO.limiteTentativas), strQuadrado = String.valueOf( ConfiguracaoVO.quadrado);
		String strCaminho = ConfiguracaoVO.local.concat( CVRAVO.subsistema).concat("/");

		strScript = JOptionPane.showInputDialog( "Arquivo de URA em texto:", strScript);
		strLimiteTentativas = JOptionPane.showInputDialog( "Limite de tentativas dos menus:", strLimiteTentativas);
		strQuadrado = JOptionPane.showInputDialog( "Tamanho/zoom da tela: ", strQuadrado);
		strCaminho = JOptionPane.showInputDialog( "Caminho:", strCaminho);
		String diagnostico = null;

		boolean blnDadosPreenchidos = ( strScript != null && strLimiteTentativas != null && strQuadrado != null && strCaminho != null); 
		if( blnDadosPreenchidos){
			diagnostico = "Arquivo de URA em texto: \"".concat( strScript).concat("\"\n") +
				"Limite de tentativas dos menus: \"".concat( strLimiteTentativas).concat("\"\n") +
				"Tamanho/zoom da tela: \"".concat( strQuadrado).concat("\"\n") +
				"Local: \"".concat( strCaminho).concat("\"");
		}
		else {
			strScript = ConfiguracaoVO.script;
			strCaminho = ConfiguracaoVO.local.concat("../");
			strLimiteTentativas = String.valueOf( ConfiguracaoVO.limiteTentativas);
			strQuadrado = String.valueOf( ConfiguracaoVO.quadrado);
			diagnostico = "Arquivo de URA em texto: \"".concat( strScript).concat("\"\n") +
					"Limite de tentativas dos menus: \"".concat( strLimiteTentativas).concat("\"\n") +
					"Tamanho/zoom da tela: \"".concat( strQuadrado).concat("\"\n") +
					"Local: \"".concat( strCaminho).concat("\"");
		}

		JOptionPane.showMessageDialog( null, diagnostico, CVRAVO.subsistema, 1);

		int tentativas = 0;
		boolean blnCaminhoComAcento = strCaminho.indexOf( "%")>0;
		boolean blnPossuiTentativas = tentativas < ConfiguracaoVO.limiteTentativas;
		boolean blnCaminhoSemAcento = !blnCaminhoComAcento;

		while( blnCaminhoComAcento && blnPossuiTentativas){
			strCaminho = JOptionPane.showInputDialog( "Remova os erros (%??) do caminho identificado, por favor", strCaminho);
			boolean blnCaminhoCorrigido = strCaminho != null; 

			if( blnCaminhoCorrigido){
				blnCaminhoComAcento = strCaminho.indexOf( "%")>0;

				if( blnCaminhoComAcento){
					tentativas = tentativas+1;
					blnPossuiTentativas = tentativas < ConfiguracaoVO.limiteTentativas;
					boolean blnTentativasEsgotadas = !blnPossuiTentativas; 
					if( blnTentativasEsgotadas){
						JOptionPane.showMessageDialog( null, "O caminho nao existe.", ConstituicaoVO.insucesso, JOptionPane.ERROR_MESSAGE);
					}

				}
				else {
					tentativas = 0;
					blnCaminhoSemAcento = !blnCaminhoComAcento;
				}

			}
			else {
				break;
			}

		}

		if( blnCaminhoSemAcento) {
			String strArquivo = strCaminho.concat( strScript);
			java.io.File file = new java.io.File( strArquivo);

			if( file.exists()){
				ConfiguracaoVO.caminho = strCaminho;
				ConfiguracaoVO.quadrado = Integer.valueOf(strQuadrado);
				ConfiguracaoVO.script = strScript;
				ConfiguracaoVO.limiteTentativas = Integer.valueOf( strLimiteTentativas);

				getCVRA().getCVRABL().constituir();
				//getSimuladorUI().gerarComponentesSimulacao();
				gerarComponentesSimulacao();
			}
			else {
				JOptionPane.showMessageDialog( null, "O script \n\"".concat(strArquivo).concat("\"\n nao existe ou nao foi encontrado no caminho."), ConstituicaoVO.insucesso, JOptionPane.ERROR_MESSAGE);
			}

			file = null;
		}

		strScript = null;
	}

	public void gerarComponentesSimulacao(){
		//SimuladorUI simuladorUI = this;
		getCVRA().getContentPane().removeAll();
		getCVRA().setBarraMenu( getCVRA().simularBarraMenu());
		setMonitor( simularMonitor());
		setTecladoTelefonicoUI( simularTecladoTelefonico( ));
		getCVRA().setVisible( true);
		getCVRA().setJMenuBar( getCVRA().getBarraMenu());
		getCVRA().getContentPane().add( ( javax.swing.JLabel)getMonitor());

		for( int i=0; i < getTecladoTelefonicoUI().getTeclado().size(); i++){
			getCVRA().getContentPane().add( ( javax.swing.JButton)getTecladoTelefonicoUI().getTeclado().get( i));
		}

		getCVRA().setSize( ao722.cvra.modelo.estado.TecladoTelefonicoVO.larguraTotal, ao722.cvra.modelo.estado.ConfiguracaoVO.quadrado*26);
	}

	public VisorUI simularMonitor(){
		return new VisorUI( ConfiguracaoVO.quadrado, ConfiguracaoVO.quadrado, ConfiguracaoVO.quadrado*15, ConfiguracaoVO.quadrado*1);
	}

	public TecladoTelefonicoUI simularTecladoTelefonico(){
		setInteracaoUI( new InteracaoUI( getMonitor()));
		TecladoTelefonicoUI tecladoTelefonicoUI = new TecladoTelefonicoUI( getInteracaoUI());
		return tecladoTelefonicoUI;
	}

}

package ao722.cvra.visao.entrada;

public class AcionadorUI
	extends javax.swing.JButton {

	// Propriedades
	private static final long serialVersionUID = 1L;
	
	public AcionadorUI() {	}
	public AcionadorUI( javax.swing.Action action, int posicaoH, int posicaoV, int largura, int altura, String texto){
		super( action);
		this.setVisible( true);
		this.setText( texto);
		this.setBounds( posicaoH, posicaoV, largura, altura);
	}

}

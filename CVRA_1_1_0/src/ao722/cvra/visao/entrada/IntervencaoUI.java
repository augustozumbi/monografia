package ao722.cvra.visao.entrada;

import java.awt.event.ActionEvent;
//import java.io.IOException;

//import javax.swing.JFileChooser;


import ao722.cvra.controle.interpretacao.IntervencaoBL;
import ao722.cvra.modelo.estado.ControladorVO;
import ao722.cvra.modelo.estado.IntervencaoVO;
import ao722.cvra.visao.saida.TabelaSumarioUI;
import ao722.cvra.visao.saida.VisorUI;

//import ao722.simulacao.visao.saida.TabelaUI;

public class IntervencaoUI 
	extends javax.swing.AbstractAction
	implements Runnable {

	private static final long serialVersionUID = -8402966560094626450L;

	// Propriedades
	private VisorUI visor;
	private TabelaSumarioUI tabelaSumario;//, relatorioTab;  
	private IntervencaoBL intervencaoBL;
//	private String[] titulosSumario, titulosRelatorio;
//	private Object[][] conteudoSumario, conteudoRelatorio;
//	private CertificacaoUI certificacao;

	// Construtores
	public IntervencaoUI( VisorUI visor, TabelaSumarioUI sumario) {
		setVisor( visor);
		setTabelaSumario(sumario);
		//setCertificacao( certificacaoUI);
		setIntervencaoBL( new IntervencaoBL( this));
	}

	// Getters and setters, Encapsulamento
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public VisorUI getVisor() {
		return visor;
	}
	public void setVisor(VisorUI visor) {
		this.visor = visor;
	}

	public TabelaSumarioUI getTabelaSumario() {
		return tabelaSumario;
	}
	public void setTabelaSumario(TabelaSumarioUI tabelaSumario) {
		this.tabelaSumario = tabelaSumario;
	}

//	public TabelaUI getRelatorioTab() {
//		return relatorioTab;
//	}
//	public void setRelatorioTab(TabelaUI relatorioTab) {
//		this.relatorioTab = relatorioTab;
//	}

	public IntervencaoBL getIntervencaoBL() {
		return intervencaoBL;
	}
	public void setIntervencaoBL(IntervencaoBL intervencaoBL) {
		this.intervencaoBL = intervencaoBL;
	}

//	public String[] getTitulosSumario() {
//		return titulosSumario;
//	}
//	public void setTitulosSumario(String[] titulosSumario) {
//		this.titulosSumario = titulosSumario;
//	}

//	public String[] getTitulosRelatorio() {
//		return titulosRelatorio;
//	}
//	public void setTitulosRelatorio(String[] titulosRelatorio) {
//		this.titulosRelatorio = titulosRelatorio;
//	}

//	public Object[][] getConteudoSumario() {
//		return conteudoSumario;
//	}
//	public void setConteudoSumario(Object[][] conteudoSumario) {
//		this.conteudoSumario = conteudoSumario;
//	}

//	public Object[][] getConteudoRelatorio() {
//		return conteudoRelatorio;
//	}
//	public void setConteudoRelatorio(Object[][] conteudoRelatorio) {
//		this.conteudoRelatorio = conteudoRelatorio;
//	}

//	public CertificacaoUI getCertificacao() {
//		return certificacao;
//	}
//	public void setCertificacao( CertificacaoUI certificacao) {
//		this.certificacao = certificacao;
//	}

	@Override
	public void actionPerformed( ActionEvent arg0) {

		boolean blnEventoExiste = ( arg0 != null);
		if( blnEventoExiste){
			IntervencaoVO.acionador = arg0.getActionCommand();
		}

		boolean blnLimpeza = IntervencaoVO.acionador.equalsIgnoreCase( ControladorVO.LIMPAR);
		if( blnLimpeza){
			
		}

		Thread thread = new Thread( this);
		thread.start();
	}

	// Metodos/Operacoes/Comportamentos
	public void interpretarIntervencao(){
		setIntervencaoBL( new IntervencaoBL( this));

//		if( getIntervencaoBL().isExportacao()){
//			exportarRelatorio();
//		}

//		if( getIntervencaoBL().isLimpeza()){
//			limparRelatorio();
//		}

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		interpretarIntervencao();
	}

//	public void suprirSumario(){
//		setSumarioTab( null);
//		setTitulosSumario( new String[]{ "casos", "valores"});
//		
//		for( int i=0; i<ExecucaoVO.casos.length; i++){
//			getConteudoSumario()[i][0] = ExecucaoVO.casos[i];
//			getConteudoSumario()[i][1] = ExecucaoVO.tarefas[i];
//		}

//		setRelatorio( new TabelaUI( ConfiguracaoVO.quadrado*1, ConfiguracaoVO.quadrado*10, ConfiguracaoVO.quadrado*27, ConfiguracaoVO.quadrado*4));

//	}

//	public void exportarRelatorio(){
//		javax.swing.JFileChooser jfc = new javax.swing.JFileChooser( ConfiguracaoVO.local);
//		jfc.setSelectedFile( new java.io.File( getCertificacao().getLogURL().split( "/")[3].concat( ConfiguracaoVO.extensaoExportacao)));
//		jfc.setSelectedFile( new java.io.File( ConfiguracaoVO.logURL.split( "/")[3].concat( ConfiguracaoVO.extensaoExportacao)));
//		int resp = jfc.showSaveDialog( getCertificacao().getSumario());
//		int resp = jfc.showSaveDialog( getTabelaSumario());
//
//		boolean blnEscolhaOmitida = resp != JFileChooser.APPROVE_OPTION; 
//		if( blnEscolhaOmitida) return;
//
//		java.io.File arquivoRelatorio = jfc.getSelectedFile();
//
//		try {
//			java.io.File file = new java.io.File( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.relatorio));
//			java.io.BufferedReader br = new java.io.BufferedReader( new java.io.InputStreamReader( new java.io.FileInputStream( file.getAbsolutePath())));
//			char[] armazem = new char[( int)file.length()];  
//			java.io.FileWriter fw = new java.io.FileWriter( arquivoRelatorio);
//			br.read( armazem, 0, ( int)file.length());
//			fw.write( armazem);
//			fw.close();
//			br.close();
//		}
//		catch ( IOException e) {
//			e.printStackTrace();
//		}
//		catch( Exception e){
//			e.printStackTrace();
//		}
//
//	}

//	public void limparRelatorio(){
//
//		try{
//			System.out.println( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.relatorio));
//			System.out.println( new java.io.File( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.relatorio)).delete());
//			System.out.println( new java.io.File( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.relatorio)).createNewFile());
//
//			System.out.println( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.historico));
//			System.out.println( new java.io.File( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.historico)).delete());
//			System.out.println( new java.io.File( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.historico)).createNewFile());
//
//			System.out.println( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.produto));
//			System.out.println( new java.io.File( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.produto)).delete());
//			System.out.println( new java.io.File( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.produto)).createNewFile());
//		}
//		catch( Exception e){
//			e.printStackTrace();
//		}
//
//	}

}

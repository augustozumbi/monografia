package ao722.cvra.visao.entrada;

import ao722.cvra.controle.interpretacao.ControladorBL;
import ao722.cvra.modelo.estado.ConfiguracaoVO;
import ao722.cvra.modelo.estado.ControladorVO;

public class ControladorUI {

	// Propriedades
	private ControladorBL controladorBL;
	private java.util.List< AcionadorUI> teclado;
	
	// Getters and Setters
	public ControladorBL getControladorBL() {
		return controladorBL;
	}
	public void setControladorBL(ControladorBL controladorBL) {
		this.controladorBL = controladorBL;
	}
	public java.util.List<AcionadorUI> getTeclado() {
		return teclado;
	}
	public void setTeclado(java.util.List<AcionadorUI> teclado) {
		this.teclado = teclado;
	}

	// Construtores
	public ControladorUI( IntervencaoUI intervencaoUI) {
		setControladorBL( new ControladorBL());
		fabricarPadrao( intervencaoUI);
	}

	public void fabricarPadrao( IntervencaoUI intervencaoUI){
		setTeclado( new java.util.ArrayList<AcionadorUI>());

		// Declaracao e definicao de variaveis
		ControladorVO.indice=0;
		ControladorVO.horizontal = ConfiguracaoVO.quadrado;
		ControladorVO.vertical = ConfiguracaoVO.quadrado*2;

		// Definicao de variaveis globais
		ControladorVO.larguraTotal = ConfiguracaoVO.quadrado*30;

		AcionadorUI botaoRodar = new AcionadorUI( intervencaoUI, ControladorVO.horizontal, ControladorVO.vertical,  ConfiguracaoVO.quadrado*6, ConfiguracaoVO.quadrado*3, ControladorVO.EXECUCAO);
		ControladorVO.horizontal = ControladorVO.horizontal+( ConfiguracaoVO.quadrado*7);

		getTeclado().add( botaoRodar);

		AcionadorUI botaoEsperar = new AcionadorUI( intervencaoUI, ControladorVO.horizontal, ControladorVO.vertical,  ConfiguracaoVO.quadrado*6, ConfiguracaoVO.quadrado*3, ControladorVO.AGUARDO);
		ControladorVO.horizontal = ControladorVO.horizontal+( ConfiguracaoVO.quadrado*7);

		getTeclado().add( botaoEsperar);

		AcionadorUI botaoRetomar = new AcionadorUI( intervencaoUI, ControladorVO.horizontal, ControladorVO.vertical,  ConfiguracaoVO.quadrado*6, ConfiguracaoVO.quadrado*3, ControladorVO.RETORNO);
		ControladorVO.horizontal = ControladorVO.horizontal+( ConfiguracaoVO.quadrado*7);

		getTeclado().add( botaoRetomar);

		AcionadorUI botaoDesistir = new AcionadorUI( intervencaoUI, ControladorVO.horizontal, ControladorVO.vertical,  ConfiguracaoVO.quadrado*6, ConfiguracaoVO.quadrado*3, ControladorVO.ABORTO);
		ControladorVO.horizontal = ControladorVO.horizontal+( ConfiguracaoVO.quadrado*7);

		getTeclado().add( botaoDesistir);

		ControladorVO.horizontal = ConfiguracaoVO.quadrado*3;
		ControladorVO.vertical = ConfiguracaoVO.quadrado*11;

		AcionadorUI botaoExecutar = new AcionadorUI( intervencaoUI, ControladorVO.horizontal, ControladorVO.vertical,  ConfiguracaoVO.quadrado*5, ConfiguracaoVO.quadrado*2, ControladorVO.EXECUTAR);
		ControladorVO.horizontal = ControladorVO.horizontal+( ConfiguracaoVO.quadrado*6);

		getTeclado().add( botaoExecutar);

		AcionadorUI botaoValidar = new AcionadorUI( intervencaoUI, ControladorVO.horizontal, ControladorVO.vertical,  ConfiguracaoVO.quadrado*5, ConfiguracaoVO.quadrado*2, ControladorVO.VALIDAR);
		ControladorVO.horizontal = ControladorVO.horizontal+( ConfiguracaoVO.quadrado*6);

		getTeclado().add( botaoValidar);

		AcionadorUI botaoReportar = new AcionadorUI( intervencaoUI, ControladorVO.horizontal, ControladorVO.vertical,  ConfiguracaoVO.quadrado*5, ConfiguracaoVO.quadrado*2, ControladorVO.EXPORTAR);
		ControladorVO.horizontal = ControladorVO.horizontal+( ConfiguracaoVO.quadrado*6);

		getTeclado().add( botaoReportar);

		AcionadorUI botaoExportar = new AcionadorUI( intervencaoUI, ControladorVO.horizontal, ControladorVO.vertical,  ConfiguracaoVO.quadrado*5, ConfiguracaoVO.quadrado*2, ControladorVO.LIMPAR);
		ControladorVO.horizontal = ControladorVO.horizontal+( ConfiguracaoVO.quadrado*6);

		getTeclado().add( botaoExportar);
		
	}

}

package ao722.cvra.visao.entrada;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;

import java.text.Normalizer;
import java.util.Timer;

import ao722.cvra.controle.interpretacao.InteracaoBL;
import ao722.cvra.modelo.estado.InteracaoVO;
import ao722.cvra.modelo.estado.LocalizacaoVO;
import ao722.cvra.modelo.estado.MensagemVO;
import ao722.cvra.modelo.estado.NavegacaoVO;
import ao722.cvra.modelo.estado.TecladoTelefonicoVO;
import ao722.cvra.visao.saida.VisorUI;

public class InteracaoUI
	extends javax.swing.AbstractAction
	implements java.awt.event.KeyListener {

	// Classe anexada
	class RetornoUI
		extends java.util.TimerTask {

		public void run() {
			int tentativa = NavegacaoVO.tentativas+1;
			System.out.println("Tempo de aguardo da tentativa "+ tentativa +" esgotado");
			InteracaoVO.acionador = null;
			actionPerformed( null);
		}

	}

	// Propriedades
	private static final long serialVersionUID = 1L;
	private InteracaoBL interacaoBL;
	private Timer relogio;
	private VisorUI visor;

	// Construtores
	public InteracaoUI( VisorUI visorUI) {
		setVisor( visorUI);
		InteracaoVO.acionador = InteracaoVO.saudacao;
		//actionPerformed( null);
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	// Getters and setters
	public InteracaoBL getInteracaoBL() {
		return interacaoBL;
	}
	public void setInteracaoBL(InteracaoBL interacaoBL) {
		this.interacaoBL = interacaoBL;
	}

	public VisorUI getVisor() {
		return visor;
	}
	public void setVisor(VisorUI visor) {
		this.visor = visor;
	}

	public Timer getRelogio() {
		return relogio;
	}
	public void setRelogio(Timer relogio) {
		this.relogio = relogio;
	}

	// Metodos
	public void interpretarInteracao(){
		setInteracaoBL( new InteracaoBL());
	}

	public void revisarInteracao(){

		if( getInteracaoBL().isIniciar() || getInteracaoBL().isChamar()){
			MensagemVO.tipo = JOptionPane.PLAIN_MESSAGE; 
		}
		else {

			if( getInteracaoBL().isValida()){
				MensagemVO.tipo = JOptionPane.INFORMATION_MESSAGE;
			}
			else {
				MensagemVO.tipo = JOptionPane.ERROR_MESSAGE;
			}

		}

	}

	public void ecoarInteracao(){

		if( getInteracaoBL().isDemonstravel()){

			for(int i=0; i< MensagemVO.reacao.split(";").length; i++){
				JOptionPane.showMessageDialog( null, java.text.Normalizer.normalize( MensagemVO.reacao.split(";")[i], Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", ""), MensagemVO.assunto, MensagemVO.tipo);	
			}

		}
		else if( getInteracaoBL().isVerbalizavel()){	}

	}

	public void responderInteracao(){

		if( getInteracaoBL().isDemonstravel()){

			for(int i=0; i< MensagemVO.frase.split(";").length; i++){
				JOptionPane.showMessageDialog( null, java.text.Normalizer.normalize(MensagemVO.frase.split(";")[i], Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", ""), MensagemVO.assunto, MensagemVO.tipo);
			}

		}
		else if( getInteracaoBL().isVerbalizavel()){	}

	}

	public void demonstrar(){
		getVisor().setText( LocalizacaoVO.trajetoria);
	}

	public void aguardar(){

		boolean blnAgendamentoPendente = getRelogio() != null; 
		if( blnAgendamentoPendente){
			getRelogio().cancel();
			System.out.println( "Interrompido aguardo da tentativa "+ NavegacaoVO.tentativas +": "+ getRelogio().toString());
			setRelogio( null);
		}

		setRelogio( new Timer());
		getRelogio().schedule( new RetornoUI(), InteracaoVO.intervalo);
		int tentativa = NavegacaoVO.tentativas+1;
		System.out.println("Iniciado aguardo de "+ InteracaoVO.intervalo +" milissegundos da tentativa "+ tentativa +": "+ getRelogio().toString());
	}

	public void teclagem( char e){

		for( int i=0; i< TecladoTelefonicoVO.letras.length; i++){

			boolean blnTeclaExistente = ( TecladoTelefonicoVO.letras[ i] == e);
			if( blnTeclaExistente){
				InteracaoVO.acionador = String.valueOf( e);
				actionPerformed( null);
			}

		}

	}

	@Override
	public void actionPerformed( ActionEvent e) {
		InteracaoVO.acionador = null;

		boolean blnInteracaoRealizada = ( e != null);
		if( blnInteracaoRealizada){
			InteracaoVO.acionador = e.getActionCommand();

			boolean blnAgendamentoPendente = getRelogio() != null; 
			if( blnAgendamentoPendente){
				getRelogio().cancel();
				int tentativa = NavegacaoVO.tentativas+1;
				System.out.println("Interrompido aguardo da tentativa "+ tentativa +": "+ getRelogio().toString());
				setRelogio(null);
			}
			
		}
		else {
			InteracaoVO.acionador = InteracaoVO.omissao;
//
//			boolean blnAcionadorPreenchido = ( InteracaoVO.acionador != null);
//			if( blnAcionadorPreenchido){
//				System.out.println("agendamento cancelado.");
//			}
//			else {
//			}
		}

		interpretarInteracao();

		if( getInteracaoBL().isPossivel()){

			if( getInteracaoBL().isDesligar()){	}
			else {
				revisarInteracao();
				ecoarInteracao();

				if( getInteracaoBL().isResponsavel()){
					responderInteracao();
				}

				aguardar();
				
			}

			demonstrar();
		}

		InteracaoVO.acionador = null;
	}

	@Override
	public void keyTyped(KeyEvent e) {

		if( getInteracaoBL().isPossivel()){
			char chrTecla = e.getKeyChar();
			teclagem( chrTecla);
			System.out.println( "e.getKeyChar(): "+ e.getKeyChar());
		}

	}

	@Override
	public void keyPressed(KeyEvent e) {
//		char chrTecla = e.getKeyChar();
//		int intCodigoBotao = e.getKeyCode();
//		int intIdentificador = e.getID();
//
//		boolean blnTecladoLetraNumero = KeyEvent.KEY_TYPED == intIdentificador;
//		if( blnTecladoLetraNumero){		}
//		else {
//			chrTecla = KeyEvent.getKeyText( intCodigoBotao).charAt(0);
//		}
//
//		teclagem( chrTecla);
	}

	@Override
	public void keyReleased(KeyEvent e) {	}

}

package ao722.cvra.visao.entrada;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import ao722.cvra.modelo.estado.CVRAVO;
import ao722.cvra.visao.saida.CVRA;

public class ContingenciaUI 
	extends javax.swing.AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6604413266882212258L;
	private CVRA simuladorUI;

	public CVRA getSimuladorUI() {
		return simuladorUI;
	}
	public void setSimuladorUI(CVRA simuladorUI) {
		this.simuladorUI = simuladorUI;
	}

	public ContingenciaUI( CVRA simuladorUI) {
		super( CVRAVO.CONTINGENCIA);
		this.putValue( Action.SHORT_DESCRIPTION, "Criar novo Sumario de Testes de certificacao de URA.");
		setSimuladorUI( simuladorUI);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

}

package ao722.cvra.visao.entrada;

import java.awt.event.ActionEvent;

import javax.swing.JButton;

import ao722.cvra.controle.interpretacao.TecladoTelefonicoBL;
import ao722.cvra.modelo.estado.ConfiguracaoVO;
import ao722.cvra.modelo.estado.ConstituicaoVO;
import ao722.cvra.modelo.estado.TecladoTelefonicoVO;

public class TecladoTelefonicoUI
 	implements java.awt.event.ActionListener{

	// Propriedades, Atributos
	private TecladoTelefonicoBL tecladoTelefonicoBL;
	private java.util.List< AcionadorUI> teclado; 

	// Construtores
	public TecladoTelefonicoUI( InteracaoUI interacaoUI){
		setTecladoTelefonicoBL( new TecladoTelefonicoBL());
		fabricarPadrao( interacaoUI);

		if( getTecladoTelefonicoBL().isExtendido()){
			adicionarComandos( interacaoUI);
		}
	
	}
	// Getters and Setters, Encapsulamento
	public TecladoTelefonicoBL getTecladoTelefonicoBL() {
		return tecladoTelefonicoBL;
	}
	public void setTecladoTelefonicoBL(TecladoTelefonicoBL tecladoTelefonicoBL) {
		this.tecladoTelefonicoBL = tecladoTelefonicoBL;
	}

	public java.util.List<AcionadorUI> getTeclado() {
		return teclado;
	}
	public void setTeclado( java.util.List<AcionadorUI> teclado) {
		this.teclado = teclado;
	}

	// Metodos
	public void fabricarPadrao( InteracaoUI interacaoUI){
		setTeclado( new java.util.ArrayList<AcionadorUI>());

		// Declaracao e definicao de variaveis
		TecladoTelefonicoVO.indice=0;
		TecladoTelefonicoVO.horizontal = ConfiguracaoVO.quadrado;
		TecladoTelefonicoVO.vertical = ConfiguracaoVO.quadrado*3;
		TecladoTelefonicoVO.letras = new char[]{ '1','2','3','4','5','6','7','8','9','*','0','#'};

		// Definicao de variaveis globais
		TecladoTelefonicoVO.larguraTotal = ConfiguracaoVO.quadrado*13;

		// Criacao do teclado convencional
		for( TecladoTelefonicoVO.indice=0; TecladoTelefonicoVO.indice<12; TecladoTelefonicoVO.indice++){
			AcionadorUI tmpBotaoUI = new AcionadorUI( 
				interacaoUI
				, TecladoTelefonicoVO.horizontal
				, TecladoTelefonicoVO.vertical
				, ConfiguracaoVO.quadrado*3
				, ConfiguracaoVO.quadrado*3
				, String.valueOf( TecladoTelefonicoVO.letras[ TecladoTelefonicoVO.indice])
			);
			TecladoTelefonicoVO.horizontal = TecladoTelefonicoVO.horizontal+(ConfiguracaoVO.quadrado*4);

			getTecladoTelefonicoBL().setLateralAtingida( TecladoTelefonicoVO.horizontal == ConfiguracaoVO.quadrado*13);
			if( getTecladoTelefonicoBL().isLateralAtingida()){
				TecladoTelefonicoVO.vertical = TecladoTelefonicoVO.vertical+(ConfiguracaoVO.quadrado*4);
				TecladoTelefonicoVO.horizontal = ConfiguracaoVO.quadrado;
			}

			tmpBotaoUI.addKeyListener( interacaoUI);
			getTeclado().add( tmpBotaoUI);
		}

		// Botoes de comando
		getTeclado().add( new AcionadorUI( interacaoUI, ConfiguracaoVO.quadrado, ConfiguracaoVO.quadrado*19, ConfiguracaoVO.quadrado*5, ConfiguracaoVO.quadrado*3, TecladoTelefonicoVO.CHAMAR));
		getTeclado().add( new AcionadorUI( interacaoUI, ConfiguracaoVO.quadrado*7, ConfiguracaoVO.quadrado*19, ConfiguracaoVO.quadrado*5, ConfiguracaoVO.quadrado*3, TecladoTelefonicoVO.DESLIGAR));
	}

	public void adicionarComandos( InteracaoUI interacaoUI){
		// Declaracao e definicao de propriedades
		TecladoTelefonicoVO.indice=0;
		TecladoTelefonicoVO.horizontal = ConfiguracaoVO.quadrado*13;
		TecladoTelefonicoVO.vertical = ConfiguracaoVO.quadrado*3;
		TecladoTelefonicoVO.alargador = 0;

		// Definicao de propriedades globais
		TecladoTelefonicoVO.larguraTotal = TecladoTelefonicoVO.larguraTotal+ConfiguracaoVO.quadrado*8;

		// Criacao de botoes adicionais
		for( TecladoTelefonicoVO.indice=0; TecladoTelefonicoVO.indice< ConstituicaoVO.comandos.split(";").length; TecladoTelefonicoVO.indice++){
			TecladoTelefonicoVO.alargador++;

			// Decisao de alargamento da tela do simulador
			getTecladoTelefonicoBL().setAlargamentoNecessario( TecladoTelefonicoVO.alargador==5);
			if( getTecladoTelefonicoBL().isAlargamentoNecessario()){
				TecladoTelefonicoVO.larguraTotal = TecladoTelefonicoVO.larguraTotal+ConfiguracaoVO.quadrado*6;
				TecladoTelefonicoVO.alargador = 0;
			}

			// Definicao do botao
			AcionadorUI tmpBotaoUI = new AcionadorUI( interacaoUI, TecladoTelefonicoVO.horizontal, TecladoTelefonicoVO.vertical, ConfiguracaoVO.quadrado*5, ConfiguracaoVO.quadrado*3, ConstituicaoVO.comandos.split(";")[TecladoTelefonicoVO.indice]);
			TecladoTelefonicoVO.vertical= TecladoTelefonicoVO.vertical+( ConfiguracaoVO.quadrado*4);

			// Decisao de reinicio do posicionamento dos botoes
			getTecladoTelefonicoBL().setBaseAtingida( TecladoTelefonicoVO.vertical > ConfiguracaoVO.quadrado*18);
			if( getTecladoTelefonicoBL().isBaseAtingida()){
				TecladoTelefonicoVO.vertical = ConfiguracaoVO.quadrado*3;
				TecladoTelefonicoVO.horizontal = TecladoTelefonicoVO.horizontal+( ConfiguracaoVO.quadrado*6);
			}

			// Adicao do botao
			getTeclado().add( tmpBotaoUI);
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		for( JButton tmpBotao : this.getTeclado()){
			tmpBotao.requestFocusInWindow();
		}

	}

}

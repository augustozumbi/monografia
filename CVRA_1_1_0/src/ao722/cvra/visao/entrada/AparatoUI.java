package ao722.cvra.visao.entrada;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import ao722.cvra.modelo.estado.CVRAVO;
import ao722.cvra.visao.saida.CVRA;

public class AparatoUI 
	extends javax.swing.AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6604413266882212258L;
	private CVRA simuladorUI;

	public CVRA getSimuladorUI() {
		return simuladorUI;
	}
	public void setSimuladorUI(CVRA simuladorUI) {
		this.simuladorUI = simuladorUI;
	}

	public AparatoUI( CVRA simuladorUI) {
		super( CVRAVO.APARATO);
		this.putValue( Action.SHORT_DESCRIPTION, "Editar os recursos de teste atuais.");
		setSimuladorUI( simuladorUI);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

}

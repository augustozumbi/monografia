package ao722.cvra.visao.entrada;

import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;

import ao722.cvra.modelo.estado.ConfiguracaoVO;
import ao722.cvra.modelo.estado.ConstituicaoVO;
import ao722.cvra.modelo.estado.ExecucaoVO;
import ao722.cvra.modelo.estado.CVRAVO;
import ao722.cvra.visao.saida.SelecionadorArquivoUI;
import ao722.cvra.visao.saida.CVRA;
import ao722.cvra.visao.saida.TabelaSumarioUI;
import ao722.cvra.visao.saida.VisorUI;
//import ao722.simulacao.visao.saida.SelecionadorArquivoUI;
//import ao722.simulacao.visao.saida.TabelaUI;

public class CertificacaoUI 
	extends javax.swing.AbstractAction {

	// Componentes/Propriedades
	private static final long serialVersionUID = 1L;
	private String extensao, destino, logURL, pastaSaida; 
	private CVRA cVRA;
	private VisorUI monitor;
	private ControladorUI controladorUI;
	private	IntervencaoUI intervencaoUI;
//	private TabelaUI relatorio;
	private TabelaSumarioUI tabelaSumario;
	private SelecionadorArquivoUI selecionadorArquivo;
//	private JSeparator separador;

	// Sobrecarga
	public CertificacaoUI( CVRA simuladorUI){
		super( CVRAVO.CERTIFICACAO);
		this.putValue( Action.SHORT_DESCRIPTION, "Abrir um sumario de testes para certificacao e testes de URA");
		setCVRA( simuladorUI);
	}

	// Getters and Setters / Encapsulamento	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getExtensao() {
		return extensao;
	}
	public void setExtensao(String extensao) {
		this.extensao = extensao;
	}
	
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	
	public String getLogURL() {
		return logURL;
	}
	public void setLogURL(String logURL) {
		this.logURL = logURL;
	}

	public String getPastaSaida() {
		return pastaSaida;
	}
	public void setPastaSaida( String pastaSaida) {
		this.pastaSaida = pastaSaida;
	}

	public CVRA getCVRA() {
		return cVRA;
	}
	public void setCVRA( CVRA cVRA) {
		this.cVRA = cVRA;
	}

	public VisorUI getMonitor() {
		return monitor;
	}
	public void setMonitor( VisorUI monitor) {
		this.monitor = monitor;
	}

//	public JSeparator getSeparador() {
//		return separador;
//	}
//	public void setSeparador(JSeparator separador) {
//		this.separador = separador;
//	}

	public ControladorUI getControladorUI() {
		return controladorUI;
	}
	public void setControladorUI(ControladorUI controladorUI) {
		this.controladorUI = controladorUI;
	}

	public IntervencaoUI getIntervencaoUI() {
		return intervencaoUI;
	}
	public void setIntervencaoUI(IntervencaoUI intervencaoUI) {
		this.intervencaoUI = intervencaoUI;
	}

	public TabelaSumarioUI getTabelaSumario() {
		return tabelaSumario;
	}
	public void setTabelaSumario(TabelaSumarioUI tabelaSumario) {
		this.tabelaSumario = tabelaSumario;
	}

	public SelecionadorArquivoUI getSelecionadorArquivo() {
		return selecionadorArquivo;
	}
	public void setSelecionadorArquivo(SelecionadorArquivoUI selecionadorArquivo) {
		this.selecionadorArquivo = selecionadorArquivo;
	}

//	public TabelaUI getRelatorio() {
//		return relatorio;
//	}
//	public void setRelatorio(TabelaUI relatorio) {
//		this.relatorio = relatorio;
//	}

	// Metodos
	public void actionPerformed( ActionEvent evento){	
		CVRAVO.subsistema = CVRAVO.certificacao;
		setCVRA( getCVRA());
		getCVRA().getCVRABL().configurar();

		capturar();

		String diagnostico = null;
		String strCaminho = getPastaSaida();//ConfiguracaoVO.local.concat("../");
		String strSumario = ConfiguracaoVO.sumario.split("\\\\")[ ConfiguracaoVO.sumario.split("\\\\").length-1], strMassa = ConfiguracaoVO.massa, strProduto = ConfiguracaoVO.produto, strHistorico = ConfiguracaoVO.historico, strRelatorio = ConfiguracaoVO.relatorio;

		String strNomeArquivo = strSumario.split("\\.")[0];
		System.out.println( "strSumario: "+ strSumario);
		//strSumario = getLogURL().split("/")[3].concat( ConfiguracaoVO.extensaoSumario);
		//strSumario = strSumario.split("\\\\")[ strSumario.split("\\\\").length-1];
		System.out.println( "strSumario: "+ strSumario);
//		strMassa = getLogURL().split("/")[3].concat( ConfiguracaoVO.extensaoMassa);
//		strProduto = getLogURL().split("/")[3].concat( ConfiguracaoVO.extensaoProduto);
//		strHistorico = getLogURL().split("/")[3].concat( ConfiguracaoVO.extensaoHistorico);
//		strRelatorio = getLogURL().split("/")[3].concat( ConfiguracaoVO.extensaoRelatorio);
		strMassa = strNomeArquivo.concat( ConfiguracaoVO.extensaoMassa);
		strProduto = strNomeArquivo.concat( ConfiguracaoVO.extensaoProduto);
		strHistorico = strNomeArquivo.concat( ConfiguracaoVO.extensaoHistorico);
		strRelatorio = strNomeArquivo.concat( ConfiguracaoVO.extensaoRelatorio);
		strCaminho = getPastaSaida().concat("/"); // ConfiguracaoVO.local.concat( SimuladorVO.subsistema).concat("/");
		diagnostico = "Dados insistidos\n"+
			"Ramal de origem das chamadas: \"".concat( getExtensao()).concat("\"\n") +
			"Telefone de destino das chamadas: \"".concat( getDestino()).concat("\"\n") +
			"Arquivo de sumario de testes: \"".concat( strSumario).concat("\"\n") +
			"Arquivo de massa de testes: \"".concat( strMassa).concat("\"\n") +
			"Arquivo de produto de testes: \"".concat( strProduto).concat("\"\n") +
			"Arquivo de historico de testes: \"".concat( strHistorico).concat("\"\n") +
			"Arquivo de relatorio de testes: \"".concat( strRelatorio).concat("\"\n") +
			"Caminho do sumario de testes: \"".concat( strCaminho).concat("\"\n")+
			"Caminho de geracao do log de chamada: \"".concat( getLogURL()).concat("\"");

		JOptionPane.showMessageDialog( null, diagnostico, CVRAVO.subsistema, 1);

		int tentativas = 0;
		boolean blnCaminhoComAcento = strCaminho.indexOf( "%")>0;
		boolean blnPossuiTentativas = ( tentativas <= ConfiguracaoVO.limiteTentativas);
		boolean blnCaminhoSemAcento = !blnCaminhoComAcento;

		while( blnCaminhoComAcento && blnPossuiTentativas){
			strCaminho = JOptionPane.showInputDialog( "Remova os erros (%??) do caminho identificado, por favor", strCaminho);
			blnCaminhoComAcento = strCaminho.indexOf( "%")>0;

			if( blnCaminhoComAcento){
				tentativas = tentativas+1;
				blnPossuiTentativas = tentativas <= ConfiguracaoVO.limiteTentativas;

				boolean blnTentativasEsgotadas = !blnPossuiTentativas; 
				if( blnTentativasEsgotadas){
					JOptionPane.showMessageDialog( null, "O caminho nao existe.", ConstituicaoVO.insucesso, JOptionPane.ERROR_MESSAGE);
				}

			}
			else {
				tentativas = 0;
				blnCaminhoSemAcento = !blnCaminhoComAcento;
			}

		}

		if( blnCaminhoSemAcento){
			String strArquivo = strCaminho.concat( strSumario);
			java.io.File file = new java.io.File( strArquivo);

			if( file.exists()){
				ConfiguracaoVO.extensao = getExtensao();
				ConfiguracaoVO.destino = getDestino();
				ConfiguracaoVO.sumario = strSumario;
				ConfiguracaoVO.massa = strMassa;
				ConfiguracaoVO.produto = strProduto;
				ConfiguracaoVO.historico = strHistorico;
				ConfiguracaoVO.relatorio = strRelatorio;
				ConfiguracaoVO.caminho = strCaminho;
				ConfiguracaoVO.logURL = getLogURL();

				preparar();

				abrir();
			}
			else {
				JOptionPane.showMessageDialog( null, "O script \n\"".concat(strArquivo).concat("\"\n nao existe ou nao foi encontrado no caminho."), ConstituicaoVO.insucesso, JOptionPane.ERROR_MESSAGE);
			}

			file = null;
		}

		strSumario = null;
	}

	public boolean capturar(){
		setExtensao( ConfiguracaoVO.extensao);
		setDestino( ConfiguracaoVO.destino);
		setLogURL( ConfiguracaoVO.logURL);
		setPastaSaida( ConfiguracaoVO.local.concat( CVRAVO.subsistema).concat("/"));

		setExtensao( JOptionPane.showInputDialog( "Ramal de origem das chamadas:", getExtensao()));
		setDestino( JOptionPane.showInputDialog( "Numero de chamada da aplicacao:", getDestino()));
		setLogURL( JOptionPane.showInputDialog( "Caminho de log da aplicacao:", getLogURL()));
		setSelecionadorArquivo( new SelecionadorArquivoUI( ConfiguracaoVO.local.concat("../")));
		getSelecionadorArquivo().setSelectedFile( new java.io.File( getLogURL().split( "/")[3].concat( ConfiguracaoVO.extensaoSumario)));

		boolean blnEscolheuSumario = ( getSelecionadorArquivo().showOpenDialog( null) == JFileChooser.APPROVE_OPTION);
		if( blnEscolheuSumario){
			ConfiguracaoVO.sumario = getSelecionadorArquivo().getSelectedFile().getName(); //.getPath();
			setPastaSaida( getSelecionadorArquivo().getSelectedFile().getParentFile().getAbsolutePath());
			System.out.println( "ConfiguracaoVO.sumario: "+ ConfiguracaoVO.sumario +", getPastaSaida(): "+ getPastaSaida());
		}

		boolean blnDadosOmitidos = ( getExtensao() == null && getDestino() == null && getLogURL() == null);
		if( blnDadosOmitidos){
			setExtensao( ConfiguracaoVO.extensao);
			setDestino( ConfiguracaoVO.destino);
			setLogURL( ConfiguracaoVO.logURL);
		}

		return true;
	}
	
	public void preparar(){

		try {

			if( !new java.io.File( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.massa)).exists()){
				System.out.println( new java.io.File( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.massa)).createNewFile());
			}

			if( !new java.io.File( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.produto)).exists()){
				System.out.println( new java.io.File( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.produto)).createNewFile());
			}

			if( !new java.io.File( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.historico)).exists()){
				System.out.println( new java.io.File( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.historico)).createNewFile());
			}

			if( !new java.io.File( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.relatorio)).exists()){
				System.out.println( new java.io.File( ConfiguracaoVO.caminho.concat( ConfiguracaoVO.relatorio)).createNewFile());
			}

		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void abrir(){
		getCVRA().getCVRABL().constituir();
		gerarComponentesCertificacao();
	}

	public void gerarComponentesCertificacao(){
		//SimuladorUI simuladorUI = this;
		getCVRA().getContentPane().removeAll();
		getCVRA().setBarraMenu( getCVRA().simularBarraMenu());
		setMonitor( certificarMonitor());
		setControladorUI( certificarControlador( ));
		setTabelaSumario( certificarSumario());
		getCVRA().getContentPane().add( new JSeparator());
		getCVRA().getContentPane().add( getTabelaSumario());
		getCVRA().setVisible( true);
		getCVRA().setJMenuBar( getCVRA().getBarraMenu());
		getCVRA().getContentPane().add( ( javax.swing.JLabel)getMonitor());

		for( int i=0; i < getControladorUI().getTeclado().size(); i++){
			getCVRA().getContentPane().add( ( javax.swing.JButton)getControladorUI().getTeclado().get( i));
		}

//		getSimuladorUI().getContentPane().add( getSumario( ));
//		getSimuladorUI().getContentPane().add( getRelatorio());
		getCVRA().setSize( ao722.cvra.modelo.estado.ControladorVO.larguraTotal, ao722.cvra.modelo.estado.ConfiguracaoVO.quadrado*17);
	}

	public VisorUI certificarMonitor(){
		return new VisorUI( ConfiguracaoVO.quadrado, ConfiguracaoVO.quadrado, ConfiguracaoVO.quadrado*27, ConfiguracaoVO.quadrado*1);
	}

	public TabelaSumarioUI certificarSumario(){
		return new TabelaSumarioUI( ExecucaoVO.titulos, ExecucaoVO.casos, ExecucaoVO.tarefas, ConfiguracaoVO.quadrado, ConfiguracaoVO.quadrado*6, ConfiguracaoVO.quadrado*27, ConfiguracaoVO.quadrado*5);
	}

	public ControladorUI certificarControlador(){
		setIntervencaoUI( new IntervencaoUI( getMonitor(), getTabelaSumario()));
		ControladorUI controladorUI = new ControladorUI( getIntervencaoUI());
		return controladorUI;
	}

}

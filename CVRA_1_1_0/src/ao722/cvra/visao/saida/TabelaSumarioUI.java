package ao722.cvra.visao.saida;

public class TabelaSumarioUI
	extends javax.swing.JScrollPane {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7717687907352363854L;

	public TabelaSumarioUI( String[] arrNomeColuna, String[] arrCodigoTeste, String[] arrScriptTeste, int posicaoH, int posicaoV, int largura, int altura) {
		int j = 0;
		String strCasos = "", strTarefas = "";

		System.out.println( "arrNomeColuna: "+ arrNomeColuna.toString() +", arrCodigoTeste: "+ arrCodigoTeste.toString());

		for( int i=0; i<arrCodigoTeste.length; i++){

			if( !arrCodigoTeste[i].contains( "CONSTITUICAO")){

				if( strCasos.isEmpty() && strTarefas.isEmpty()){
					strCasos = arrCodigoTeste[i];
					strTarefas = arrScriptTeste[i];
				}
				else{
					strCasos += "@"+ arrCodigoTeste[i];
					strTarefas += "@"+ arrScriptTeste[i];
				}

				j++;
			}

		}

		System.out.println("j: "+ j +", strCasos: "+ strCasos +", strTarefas: "+ strTarefas);
		Object[][] dados;

		if( j == 1){
			dados = new Object[2][2];
			dados[0][0] = strCasos;
			dados[0][1] = strTarefas;
		}
		else {
			dados = new Object[j][j];

			for( int i=0; i < j; i++){
				dados[i][0] = strCasos.split("@")[i];
				dados[i][1] = strTarefas.split("@")[i];
			}

		}

		javax.swing.JTable tabela = new javax.swing.JTable( dados, arrNomeColuna);

		tabela.setVisible( true);
		tabela.setEnabled(false);
		this.setViewportView( tabela);

		this.setVisible( true);
		this.setLocation( posicaoH, posicaoV);
		this.setSize( largura, altura);
	}

}

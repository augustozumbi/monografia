package ao722.cvra.visao.saida;

import ao722.cvra.controle.interpretacao.CVRABL;
import ao722.cvra.modelo.estado.CVRAVO;

public class CVRA
	extends javax.swing.JFrame  {

	// Propriedades
	private static final long serialVersionUID = 1L;
	private CVRABL cVRABL;
	private BarraMenusUI barraMenu;
//	private VisorUI monitor;
//	private TecladoTelefonicoUI tecladoTelefonicoUI;
//	private ControladorUI controladorUI;
//	private InteracaoUI interacaoUI;
//	private	IntervencaoUI intervencaoUI;

	// Construtores
	public CVRA(){
		super( CVRAVO.TITULO);
		CVRA cVRA = this;
		cVRA.setDefaultCloseOperation( javax.swing.JFrame.EXIT_ON_CLOSE);
		cVRA.getContentPane( ).setLayout( null);
		setCVRABL( new CVRABL());
		gerarComponentes();
		cVRA.setJMenuBar( getBarraMenu());
		cVRA.setSize( ao722.cvra.modelo.estado.ConfiguracaoVO.quadrado*16, ao722.cvra.modelo.estado.ConfiguracaoVO.quadrado*12);
		cVRA.setVisible( true);
	}

	// Getters and Setters
//	public VisorUI getMonitor() {
//		return monitor;
//	}
//	public void setMonitor( VisorUI monitor) {
//		this.monitor = monitor;
//	}
//
//	public TecladoTelefonicoUI getTecladoTelefonicoUI() {
//		return tecladoTelefonicoUI;
//	}
//	public void setTecladoTelefonicoUI( TecladoTelefonicoUI tecladoTelefonicoUI) {
//		this.tecladoTelefonicoUI = tecladoTelefonicoUI;
//	}
//
//	public InteracaoUI getInteracaoUI() {
//		return interacaoUI;
//	}
//	public void setInteracaoUI(InteracaoUI interacaoUI) {
//		this.interacaoUI = interacaoUI;
//	}

//	public ControladorUI getControladorUI() {
//		return controladorUI;
//	}
//	public void setControladorUI(ControladorUI controladorUI) {
//		this.controladorUI = controladorUI;
//	}
//
//	public IntervencaoUI getIntervencaoUI() {
//		return intervencaoUI;
//	}
//	public void setIntervencaoUI(IntervencaoUI intervencaoUI) {
//		this.intervencaoUI = intervencaoUI;
//	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BarraMenusUI getBarraMenu() {
		return barraMenu;
	}
	public void setBarraMenu(BarraMenusUI barraMenu) {
		this.barraMenu = barraMenu;
	}

	public CVRABL getCVRABL() {
		return cVRABL;
	}
	public void setCVRABL(CVRABL cVRABL) {
		this.cVRABL = cVRABL;
	}

	// Metodos
	public void gerarComponentes(){
		setBarraMenu( simularBarraMenu());
	}

//	public void gerarComponentesSimulacao(){
//		SimuladorUI simuladorUI = this;
//		simuladorUI.getContentPane().removeAll();
//		setBarraMenu( simularBarraMenu());
//		setMonitor( simularMonitor());
//		setTecladoTelefonicoUI( simularTecladoTelefonico( ));
//		simuladorUI.setVisible( true);
//		simuladorUI.setJMenuBar( getBarraMenu());
//		simuladorUI.getContentPane().add( ( javax.swing.JLabel)getMonitor());

//		for( int i=0; i < getTecladoTelefonicoUI().getTeclado().size(); i++){
//			simuladorUI.getContentPane().add( ( javax.swing.JButton)getTecladoTelefonicoUI().getTeclado().get( i));
//		}

//		simuladorUI.setSize( ao722.simulacao.modelo.estado.TecladoTelefonicoVO.larguraTotal, ao722.simulacao.modelo.estado.ConfiguracaoVO.quadrado*26);
//	}

//	public void gerarComponentesCertificacao(){
//		SimuladorUI simuladorUI = this;
//		simuladorUI.getContentPane().removeAll();
//		setBarraMenu( simularBarraMenu());
//		setMonitor( simularMonitor());
//		setControladorUI( simularControlador( ));
//		simuladorUI.setVisible( true);
//		simuladorUI.setJMenuBar( getBarraMenu());
//		simuladorUI.getContentPane().add( ( javax.swing.JLabel)getMonitor());

//		for( int i=0; i < getControladorUI().getTeclado().size(); i++){
//			simuladorUI.getContentPane().add( ( javax.swing.JButton)getControladorUI().getTeclado().get( i));
//		}

//		simuladorUI.setSize( ao722.simulacao.modelo.estado.ControladorVO.larguraTotal, ao722.simulacao.modelo.estado.ConfiguracaoVO.quadrado*16);
//	}

	public BarraMenusUI simularBarraMenu(){
		CVRA cVRA = this;
		BarraMenusUI barraMenu = new BarraMenusUI( cVRA);
		return barraMenu;
	}

//	public VisorUI simularMonitor(){
//		return new VisorUI( ConfiguracaoVO.quadrado, ConfiguracaoVO.quadrado, ConfiguracaoVO.quadrado*15, ConfiguracaoVO.quadrado*1);
//	}

//	public TecladoTelefonicoUI simularTecladoTelefonico(){
//		setInteracaoUI( new InteracaoUI( getMonitor()));
//		TecladoTelefonicoUI tecladoTelefonicoUI = new TecladoTelefonicoUI( getInteracaoUI());
//		return tecladoTelefonicoUI;
//	}

//	public ControladorUI simularControlador(){
//		setIntervencaoUI( new IntervencaoUI( getMonitor()));
//		ControladorUI controladorUI = new ControladorUI( getIntervencaoUI());
//		return controladorUI;
//	}

	public static void main( String[] args){
		CVRA cVRA = new CVRA();
	}

}

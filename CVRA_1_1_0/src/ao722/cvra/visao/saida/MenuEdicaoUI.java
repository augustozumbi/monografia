package ao722.cvra.visao.saida;

import ao722.cvra.visao.entrada.AparatoUI;
import ao722.cvra.visao.entrada.PlataformaUI;
import ao722.cvra.visao.entrada.PreferenciasUI;
import ao722.cvra.visao.entrada.PropriedadesUI;
import ao722.cvra.visao.entrada.ScriptUI;
import ao722.cvra.visao.entrada.SumarioUI;


public class MenuEdicaoUI
	extends javax.swing.JMenu {

	private static final long serialVersionUID = -8525447311787511572L;

	public MenuEdicaoUI( String nome, CVRA simuladorUI) {
		super(nome);

		this.add( new ScriptUI( simuladorUI)).setEnabled( simuladorUI.getCVRABL().isSimulacao());
		this.add( new PreferenciasUI( simuladorUI)).setEnabled( simuladorUI.getCVRABL().isSimulacao());

		this.addSeparator();

		this.add( new SumarioUI( simuladorUI)).setEnabled( simuladorUI.getCVRABL().isCertificacao());
		this.add( new PlataformaUI( simuladorUI)).setEnabled( simuladorUI.getCVRABL().isCertificacao());
		this.add( new AparatoUI( simuladorUI)).setEnabled( simuladorUI.getCVRABL().isCertificacao());

		this.addSeparator();

		this.add( new PropriedadesUI( simuladorUI));
	}
	
}

package ao722.cvra.visao.saida;

import ao722.cvra.modelo.estado.ExecucaoVO;
import ao722.cvra.modelo.estado.LocalizacaoVO;

public class VisorUI
	extends javax.swing.JLabel {

	// Propriedades
	private static final long serialVersionUID = 1L;

	// Construtores
	public VisorUI(){}
	public VisorUI( int posicaoH, int posicaoV, int largura, int altura){
		this.setVisible( true);
		this.setText( LocalizacaoVO.trajetoria);
		this.setBounds( posicaoH, posicaoV, largura, altura);
	}

	public void atualizarExecucao(){
		this.setText( "executando script "+ ExecucaoVO.linha);
	}
	
	public void atualizarInterrupcao(){
		this.setText( "interrompida execucao");
	}

}

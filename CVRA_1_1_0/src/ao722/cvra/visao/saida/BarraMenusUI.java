package ao722.cvra.visao.saida;

import ao722.cvra.modelo.estado.CVRAVO;

public class BarraMenusUI
	extends javax.swing.JMenuBar {

	private static final long serialVersionUID = -7981913752501566022L;

	public BarraMenusUI( CVRA cVRA) {
		this.add( new MenuRealizacaoUI( CVRAVO.REALIZACAO, cVRA));
		this.add( new MenuEdicaoUI( CVRAVO.EDICAO, cVRA));
		this.add( new MenuConsultaUI( CVRAVO.CONSULTA, cVRA));
	}

}

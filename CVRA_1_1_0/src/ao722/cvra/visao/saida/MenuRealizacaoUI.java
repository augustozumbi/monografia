package ao722.cvra.visao.saida;

import ao722.cvra.visao.entrada.CertificacaoUI;
import ao722.cvra.visao.entrada.ConstrucaoUI;
import ao722.cvra.visao.entrada.ContingenciaUI;
import ao722.cvra.visao.entrada.SaidaUI;
import ao722.cvra.visao.entrada.SimulacaoUI;
import ao722.cvra.visao.saida.CVRA;

public class MenuRealizacaoUI 
	extends javax.swing.JMenu {

	// Propriedades
	private static final long serialVersionUID = 7471330635595003566L;

	public MenuRealizacaoUI( String name, CVRA cVRA) {
		super( name);

		this.add( new ConstrucaoUI( cVRA));
		this.add( new ContingenciaUI( cVRA));

		this.addSeparator();

		this.add( new SimulacaoUI( cVRA));
		this.add( new CertificacaoUI( cVRA));

		this.addSeparator();

		this.add( new SaidaUI( cVRA));
	}

}

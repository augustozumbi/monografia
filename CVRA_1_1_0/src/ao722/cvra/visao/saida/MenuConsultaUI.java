package ao722.cvra.visao.saida;

import ao722.cvra.visao.entrada.AtualizacaoUI;
import ao722.cvra.visao.entrada.ConsultaUI;
import ao722.cvra.visao.entrada.FacUI;
import ao722.cvra.visao.entrada.ManualUI;
import ao722.cvra.visao.entrada.SuporteUI;

public class MenuConsultaUI
	extends javax.swing.JMenu {

	private static final long serialVersionUID = -8525447311787511572L;

	public MenuConsultaUI( String name, CVRA simuladorUI) {
		super(name);
		
		this.add( new ManualUI( simuladorUI));
		this.add( new FacUI( simuladorUI));

		this.addSeparator();
		
		this.add( new AtualizacaoUI( simuladorUI));
		this.add( new SuporteUI(simuladorUI));
		
		this.addSeparator();

		this.add( new ConsultaUI( simuladorUI));

	}
	
}

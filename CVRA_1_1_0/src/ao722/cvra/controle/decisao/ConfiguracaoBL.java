package ao722.cvra.controle.decisao;

import ao722.cvra.controle.interpretacao.CVRABL;
import ao722.cvra.modelo.estado.ConfiguracaoVO;
import ao722.cvra.modelo.transicao.ConfiguracaoDAO;

public class ConfiguracaoBL {

	// Propriedades
	private boolean configurado;
	private ConfiguracaoDAO configuracaoDAO;
	
	// Construtores
	public ConfiguracaoBL( CVRABL simuladorBL){
		setConfiguracaoDAO( new ConfiguracaoDAO());
		configurarCVRA( simuladorBL);
	}
	// Getters and Setters
	public boolean isConfigurado() {
		return configurado;
	}
	public void setConfigurado( boolean configurado) {
		this.configurado = configurado;
	}
	public ConfiguracaoDAO getConfiguracaoDAO() {
		return configuracaoDAO;										
	}
	public void setConfiguracaoDAO( ConfiguracaoDAO configuracaoDAO) {
		this.configuracaoDAO = configuracaoDAO;
	}

	// Metodos
	public void configurarCVRA( CVRABL cVRABL){

		if( cVRABL.isSimulacao()){	
			getConfiguracaoDAO().configurarSimulacao();
			System.out.println( "ConfiguracaoVO.script: ".concat(ConfiguracaoVO.script));

			setConfigurado( ConfiguracaoVO.limiteTentativas > 0);
			setConfigurado( ConfiguracaoVO.quadrado > 12);
			setConfigurado( ConfiguracaoVO.caminho != null);
			setConfigurado( ConfiguracaoVO.script != null);
		}
		else if( cVRABL.isCertificacao()){	
			getConfiguracaoDAO().configurarCertificacao();
			System.out.println( "ConfiguracaoVO.sumario: ".concat(ConfiguracaoVO.sumario));

			setConfigurado( ConfiguracaoVO.sumario != null);
			setConfigurado( ConfiguracaoVO.extensao != null);
			setConfigurado( ConfiguracaoVO.destino != null);
			setConfigurado( ConfiguracaoVO.logURL != null);
			setConfigurado( ConfiguracaoVO.tempoEntreDigitos > 0);
		}
		else {
			getConfiguracaoDAO().definirConfiguracao();
			System.out.println( "Default");
			//getConfiguracaoDAO().resgatarPadroes();
		}

	}

}

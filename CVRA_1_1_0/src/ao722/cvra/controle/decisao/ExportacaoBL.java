package ao722.cvra.controle.decisao;

import ao722.cvra.controle.interpretacao.IntervencaoBL;
import ao722.cvra.modelo.transicao.ExportacaoDAO;

public class ExportacaoBL {

	// Propriedades e Atributos
	private ExportacaoDAO exportacaoDAO;

	// Construtores de Sobrecargas
	public ExportacaoBL( IntervencaoBL intervencaoBL) {
		// TODO Auto-generated constructor stub
		setExportacaoDAO( new ExportacaoDAO( ));
		processar( intervencaoBL);
	}

	// Encapsulamento, Getters and setters
	public ExportacaoDAO getExportacaoDAO() {
		return exportacaoDAO;
	}
	public void setExportacaoDAO(ExportacaoDAO exportacaoDAO) {
		this.exportacaoDAO = exportacaoDAO;
	}

	// Operacoes e comportamentos
	public void processar( IntervencaoBL intervencaoBL){

		if( intervencaoBL.isExportacao() || intervencaoBL.isApuracaoEsgotada()){
			predefinirArquivo();
			requererEscolha();

			intervencaoBL.interpretarExportacao();
			if( intervencaoBL.isArquivoEscolhido()){
				exportarRelatorio( intervencaoBL);
			}

			if( intervencaoBL.isApuracaoEsgotada()){
				limparExportaveis(intervencaoBL);
			}

		}
		else if(intervencaoBL.isLimpeza()){
			limparExportaveis(intervencaoBL);
		}
		
	}

	public void predefinirArquivo(){
		// TODO Auto-generated constructor stub
		getExportacaoDAO().predefinirArquivoSelecionado();
	}

	public void requererEscolha(){
		// TODO Auto-generated constructor stub
		getExportacaoDAO().mostrarSelecionadorArquivo();
	}

	public void exportarRelatorio( IntervencaoBL intervencaoBL){
		// TODO Auto-generated constructor stub
		getExportacaoDAO().obterArquivoSelecionado();
		intervencaoBL.interpretarExportacao();

		if(intervencaoBL.isExportacaoHiperTexto()){
			transcreverPadrao( intervencaoBL);
		}

	}

	public void transcreverPadrao( IntervencaoBL intervencaoBL){
		getExportacaoDAO().pegarRelatorioArquivado();
		intervencaoBL.interpretarExportacao();

		if( intervencaoBL.isRelatorioArmazenado()){
			getExportacaoDAO().medirTamanhoRelatorioArquivado();
			getExportacaoDAO().lerRelatorioArquivado();
			getExportacaoDAO().escreverRelatorioExportacao();
		}

	}

	public void limparExportaveis( IntervencaoBL intervencaoBL){
		getExportacaoDAO().mostrarConfirmadorLimpeza();
		intervencaoBL.interpretarExportacao();
		
		if( intervencaoBL.isLimpezaConfirmada()){
			getExportacaoDAO().limparHistoricoArquivado();
			getExportacaoDAO().limparDossieArquivado();
			getExportacaoDAO().limparRelatorioArquivado();
		}

	}

}

package ao722.cvra.controle.decisao;

import ao722.cvra.controle.interpretacao.CVRABL;
import ao722.cvra.modelo.estado.ConstituicaoVO;
import ao722.cvra.modelo.transicao.ConstituicaoDAO;

public class ConstituicaoBL {
	
	// Propriedades
	private boolean chavesEncontradas, desfechosEncontrados, integracaoEncontrada;
	private ConstituicaoDAO constituicaoDAO;

	// Construtores
	public ConstituicaoBL( CVRABL cVRABL){
		setConstituicaoDAO( new ConstituicaoDAO());
		constituirCVRA( cVRABL);
	}
	// Getters and Setters
	public boolean isChavesEncontradas() {
		return chavesEncontradas;
	}
	public void setChavesEncontradas(boolean chavesEncontradas) {
		this.chavesEncontradas = chavesEncontradas;
	}

	public boolean isDesfechosEncontrados() {
		return desfechosEncontrados;
	}
	public void setDesfechosEncontrados(boolean desfechosEncontrados) {
		this.desfechosEncontrados = desfechosEncontrados;
	}

	public boolean isIntegracaoEncontrada() {
		return integracaoEncontrada;
	}
	public void setIntegracaoEncontrada(boolean integracaoEncontrada) {
		this.integracaoEncontrada = integracaoEncontrada;
	}

	public ConstituicaoDAO getConstituicaoDAO() {
		return constituicaoDAO;
	}
	public void setConstituicaoDAO(ConstituicaoDAO constituicaoDAO) {
		this.constituicaoDAO = constituicaoDAO;
	}

	// Metodos
	public void constituirCVRA( CVRABL cVRABL){

		if( cVRABL.isSimulacao()){
			constituirSimulacao();
		}
		else if( cVRABL.isCertificacao()){
			constituirCertificacao();
		}
		else {

		}

	}
	
	public void constituirSimulacao(){
		getConstituicaoDAO().obterChaveamentoSimulacao();
		setChavesEncontradas( ConstituicaoVO.chaves != null && ConstituicaoVO.chaves.indexOf(";")>0);
		System.out.println( "ConstituicaoVO.chaves: ".concat(ConstituicaoVO.chaves));

		if( isChavesEncontradas()){
			getConstituicaoDAO().definirChaveamentoSimulacao();
		}
		else {
			getConstituicaoDAO().contingenciarChaveamentoSimulacao();
		}

		getConstituicaoDAO().obterDesfechos();
		setDesfechosEncontrados( ConstituicaoVO.desfechos.indexOf(";")>0);
		System.out.println( "ConstituicaoVO.desfechos: ".concat(ConstituicaoVO.desfechos));

		if( isDesfechosEncontrados()){
			getConstituicaoDAO().definirDesfechos();
		}
		else {
			getConstituicaoDAO().contingenciarDesfechos();
		}

		getConstituicaoDAO().obterComandos();
		System.out.println( "ConstituicaoVO.comandos: ".concat(ConstituicaoVO.comandos));
	}

	public void constituirCertificacao(){
		getConstituicaoDAO().obterChaveamentoCertificacao();
		setChavesEncontradas( ConstituicaoVO.chaves != null && ConstituicaoVO.chaves.indexOf(";")>0);

		if( isChavesEncontradas()){
			getConstituicaoDAO().definirChaveamentoCertificacao();
		}

		getConstituicaoDAO().obterIntegracao();
		setIntegracaoEncontrada( ConstituicaoVO.chaves != null && ConstituicaoVO.chaves.indexOf(";")>0);

		if( isIntegracaoEncontrada()){
			getConstituicaoDAO().definirIntegracao();
		}

		getConstituicaoDAO().obterCampos();
		//setCamposEncontrados( ConstituicaoVO.campos != null && ConstituicaoVO.campos.indexOf(";")>0);

	}

}

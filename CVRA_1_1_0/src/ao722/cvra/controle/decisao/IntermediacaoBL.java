package ao722.cvra.controle.decisao;

import ao722.cvra.contrato.fornecedor.IntegracaoTelefonicaAESPeer;
import ao722.cvra.controle.interpretacao.IntervencaoBL;
import ao722.cvra.modelo.transicao.IntermediacaoDAO;

public class IntermediacaoBL {

	// Propriedades, atributos
	private boolean terminalEncontrado, ligadoChamando, ligadoEncontrado, chamadaAtendida, digitoTomPreenchido; 
	private IntermediacaoDAO intermediacaoDAO;

	// Construtores
	public IntermediacaoBL( IntervencaoBL intervencaoBL) {
		setIntermediacaoDAO( new IntermediacaoDAO());
	}
	// Getters and setters
	public IntermediacaoDAO getIntermediacaoDAO() {
		return intermediacaoDAO;
	}
	public void setIntermediacaoDAO(IntermediacaoDAO intermediacaoDAO) {
		this.intermediacaoDAO = intermediacaoDAO;
	}

	public boolean isTerminalEncontrado() {
		return terminalEncontrado;
	}
	public void setTerminalEncontrado(boolean terminalEncontrado) {
		this.terminalEncontrado = terminalEncontrado;
	}

	public boolean isLigadoChamando() {
		return ligadoChamando;
	}
	public void setLigadoChamando(boolean ligadoChamando) {
		this.ligadoChamando = ligadoChamando;
	}

	public boolean isLigadoEncontrado() {
		return ligadoEncontrado;
	}
	public void setLigadoEncontrado(boolean ligadoEncontrado) {
		this.ligadoEncontrado = ligadoEncontrado;
	}

	public boolean isChamadaAtendida() {
		return chamadaAtendida;
	}
	public void setChamadaAtendida(boolean chamadaAtendida) {
		this.chamadaAtendida = chamadaAtendida;
	}

	public boolean isDigitoTomPreenchido() {
		return digitoTomPreenchido;
	}
	public void setDigitoTomPreenchido(boolean digitoTomPreenchido) {
		this.digitoTomPreenchido = digitoTomPreenchido;
	}

	public void fazerChamada( IntervencaoBL intervencaoBL){
//		getIntermediacaoDAO().chamar();
		getIntermediacaoDAO().definirChamador();
		getIntermediacaoDAO().definirAparelhagem();
		intervencaoBL.interpretarIntermediacao();
		
		if( intervencaoBL.isSegurancaDesligada()){
			getIntermediacaoDAO().definirAparelho();
			intervencaoBL.interpretarIntermediacao();
		}
		else{
			getIntermediacaoDAO().escolherAparelho();
			intervencaoBL.interpretarIntermediacao();
			
			while( !intervencaoBL.isAparelhoEncontrado()){
				getIntermediacaoDAO().saltarAparelho();
				getIntermediacaoDAO().escolherAparelho();
				intervencaoBL.interpretarIntermediacao();
	
				if(!intervencaoBL.isAparelhagemEsgotada()){ break;}

			}
			
		}

		if( intervencaoBL.isAparelhoEncontrado()){
			getIntermediacaoDAO().definirChamada();

			if( intervencaoBL.isDestinoExterno()){
				getIntermediacaoDAO().ligarExterno();
			}
			else {
				getIntermediacaoDAO().ligar();
			}

		}

		//getIntermediacaoDAO().digitar();
		//getIntermediacaoDAO().desligar();
	}

	public void concluirProcesso(){
//		getIntegracaoTelefonicaPeer().desligar();
		getIntermediacaoDAO().desprover();
//		getIntermediacaoDAO().zerarAlerta();
//		getIntermediacaoDAO().zerarLigado();
//		getIntermediacaoDAO().zerarServico();
//		getIntermediacaoDAO().zerarProcedimento();
//		getIntermediacaoDAO().zerarAparelho();
	}

	public void abortarProcesso( IntervencaoBL intervencaoBL){
		getIntermediacaoDAO().zerarAlerta();
		getIntermediacaoDAO().zerarLigado();
		getIntermediacaoDAO().zerarProcedimento();
		getIntermediacaoDAO().zerarAparelho();

		intervencaoBL.interpretarIntermediacao();

		if( intervencaoBL.isLigacaoAtendida()){
			getIntermediacaoDAO().setIntegracaoTelefonicaAESPeer( new IntegracaoTelefonicaAESPeer()); 
			getIntermediacaoDAO().desligar();
		}

		getIntermediacaoDAO().desprover();
	}
	
//	public void chamar(){	
//		IntermediacaoVO.chamador = getIntegracaoTelefonicaPeer().definirChamador( IntermediacaoVO.integrador, ConfiguracaoVO.extensao);
//		getIntermediacaoDAO().definirChamador();
//		IntermediacaoVO.aparelhagem = getIntegracaoTelefonicaPeer().definirAparelhagem( IntermediacaoVO.integrador);
//		getIntermediacaoDAO().definirAparelhagem();
//	}

	public void pegarReceptor( IntervencaoBL intervencaoBL){
//		IntermediacaoVO.ligacao = getIntegracaoTelefonicaPeer().definirLigacao( IntermediacaoVO.call);
		getIntermediacaoDAO().definirLigacao();
		getIntermediacaoDAO().definirLigacoes();
		intervencaoBL.interpretarIntermediacao();

		if( intervencaoBL.isLigacaoValida()){
			identificarOrigem( intervencaoBL);

			while( !intervencaoBL.isOrigemEncontrada()){
				getIntermediacaoDAO().saltarLigado();
				identificarOrigem( intervencaoBL);

				if( intervencaoBL.isLigacaoEsgotada()){ break;}

			}

			getIntermediacaoDAO().zerarLigado();
			identificarDestino( intervencaoBL);

			while( !intervencaoBL.isDestinoEncontrado()){
				getIntermediacaoDAO().saltarLigado();
				identificarDestino( intervencaoBL);

				if( intervencaoBL.isLigacaoEsgotada()){ break;}

			}

			if( intervencaoBL.isOrigemEncontrada()){
				intervencaoBL.interpretarIntermediacao();
			
				while( !intervencaoBL.isLigacaoAtendida()){
					getIntermediacaoDAO().aguardarToque();
					getIntermediacaoDAO().saltarAlerta();
					intervencaoBL.interpretarIntermediacao();
	
					if( intervencaoBL.isAlertasEsgotados()){ break;}

				}

				if( intervencaoBL.isLigacaoAtendida()){
					getIntermediacaoDAO().definirVinculacao();
					intervencaoBL.interpretarIntermediacao();

					if( intervencaoBL.isVinculacaoValida()){
						getIntermediacaoDAO().definirMidia();
					}

				}

			}

		}

	}

	public void realizarDesconexao( IntervencaoBL intervencaoBL){
		intervencaoBL.interpretarIntermediacao();

		if( intervencaoBL.isLigacaoAtendida() && !intervencaoBL.isAbortar()){
			getIntermediacaoDAO().desligar();
		}

	}

	public void concluirProcedimento( IntervencaoBL intervencaoBL){
		intervencaoBL.interpretarIntermediacao();

		getIntermediacaoDAO().zerarAlerta();

		while( intervencaoBL.isLigacaoAtendida()){
			getIntermediacaoDAO().aguardarToque();
			getIntermediacaoDAO().saltarAlerta();
			intervencaoBL.interpretarIntermediacao();

			if( !intervencaoBL.isLigacaoAtendida()){ 
				getIntermediacaoDAO().zerarAlerta();
				break;
			}

		}

		getIntermediacaoDAO().zerarAlerta();
		getIntermediacaoDAO().zerarLigado();
		getIntermediacaoDAO().zerarProcedimento();
		getIntermediacaoDAO().zerarAparelho();
		getIntermediacaoDAO().aguardarToque();
	}

	public void prover( IntervencaoBL intervencaoBL){
		getIntermediacaoDAO().setIntegracaoTelefonicaAESPeer( new IntegracaoTelefonicaAESPeer()); 
		getIntermediacaoDAO().definirInstancia();
		getIntermediacaoDAO().definirCatalogo();
		getIntermediacaoDAO().definirServicos();
		getIntermediacaoDAO().definirServico();
		intervencaoBL.interpretarIntermediacao();

		while( !intervencaoBL.isServicoEncontrado() && !intervencaoBL.isPortfolioEsgotado()){
			getIntermediacaoDAO().saltarServico();
			getIntermediacaoDAO().definirServico();
			intervencaoBL.interpretarIntermediacao();

			if( intervencaoBL.isPortfolioEsgotado()){ break; }

		}

		if( intervencaoBL.isServicoEncontrado()){
			getIntermediacaoDAO().definirExpressaoAcesso();
			getIntermediacaoDAO().definirIntegrador();
		}

	}

	public void digitar( IntervencaoBL intervencaoBL){
		getIntermediacaoDAO().definirDTMF();
		intervencaoBL.interpretarIntermediacao();

		if( intervencaoBL.isdTMFValido() && !intervencaoBL.isAbortar()){
			getIntermediacaoDAO().gerarTom();
		}

	}

	public void identificarOrigem( IntervencaoBL intervencaoBL){
		getIntermediacaoDAO().definirNumeroOrigem();
		getIntermediacaoDAO().definirLigador();
		intervencaoBL.interpretarIntermediacao();
	}

	public void identificarDestino( IntervencaoBL intervencaoBL){
		getIntermediacaoDAO().definirNumeroDestino();
		getIntermediacaoDAO().definirLigado();
		intervencaoBL.interpretarIntermediacao();
	}
	
}

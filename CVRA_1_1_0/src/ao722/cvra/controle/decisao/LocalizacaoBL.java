package ao722.cvra.controle.decisao;

import ao722.cvra.controle.interpretacao.InteracaoBL;
import ao722.cvra.modelo.transicao.LocalizacaoDAO;

public class LocalizacaoBL {
	
	private boolean navegou, desligou;
	private LocalizacaoDAO localizacaoDAO;

	public boolean isNavegou() {
		return navegou;
	}
	public void setNavegou(boolean navegou) {
		this.navegou = navegou;
	}

	public boolean isDesligou() {
		return desligou;
	}
	public void setDesligou(boolean desligou) {
		this.desligou = desligou;
	}
	
	public LocalizacaoDAO getDeslocamentoDAO() {
		return localizacaoDAO;
	}

	public void setLocalizacaoDAO(LocalizacaoDAO localizacaoDAO) {
		this.localizacaoDAO = localizacaoDAO;
	}

	public LocalizacaoBL( InteracaoBL interacaoBL) {
		setLocalizacaoDAO( new LocalizacaoDAO());
		realizarSolicitacao( interacaoBL);
	}
	
	// Metodos
	public void realizarSolicitacao( InteracaoBL interacaoBL){

		if( interacaoBL.isPossivel()){

			if( interacaoBL.isIniciar() || interacaoBL.isChamar()){
				entrar();
			}
			else if( interacaoBL.isNavegacao()){			// Foi encontrado um menu para a opcao teclada
				realizarNavegacao( interacaoBL);
			}
			else if( interacaoBL.isDesfecho()){		// Foi encontrado um desfecho para a opcao teclada
				realizarDesfecho( interacaoBL);
			}
			else if( interacaoBL.isRepeticao()){	// Foi identificado pedido para repetir o menu
				realizarRepeticao();
			}
			else if( interacaoBL.isRetorno()){		// Foi identificado pedido para retornar ao menu anterior
				realizarRetorno();
			}
			else if( interacaoBL.isDesligar()){		// Foi identificado pedido para retornar ao menu anterior
				abandonar();
			}
			else {						// Nao foi encontrado um destino para a opcao teclada
				realizarPonderacao( interacaoBL);
			}

		}

	}

	public void entrar(){
		getDeslocamentoDAO().definirEntrou();
		setNavegou( true);
	}

	public void abandonar(){
		getDeslocamentoDAO().definirAbandonou();
		setDesligou( true);
	}

	public void realizarNavegacao( InteracaoBL interacaoBL){
		
		if( interacaoBL.isValida()){
			getDeslocamentoDAO().definirNavegou();
		}

		setNavegou( interacaoBL.isValida());
	}
	
	public void realizarRetorno( ){
		getDeslocamentoDAO().definirNavegou();
		setNavegou( true);
	}
	
	public void realizarRepeticao( ){
		setNavegou( true);
	}

	public void realizarPonderacao( InteracaoBL interacaoBL){

		if( interacaoBL.isInsucesso()){
			getDeslocamentoDAO().definirExcedeu();
			setNavegou( interacaoBL.isInsucesso());
		}

	}

	public void realizarDesfecho( InteracaoBL interacaoBL){

		if( interacaoBL.isTransferencia()){
			getDeslocamentoDAO().definirTransferido();
		}
		else {
			getDeslocamentoDAO().definirDesconectado();
			getDeslocamentoDAO().definirAssuntoDesconectado();
		}

	}

}

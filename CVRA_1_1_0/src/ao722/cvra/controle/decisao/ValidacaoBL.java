package ao722.cvra.controle.decisao;

import ao722.cvra.controle.interpretacao.IntervencaoBL;
import ao722.cvra.modelo.estado.ControleMaterialVO;
import ao722.cvra.modelo.estado.ValidacaoVO;
import ao722.cvra.modelo.transicao.ValidacaoDAO;

public class ValidacaoBL {

	// Atributos
	private ConexaoBL conexaoBL;
	private ReportagemBL reportagemBL;
	private ValidacaoDAO validacaoDAO;

	// Construtores
	public ValidacaoBL( IntervencaoBL intervencaoBL){
		setValidacaoDAO( new ValidacaoDAO( ));
		setReportagemBL( new ReportagemBL());
		validar( intervencaoBL);
	}
	// Encapsulamento	
	public ConexaoBL getConexaoBL() {
		return conexaoBL;
	}
	public void setConexaoBL(ConexaoBL conexaoBL) {
		this.conexaoBL = conexaoBL;
	}

	public ReportagemBL getReportagemBL() {
		return reportagemBL;
	}
	public void setReportagemBL(ReportagemBL reportagemBL) {
		this.reportagemBL = reportagemBL;
	}

	public ValidacaoDAO getValidacaoDAO() {
		return validacaoDAO;
	}
	public void setValidacaoDAO(ValidacaoDAO validacaoDAO) {
		this.validacaoDAO = validacaoDAO;
	}

	public void validar( IntervencaoBL intervencaoBL){

		if( ( intervencaoBL.isExecutar() || intervencaoBL.isReportagem()) && !intervencaoBL.isInterrompido() && !intervencaoBL.isSuspenso()){
			getValidacaoDAO().recuperarRelacao();
			setConexaoBL( new ConexaoBL( intervencaoBL));
			apurar( intervencaoBL);
			getConexaoBL().descartarLog();
		}

	}

	public void apurar( IntervencaoBL intervencaoBL){
		System.out.println("{inicio apuracao}");
		getConexaoBL().obterLog( intervencaoBL);
		getValidacaoDAO().zerarTeste();
		getValidacaoDAO().marcarInicioArmazem();
		identificar( intervencaoBL);

		System.out.println( "intervencaoBL.isIdSessaoEncontrada(): "+ intervencaoBL.isIdSessaoEncontrada());
		if( intervencaoBL.isIdSessaoEncontrada()){
			getValidacaoDAO().iniciarProduto();
			produzir( intervencaoBL);
		}

		getValidacaoDAO().saltarTeste();
		intervencaoBL.interpretarValidacao();

		while( !intervencaoBL.isApuracaoEsgotada()){
			getValidacaoDAO().retornarInicioArmazem();
			identificar( intervencaoBL);

			if( intervencaoBL.isIdSessaoEncontrada()){
				produzir( intervencaoBL);
			}

			getValidacaoDAO().saltarTeste();
			intervencaoBL.interpretarValidacao();
		}

		System.out.println("{fim apuracao}");
	}

	public void identificar( IntervencaoBL intervencaoBL){
		System.out.println("{inicio identificacao}");
		// BUSCA PELA DATA HORA DO TESTE NO LOG
		getReportagemBL().zerarRelatorio();
		getValidacaoDAO().definirMomento();
		getValidacaoDAO().definirMomentoInicial();
		getValidacaoDAO().definirMomentoFinal();
		getValidacaoDAO().lerLinha();
		intervencaoBL.interpretarValidacao();

		System.out.println( "intervencaoBL.isLinhaLog(): "+ intervencaoBL.isLinhaLog());
		if( intervencaoBL.isLinhaLog()){
			getValidacaoDAO().subtrairHorario();
			getValidacaoDAO().definirHorarioInicial();
			intervencaoBL.interpretarValidacao();
		}

		// LACO DE INSISTENCIA NA BUSCA
		while( !intervencaoBL.isDataInicialEncontrada() && !intervencaoBL.isLogEsgotado()){
			getValidacaoDAO().lerLinha();
			intervencaoBL.interpretarValidacao();

			if( intervencaoBL.isLinhaLog()){
				//System.out.println( "intervencaoBL.isLinhaLog(): "+ intervencaoBL.isLinhaLog());
				getValidacaoDAO().subtrairHorario();
				getValidacaoDAO().definirHorarioInicial();
				intervencaoBL.interpretarValidacao();
			}

			if( intervencaoBL.isDataInicialEncontrada()){
				System.out.println( "intervencaoBL.isDataInicialEncontrada(): "+ intervencaoBL.isDataInicialEncontrada());
				System.out.println("## DATA INICIAL ENCONTRADA: "+ ValidacaoVO.horario);
				break; 
			}

		}

		// BUSCA PELO TELEFONE DE ORIGEM DA CHAMADA NO LOG
		if( !intervencaoBL.isLogEsgotado()){
			System.out.println("!intervencaoBL.isLogEsgotado(): "+ !intervencaoBL.isLogEsgotado());
			getValidacaoDAO().definirHorarioFinal();
			getValidacaoDAO().lerLinha();

			intervencaoBL.interpretarValidacao();

			if( intervencaoBL.isLinhaDeANI() && !intervencaoBL.isAniEncontrado()){	
				System.out.println( "intervencaoBL.isLinhaDeANI(): "+ intervencaoBL.isLinhaDeANI() +", !intervencaoBL.isAniEncontrado(): "+ !intervencaoBL.isAniEncontrado());
				System.out.println("## INTERLIGACAO APAGOU ANI");
				// TODO implementacao melhor de definicao de flag
				intervencaoBL.setAniEncontrado(true);
			}
			else{

				while( !intervencaoBL.isAniEncontrado() && !intervencaoBL.isLogEsgotado()){
					getValidacaoDAO().lerLinha();
					intervencaoBL.interpretarValidacao();

					if( intervencaoBL.isLinhaDeANI() && !intervencaoBL.isAniEncontrado()){	
					System.out.println( "intervencaoBL.isLinhaDeANI(): "+ intervencaoBL.isLinhaDeANI() +", !intervencaoBL.isAniEncontrado(): "+ !intervencaoBL.isAniEncontrado());
						System.out.println("## INTERLIGACAO APAGOU ANI");
						intervencaoBL.setAniEncontrado(true);
					}
					else{

						if( intervencaoBL.isAniEncontrado()){ 
							System.out.println( "intervencaoBL.isAniEncontrado(): "+ intervencaoBL.isAniEncontrado());
							System.out.println("## ANI ENCONTRADO");
							break;}

					}

				}

			}
			
			if( intervencaoBL.isAniEncontrado()){
				System.out.println( "intervencaoBL.isAniEncontrado(): "+ intervencaoBL.isAniEncontrado());
				getValidacaoDAO().definirIdSessao();
			}

		}
		else{
			System.out.println("## LOG ESGOTADO AO ENCONTRAR HORARIO: "+ ControleMaterialVO.linha);
		}

		intervencaoBL.interpretarValidacao();
		System.out.println("{fim identificacao}");
	}

	public void evidenciar( IntervencaoBL intervencaoBL){
		System.out.println("{inicio evidenciacao}");
		getValidacaoDAO().iniciarDossie();

		while( !intervencaoBL.isLogEsgotado() && !intervencaoBL.isDataFinalEncontrada()){
			getValidacaoDAO().lerLinha();
			intervencaoBL.interpretarValidacao();

			if( intervencaoBL.isIdSessaoEncontrada()){
				coletarConvencionais( intervencaoBL);
			}
			else {
				coletarExcepcionais( intervencaoBL);

				if( intervencaoBL.isDataFinalEncontrada()){ break;}
			}

		}

		getValidacaoDAO().zerarPosicao();
		System.out.println("{fim evidenciacao}");
	}

	public void relatar( IntervencaoBL intervencaoBL){
		System.out.println("{inicio relato}");
		getReportagemBL().iniciarMapas();
		recuperar( intervencaoBL);
		delatar( intervencaoBL);
		acompanhar( intervencaoBL);
		reconhecer( intervencaoBL);
		avaliar( intervencaoBL);
		getReportagemBL().reportar();
		intervencaoBL.interpretarValidacao();

		if( intervencaoBL.isExcecaoEncontrada() && !intervencaoBL.isOpcaoFornecida() && !intervencaoBL.isExpressaoEncontrada()){
			getReportagemBL().relatarInicioQuebrado( intervencaoBL);
		}
		else {

			if( intervencaoBL.isOpcaoValida() || intervencaoBL.isDigitoEncontrado()){
				getReportagemBL().reportarInicioValido( intervencaoBL);
				getValidacaoDAO().resguardarUltimoMenu();
			}
			else {
				getReportagemBL().relatarInicioInvalido( intervencaoBL);
				getValidacaoDAO().resguardarUltimoMenu();
			}

			reconhecer( intervencaoBL);
			avaliar( intervencaoBL);
			intervencaoBL.interpretarValidacao();

			while( intervencaoBL.isExpressaoEncontrada() && !intervencaoBL.isDossieEsgotado()){
				getReportagemBL().reportarAvaliacao( intervencaoBL);
				getValidacaoDAO().zerarDigito();
				getValidacaoDAO().resguardarUltimoMenu();
				reconhecer( intervencaoBL);
				avaliar( intervencaoBL);
			}

			getValidacaoDAO().prepararExpressaoTransferencia();
			getValidacaoDAO().resgatarPosicaoLocalizada();
			intervencaoBL.interpretarValidacao();
			getValidacaoDAO().resgatarPosicaoLocalizada();
			getValidacaoDAO().restaurarUltimaPosicao();
			getReportagemBL().reportarFimValido(intervencaoBL);
		}

		getReportagemBL().terminar(intervencaoBL);
		getValidacaoDAO().zerarDigito();
		getValidacaoDAO().zerarValidador();
		getValidacaoDAO().zerarMenu();
		getValidacaoDAO().zerarUltimoMenu();
		getValidacaoDAO().zerarUltimaPosicaoDado();
		getValidacaoDAO().zerarUltimaPosicao();
		getValidacaoDAO().zerarUltimaPosicaoOmissao();
		getValidacaoDAO().zerarUltimaPosicaoEngano();
		getValidacaoDAO().zerarPosicao();
		// TODO definir uma implementa��o melhor dessa alteracao de flag
		intervencaoBL.setOpcaoValida(false);
		intervencaoBL.setPrimeiraTentativa(false);
		intervencaoBL.setOpcaoFornecida(false);
		intervencaoBL.interpretarValidacao();
		System.out.println("{fim relato}");
	}

	public void reconhecer( IntervencaoBL intervencaoBL){
		System.out.println("{inicio reconhecimento}");
		System.out.println("ValidacaoVO.posicao: "+ ValidacaoVO.posicao);
		getValidacaoDAO().guardarPosicaoInicial();
		//getValidacaoDAO().restaurarUltimaPosicaoMenu();
		getValidacaoDAO().prepararExpressaoMenu();
		getValidacaoDAO().resgatarPosicaoLocalizada();
		intervencaoBL.interpretarValidacao();

		if( intervencaoBL.isExpressaoEncontrada()){
			getValidacaoDAO().definirPosicaoDado();
			getValidacaoDAO().resguardarUltimaPosicao();
			getValidacaoDAO().prepararExpressaoDoisPontos();
			getValidacaoDAO().resgatarPosicaoLocalizada();
			getValidacaoDAO().definirMenu();
			getValidacaoDAO().definirPosicaoMenu();
			intervencaoBL.interpretarValidacao();

			while( !intervencaoBL.isPosicaoMenuDiferente()){
				System.out.println("{laco de reconhecimento}");
				getValidacaoDAO().guardarPosicaoInicial();
				getValidacaoDAO().prepararExpressaoMenu();
				getValidacaoDAO().resgatarPosicaoLocalizada();
				intervencaoBL.interpretarValidacao();
	
				if( intervencaoBL.isExpressaoEncontrada()){
					getValidacaoDAO().definirPosicaoDado();
					getValidacaoDAO().resguardarUltimaPosicao();
					getValidacaoDAO().prepararExpressaoDoisPontos();
					getValidacaoDAO().resgatarPosicaoLocalizada();
					getValidacaoDAO().definirMenu();
					getValidacaoDAO().definirPosicaoMenu();
				}
	
				intervencaoBL.interpretarValidacao();
			}

			getValidacaoDAO().resguardarPosicaoUltimoMenu();
			getValidacaoDAO().restaurarPosicaoInicial();
		}
		else{
			getValidacaoDAO().restaurarPosicaoInicial();
			
			//System.out.println(ValidacaoVO.dossie);
		}

//		}
		System.out.println("{fim reconhecimento}");
	}

	public void recuperar( IntervencaoBL intervencaoBL){
		System.out.println("{inicio recuperacao}");
		getValidacaoDAO().guardarPosicaoInicial();
		getValidacaoDAO().prepararExpressaoCanal();
		capturarPosicao( intervencaoBL);

		if( intervencaoBL.isExpressaoEncontrada()){
			System.out.println("intervencaoBL.isExpressaoEncontrada(): "+ intervencaoBL.isExpressaoEncontrada());
			capturarDado();
			getReportagemBL().definirCanal();
		}

		getValidacaoDAO().restaurarUltimaPosicao();
		getValidacaoDAO().prepararExpressaoMPP();
		capturarPosicao( intervencaoBL);

		if( intervencaoBL.isExpressaoEncontrada()){
			System.out.println("intervencaoBL.isExpressaoEncontrada(): "+ intervencaoBL.isExpressaoEncontrada());
			capturarDado();
			getReportagemBL().definirMPP();
		}

		getValidacaoDAO().restaurarUltimaPosicao();
		getValidacaoDAO().prepararExpressaoANI();
		capturarPosicao( intervencaoBL);

		if( intervencaoBL.isExpressaoEncontrada()){
			System.out.println("intervencaoBL.isExpressaoEncontrada(): "+ intervencaoBL.isExpressaoEncontrada());
			capturarDado();
			getReportagemBL().definirANI();
		}

		getValidacaoDAO().restaurarUltimaPosicao();
		getValidacaoDAO().prepararExpressaoDNIS();
		capturarPosicao( intervencaoBL);

		if( intervencaoBL.isExpressaoEncontrada()){
			System.out.println("intervencaoBL.isExpressaoEncontrada(): "+ intervencaoBL.isExpressaoEncontrada());
			capturarDado();
			getReportagemBL().definirDNIS();
		}

		getValidacaoDAO().restaurarPosicaoInicial();
		System.out.println("{fim recuperacao}");
	}

	public void delatar( IntervencaoBL intervencaoBL){
		System.out.println("{inicio delacao}");
		getValidacaoDAO().guardarPosicaoInicial();
		getValidacaoDAO().prepararExpressaoExcecao();
		capturarPosicao( intervencaoBL);

		if( intervencaoBL.isExpressaoEncontrada()){
			getValidacaoDAO().definirPosicaoDado();
			getReportagemBL().iniciarDelatoExcecao();

//			getValidacaoDAO().restaurarUltimaPosicao();
			getValidacaoDAO().prepararExpressaoMensagemExcecao();
			capturarPosicao( intervencaoBL);

			if( intervencaoBL.isExpressaoEncontrada()){
				getValidacaoDAO().definirPosicaoDado();
				getReportagemBL().iniciarDelatoMensagemExcecao();
			}

			getValidacaoDAO().restaurarUltimaPosicao();
			getValidacaoDAO().prepararExpressaoCausaExcecao();
			capturarPosicao( intervencaoBL);

			if( intervencaoBL.isExpressaoEncontrada()){
				// DEFINIR LOCAL EM CASO DE NAO SER SERVLET
				getValidacaoDAO().prepararExpressaoLocalExcecao();
				capturarPosicao( intervencaoBL);
	
				if( intervencaoBL.isExpressaoEncontrada()){
					getReportagemBL().complementarDelatoLocalExcecao();
				}

				getValidacaoDAO().restaurarUltimaPosicao();
				getValidacaoDAO().prepararExpressaoExcecaoServlet();
				capturarPosicao( intervencaoBL);
	
				if( intervencaoBL.isExpressaoEncontrada()){
					getValidacaoDAO().definirPosicaoDado();
					getReportagemBL().complementarDelatoLinhaExcecao();
				}

				getValidacaoDAO().restaurarUltimaPosicao();
			}
			else {
				getValidacaoDAO().restaurarUltimaPosicao();
			}

			getReportagemBL().finalizarDelacao();

			// PROCURAR PROXIMA EXCECAO
			getValidacaoDAO().prepararExpressaoExcecao();
			capturarPosicao( intervencaoBL);
	
			while( intervencaoBL.isExpressaoEncontrada()){
				getValidacaoDAO().definirPosicaoDado();
				getReportagemBL().complementarDelatoLinhaExcecao();

//				getValidacaoDAO().restaurarUltimaPosicao();
				getValidacaoDAO().prepararExpressaoMensagemExcecao();
				getValidacaoDAO().resguardarUltimaPosicao();
				getValidacaoDAO().resgatarPosicaoLocalizada();
				intervencaoBL.interpretarValidacao();

				if( intervencaoBL.isExpressaoEncontrada()){
					getValidacaoDAO().definirPosicaoDado();
					getReportagemBL().complementarDelatoMensagemExcecao();
				}

				// PROCURAR O CAUSED BY
				getValidacaoDAO().restaurarUltimaPosicao();
				getValidacaoDAO().prepararExpressaoCausaExcecao();
				getValidacaoDAO().resguardarUltimaPosicao();
				getValidacaoDAO().resgatarPosicaoLocalizada();
				intervencaoBL.interpretarValidacao();

				if( intervencaoBL.isExpressaoEncontrada()){
					// PROCURAR O SERVLETIMPLEMENTATION
					getValidacaoDAO().prepararExpressaoLocalExcecao();
					getValidacaoDAO().resguardarUltimaPosicao();
					getValidacaoDAO().resgatarPosicaoLocalizada();
					intervencaoBL.interpretarValidacao();

					if( intervencaoBL.isExpressaoEncontrada()){
						getReportagemBL().complementarDelatoLocalExcecao();
						getValidacaoDAO().restaurarUltimaPosicao();
					}

					// DEFINIR LOCAL EM CASO DE NAO SER SERVLET
					getValidacaoDAO().prepararExpressaoExcecaoServlet();
					getValidacaoDAO().resguardarUltimaPosicao();
					getValidacaoDAO().resgatarPosicaoLocalizada();
					intervencaoBL.interpretarValidacao();

					if( intervencaoBL.isExpressaoEncontrada()){
						getValidacaoDAO().definirPosicaoDado();
						getReportagemBL().complementarDelatoLinhaExcecao();
					}

					getValidacaoDAO().restaurarUltimaPosicao();
				}
				else {
					getValidacaoDAO().restaurarUltimaPosicao();
				}

				getReportagemBL().finalizarDelacao();

				getValidacaoDAO().restaurarUltimaPosicao();
				getValidacaoDAO().prepararExpressaoExcecao();
				getValidacaoDAO().resguardarUltimaPosicao();
				getValidacaoDAO().resgatarPosicaoLocalizada();
				intervencaoBL.interpretarValidacao();
			}

		}

		getValidacaoDAO().resguardarUltimaPosicao();
		getValidacaoDAO().restaurarPosicaoInicial();
		System.out.println("{fim delacao}");
	}

	public void avaliar( IntervencaoBL intervencaoBL){
		System.out.println("{inicio avaliacao}");
		// BUSCAR NO INPUT DE MENU
		//getValidacaoDAO().restaurarUltimaPosicao();
		getValidacaoDAO().restaurarUltimaPosicaoOmissao();
		getValidacaoDAO().prepararExpressaoOmissao();
		getValidacaoDAO().resgatarPosicaoLocalizada();
		intervencaoBL.interpretarValidacao();

		if( intervencaoBL.isExpressaoEncontrada()){
			System.out.println("intervencaoBL.isExpressaoEncontrada(): "+ intervencaoBL.isExpressaoEncontrada());
			getValidacaoDAO().definirPosicaoDado();
			getValidacaoDAO().resguardarUltimaPosicaoOmissao();
			getValidacaoDAO().resgatarDado();
			getValidacaoDAO().definirValidador();
			intervencaoBL.interpretarValidacao();
		}

		// BUSCAR NO MATCH DE MENU
		if( intervencaoBL.isOpcaoFornecida()){
			System.out.println("intervencaoBL.isOpcaoFornecida(): "+ intervencaoBL.isOpcaoFornecida());
			//getValidacaoDAO().restaurarUltimaPosicao();
			//getValidacaoDAO().resguardarUltimaPosicao();
			getValidacaoDAO().restaurarUltimaPosicaoEngano();
			getValidacaoDAO().prepararExpressaoEngano();
			getValidacaoDAO().resgatarPosicaoLocalizada();
			intervencaoBL.interpretarValidacao();

			if( intervencaoBL.isExpressaoEncontrada()){
				System.out.println("intervencaoBL.isExpressaoEncontrada(): "+ intervencaoBL.isExpressaoEncontrada());
				getValidacaoDAO().definirPosicaoDado();
				getValidacaoDAO().resguardarUltimaPosicaoEngano();
				getValidacaoDAO().resgatarDado();
				getValidacaoDAO().definirValidador();
				intervencaoBL.interpretarValidacao();
			}

			// BUSCAR OPCAO VALIDA
			if( intervencaoBL.isOpcaoValida()){
				System.out.println("intervencaoBL.isOpcaoValida(): "+ intervencaoBL.isOpcaoValida());
				//getValidacaoDAO().restaurarUltimaPosicao();
				//getValidacaoDAO().restaurarUltimaPosicaoEngano();
				//getValidacaoDAO().resguardarUltimaPosicao();
				getValidacaoDAO().restaurarUltimaPosicaoDado();
				getValidacaoDAO().prepararExpressaoDado();
				getValidacaoDAO().resgatarPosicaoLocalizada();
				intervencaoBL.interpretarValidacao();

				if( intervencaoBL.isExpressaoEncontrada()){
					System.out.println("intervencaoBL.isExpressaoEncontrada(): "+ intervencaoBL.isExpressaoEncontrada());
					getValidacaoDAO().definirPosicaoDado();
					getValidacaoDAO().resguardarUltimaPosicaoDado();
					getValidacaoDAO().resgatarDado();
					getValidacaoDAO().definirDigito();
					intervencaoBL.interpretarValidacao();
				}

				//passar( intervencaoBL);
			}
			else {
				System.out.println("intervencaoBL.isOpcaoValida(): "+ intervencaoBL.isOpcaoValida());
				intervencaoBL.interpretarValidacao();

				if( !intervencaoBL.isAvancoOcorrido()){
					System.out.println("!intervencaoBL.isAvancoOcorrido(): "+ !intervencaoBL.isAvancoOcorrido());
					getValidacaoDAO().restaurarUltimaPosicao();
				}

				intervencaoBL.interpretarValidacao();
			}

		}
		else {
			// NO INPUT, QUEBRA DE LINHA
			System.out.println("NO INPUT, QUEBRA DE LINHA");
			intervencaoBL.interpretarValidacao();

			if( !intervencaoBL.isAvancoOcorrido()){
				System.out.println("!intervencaoBL.isAvancoOcorrido(): "+ !intervencaoBL.isAvancoOcorrido() + ", ValidacaoVO.posicao: "+ ValidacaoVO.posicao +", ValidacaoVO.ultimaPosicao: "+ ValidacaoVO.ultimaPosicao);
				System.out.println("intervencaoBL.isOpcaoValida(): "+ intervencaoBL.isOpcaoValida());

				getValidacaoDAO().restaurarUltimaPosicaoDado();
				getValidacaoDAO().prepararExpressaoDado();
				getValidacaoDAO().resgatarPosicaoLocalizada();
				intervencaoBL.interpretarValidacao();

				if( intervencaoBL.isExpressaoEncontrada()){
					System.out.println("intervencaoBL.isExpressaoEncontrada(): "+ intervencaoBL.isExpressaoEncontrada());
					getValidacaoDAO().definirPosicaoDado();
					getValidacaoDAO().resguardarUltimaPosicaoDado();
					getValidacaoDAO().resgatarDado();
					getValidacaoDAO().definirDigito();
					intervencaoBL.interpretarValidacao();
				}

			}
			else {
				System.out.println("OMISSAO");
				getValidacaoDAO().zerarDigito();
			}

			intervencaoBL.interpretarValidacao();
		}

		System.out.println("{fim avaliacao}");
	}
	
	public void acompanhar( IntervencaoBL intervencaoBL){
		System.out.println("{inicio acompanhamento}");
		getValidacaoDAO().guardarPosicaoInicial();
		getValidacaoDAO().prepararExpressaoTransferencia();
		getValidacaoDAO().resgatarPosicaoLocalizada();
		intervencaoBL.interpretarValidacao();

		if( intervencaoBL.isExpressaoEncontrada()){
			getValidacaoDAO().restaurarPosicaoInicial();
			getValidacaoDAO().prepararExpressaoLinhaTransferencia();
			getValidacaoDAO().resgatarPosicaoLocalizada();
			intervencaoBL.interpretarValidacao();

			if( intervencaoBL.isExpressaoEncontrada()){
				getValidacaoDAO().prepararExpressaoVDNTransferencia();
				getValidacaoDAO().resgatarPosicaoLocalizada();
				intervencaoBL.interpretarValidacao();
	
				if( intervencaoBL.isExpressaoEncontrada()){
					getValidacaoDAO().definirPosicaoDado();
					getReportagemBL().resgatarVDNTransferencia();
				}

			}

			getValidacaoDAO().restaurarPosicaoInicial();
			getValidacaoDAO().prepararExpressaoBilheteEnvio();
			getValidacaoDAO().resgatarPosicaoLocalizada();
			intervencaoBL.interpretarValidacao();
		
			if( intervencaoBL.isExpressaoEncontrada()){
				getValidacaoDAO().prepararExpressaoUUI();
				getValidacaoDAO().resgatarPosicaoLocalizada();
				intervencaoBL.interpretarValidacao();
		
				if( intervencaoBL.isExpressaoEncontrada()){
					getValidacaoDAO().definirPosicaoDado();
					getReportagemBL().definirUUIEnvio( intervencaoBL);
				}
		
			}

			getValidacaoDAO().restaurarPosicaoInicial();
			getValidacaoDAO().definirComunicacaoCTI();
			getValidacaoDAO().resgatarPosicaoLocalizada();
			intervencaoBL.interpretarValidacao();

			if( intervencaoBL.isExpressaoEncontrada()){
				getValidacaoDAO().prepararExpressaoBilheteRecepcao();
				getValidacaoDAO().resgatarPosicaoLocalizada();
				intervencaoBL.interpretarValidacao();

				if( intervencaoBL.isExpressaoEncontrada()){
					getValidacaoDAO().definirPosicaoDado();
					getReportagemBL().definirUUIRecepcao();
					intervencaoBL.interpretarValidacao();

					if(intervencaoBL.isuUIEntradaEncontrado()){
						getReportagemBL().complementarRelatorioBilheteEntrada();
					}

				}

			}

		}

		getValidacaoDAO().restaurarPosicaoInicial();
		System.out.println("{fim acompanhamento}");
	}
	
	
	public void produzir( IntervencaoBL intervencaoBL) {
		getValidacaoDAO().retornarInicioArmazem();
		evidenciar( intervencaoBL);
		getValidacaoDAO().escreverDossie();
		relatar( intervencaoBL);
		getReportagemBL().escreverRelatorio();
		getValidacaoDAO().complementarProduto();
	}

	public void coletarConvencionais( IntervencaoBL intervencaoBL) {
		getValidacaoDAO().definirStoring();
		intervencaoBL.interpretarValidacao();

		if( intervencaoBL.isEvidenciaEncontrada()){
			getValidacaoDAO().completarDossie();
		}

		getValidacaoDAO().definirTransferido();
		intervencaoBL.interpretarValidacao();

		if( intervencaoBL.isEvidenciaEncontrada()){
			getValidacaoDAO().completarDossie();
		}

		getValidacaoDAO().definirExcecao();
		intervencaoBL.interpretarValidacao();

		if( intervencaoBL.isEvidenciaEncontrada()){
			getValidacaoDAO().completarDossie();
		}

		getValidacaoDAO().definirComunicacaoCTI();
		intervencaoBL.interpretarValidacao();

		if( intervencaoBL.isEvidenciaEncontrada()){
			getValidacaoDAO().completarDossie();
		}

	}

	public void coletarExcepcionais( IntervencaoBL intervencaoBL) {
		getValidacaoDAO().definirCausaExcecao();
		intervencaoBL.interpretarValidacao();

		if( intervencaoBL.isEvidenciaEncontrada()){
			getValidacaoDAO().completarDossie();
		}

		getValidacaoDAO().definirLocalExcecao();
		intervencaoBL.interpretarValidacao();

		if( intervencaoBL.isEvidenciaEncontrada()){
			getValidacaoDAO().completarDossie();
		}

	}
	
	public void capturarPosicao( IntervencaoBL intervencaoBL) {
		getValidacaoDAO().resguardarUltimaPosicao();
		getValidacaoDAO().resgatarPosicaoLocalizada();
		intervencaoBL.interpretarValidacao();
	}

	public void capturarDado() {
		getValidacaoDAO().definirPosicaoDado();
		getValidacaoDAO().resgatarDado();
	}
	
}

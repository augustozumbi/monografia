package ao722.cvra.controle.decisao;

import ao722.cvra.controle.interpretacao.IntervencaoBL;
import ao722.cvra.modelo.transicao.ReportagemDAO;

public class ReportagemBL {

	// Atributos, Propriedades
	private ReportagemDAO reportagemDAO;

	// Construtores, Sobrecargas
	public ReportagemBL() {
		setReportagemDAO( new ReportagemDAO());
		// TODO Auto-generated constructor stub
	}

	// Getters and Setters, Encapsulamento
	public ReportagemDAO getReportagemDAO() {
		return reportagemDAO;
	}
	public void setReportagemDAO(ReportagemDAO reportagemDAO) {
		this.reportagemDAO = reportagemDAO;
	}

	// Comportamentos, Metodos, Operacoes
	public void reportarInicioValido(IntervencaoBL intervencaoBL){
		System.out.println("{inicio relatar inicio valido}");
		System.out.println( "1. intervencaoBL.isOpcaoValida(): "+ intervencaoBL.isOpcaoValida());

		if( intervencaoBL.isPrimeiraTentativa()){
			System.out.println( "2. intervencaoBL.isPrimeiraTentativa(): "+ intervencaoBL.isPrimeiraTentativa());
			//getValidacaoDAO().complementarRelatorioEntradaValida();
			getReportagemDAO().iniciarRelatoEntradaValida();
			getReportagemDAO().iniciarTrajetoria();
		}
		else{
			System.out.println( "3. intervencaoBL.isPrimeiraTentativa(): "+ intervencaoBL.isPrimeiraTentativa());

			if( intervencaoBL.isMenuDiferente() || intervencaoBL.isDigitoEmCadeia()){
				System.out.println( "4. intervencaoBL.isPrimeiraTentativa(): "+ intervencaoBL.isPrimeiraTentativa() +", intervencaoBL.isMenuDiferente(): "+ intervencaoBL.isMenuDiferente() +", intervencaoBL.isDigitoEmCadeia(): "+ intervencaoBL.isDigitoEmCadeia());
				//getValidacaoDAO().complementarRelatorio();
				getReportagemDAO().complementarRelato();
				getReportagemDAO().complementarTrajetoria();
			}
			else {
				System.out.println( "5. intervencaoBL.isPrimeiraTentativa(): "+ intervencaoBL.isPrimeiraTentativa() +", intervencaoBL.isMenuDiferente(): "+ intervencaoBL.isMenuDiferente() +", intervencaoBL.isDigitoEmCadeia(): "+ intervencaoBL.isDigitoEmCadeia());
				//getValidacaoDAO().concatenarRelatorio();
				getReportagemDAO().concatenarRelato();
				getReportagemDAO().concatenarTrajetoria();
			}

		}

	}

	public void reportarAvaliacao(IntervencaoBL intervencaoBL){
		System.out.println( "6. intervencaoBL.isExpressaoEncontrada() : "+ intervencaoBL.isExpressaoEncontrada() +", intervencaoBL.isDossieEsgotado(): "+ intervencaoBL.isDossieEsgotado());
		intervencaoBL.interpretarValidacao();

		if( intervencaoBL.isOpcaoValida() || intervencaoBL.isDigitoEncontrado()){
			System.out.println( "7. intervencaoBL.isExpressaoEncontrada() : "+ intervencaoBL.isExpressaoEncontrada() +", intervencaoBL.isDossieEsgotado(): "+ intervencaoBL.isDossieEsgotado() +", intervencaoBL.isOpcaoValida(): "+ intervencaoBL.isOpcaoValida());

			if( intervencaoBL.isPrimeiraTentativa() && !intervencaoBL.isMenuDiferente()){
				getReportagemDAO().complementarRelato();
				//getValidacaoDAO().complementarRelatoNeutro();
				getReportagemDAO().complementarTrajetoria();
			}
			else if( intervencaoBL.isMenuDiferente() || intervencaoBL.isDigitoEmCadeia()){
				System.out.println( "8. complementarRelato: intervencaoBL.isExpressaoEncontrada() : "+ intervencaoBL.isExpressaoEncontrada() +", intervencaoBL.isDossieEsgotado(): "+ intervencaoBL.isDossieEsgotado() +", intervencaoBL.isOpcaoValida(): "+ intervencaoBL.isOpcaoValida() +", intervencaoBL.isMenuDiferente(): "+ intervencaoBL.isMenuDiferente() +", intervencaoBL.isDigitoEmCadeia(): "+ intervencaoBL.isDigitoEmCadeia());
				//getValidacaoDAO().complementarRelatorio();
				getReportagemDAO().complementarRelato();
				getReportagemDAO().complementarTrajetoria();
			}
			else {
				System.out.println( "9. concatenarTrajetoria: intervencaoBL.isExpressaoEncontrada() : "+ intervencaoBL.isExpressaoEncontrada() +", intervencaoBL.isDossieEsgotado(): "+ intervencaoBL.isDossieEsgotado() +", intervencaoBL.isOpcaoValida(): "+ intervencaoBL.isOpcaoValida() +", intervencaoBL.isMenuDiferente(): "+ intervencaoBL.isMenuDiferente() +", intervencaoBL.isDigitoEmCadeia(): "+ intervencaoBL.isDigitoEmCadeia());
				//getValidacaoDAO().concatenarRelatorio();
				//getValidacaoDAO().concatenarRelato();
				getReportagemDAO().concatenarTrajetoria();
			}

		}
		else {
			System.out.println( "10. intervencaoBL.isExpressaoEncontrada() : "+ intervencaoBL.isExpressaoEncontrada() +", intervencaoBL.isDossieEsgotado(): "+ intervencaoBL.isDossieEsgotado());
			intervencaoBL.interpretarValidacao();

			if( intervencaoBL.isRepeticao()){
				System.out.println( "11. intervencaoBL.isExpressaoEncontrada() : "+ intervencaoBL.isExpressaoEncontrada() +", intervencaoBL.isDossieEsgotado(): "+ intervencaoBL.isDossieEsgotado() +", intervencaoBL.isRepeticao(): "+ intervencaoBL.isRepeticao() +", intervencaoBL.isOpcaoValida(): "+ intervencaoBL.isOpcaoValida());
				//getValidacaoDAO().complementarRelatorio();
				getReportagemDAO().complementarRelato();

				if( intervencaoBL.isOpcaoFornecida()){
					getReportagemDAO().complementarTrajetoriaEquivoco();
				}
				else {
					getReportagemDAO().complementarTrajetoriaOmissao();
				}

			}
			else {
				System.out.println( "12. intervencaoBL.isExpressaoEncontrada() : "+ intervencaoBL.isExpressaoEncontrada() +", intervencaoBL.isDossieEsgotado(): "+ intervencaoBL.isDossieEsgotado() +", intervencaoBL.isRepeticao(): "+ intervencaoBL.isRepeticao() +", intervencaoBL.isOpcaoValida(): "+ intervencaoBL.isOpcaoValida());

//				if( intervencaoBL.isMenuDiferente() || intervencaoBL.isDigitoEmCadeia()){
//					System.out.println( "intervencaoBL.isExpressaoEncontrada() : "+ intervencaoBL.isExpressaoEncontrada() +", intervencaoBL.isDossieEsgotado(): "+ intervencaoBL.isDossieEsgotado() +", intervencaoBL.isRepeticao(): "+ intervencaoBL.isRepeticao() +", intervencaoBL.isOpcaoValida(): "+ intervencaoBL.isOpcaoValida() +", intervencaoBL.isMenuDiferente(): "+ intervencaoBL.isMenuDiferente() +", intervencaoBL.isDigitoEmCadeia(): "+ intervencaoBL.isDigitoEmCadeia());
//					//getValidacaoDAO().complementarRelatorioExcedeu();
//					getValidacaoDAO().complementarRelatoExcedeu();
//				}
//				else {
					System.out.println( "13. intervencaoBL.isExpressaoEncontrada() : "+ intervencaoBL.isExpressaoEncontrada() +", intervencaoBL.isDossieEsgotado(): "+ intervencaoBL.isDossieEsgotado() +", intervencaoBL.isRepeticao(): "+ intervencaoBL.isRepeticao() +", intervencaoBL.isOpcaoValida(): "+ intervencaoBL.isOpcaoValida() +", intervencaoBL.isMenuDiferente(): "+ intervencaoBL.isMenuDiferente() +", intervencaoBL.isDigitoEmCadeia(): "+ intervencaoBL.isDigitoEmCadeia());
					//getValidacaoDAO().complementarRelatorioEquivoco();
					getReportagemDAO().complementarRelatoEquivoco();
					getReportagemDAO().complementarTrajetoriaEquivoco();
//				}

			}

		}

	}

	public void reportarFimValido(IntervencaoBL intervencaoBL){

		if( intervencaoBL.isExpressaoEncontrada()){
			System.out.println("Level 1");
			//getValidacaoDAO().complementarRelatorioTransferido();
			getReportagemDAO().complementarRelatoTransferido();
			getReportagemDAO().relatarDesfechoTransferencia();
			//getReportagemDAO().complementarRelatorioVDNTransferencia();
			getReportagemDAO().relatarVDNTransferencia();
			intervencaoBL.interpretarValidacao();
			
			if( intervencaoBL.isuUISaidaEncontrado()){
				//getReportagemDAO().complementarRelatorioBilheteSaida();
				getReportagemDAO().relatarBilheteSaida();
			}

		}
		else {
			System.out.println("Level 2");

			if( !intervencaoBL.isExpressaoEncontrada() && !intervencaoBL.isAvancoOcorrido()){
				System.out.println("Level 3");

				if( intervencaoBL.isMenuDiferente() || intervencaoBL.isPrimeiraTentativa()){
					System.out.println("Level 4");
					//getValidacaoDAO().complementarRelatorioAbandonou();
					getReportagemDAO().complementarRelatoAbandonou();
					getReportagemDAO().complementarTrajetoriaAbandono();
					getReportagemDAO().relatarDesfechoAbandono();
				}
				else {
					getReportagemDAO().relatarDesfechoExcesso();
				}

			}
			else {
				System.out.println("Level 6");

				if( intervencaoBL.isPrimeiraTentativa()){
					System.out.println("Level 7");
					//getValidacaoDAO().complementarRelatorio();
					getReportagemDAO().complementarRelato();
					getReportagemDAO().complementarTrajetoria();
					getReportagemDAO().relatarDesfechoDesconexao();
				}

			}

			//getValidacaoDAO().complementarRelatorioRetido();
			getReportagemDAO().complementarRelatoRetido();
		}

		System.out.println("{fim relatar inicio valido}");
	}

	public void relatarInicioQuebrado( IntervencaoBL intervencaoBL){
		System.out.println("{inicio relatar quebra entrada}");
		getReportagemDAO().relatarExcecao();
		getReportagemDAO().complementarRelatorioExcecao();
		getReportagemDAO().iniciarRelatoEntradaQuebra();
		getReportagemDAO().complementarTrajetoriaQuebra();
		getReportagemDAO().relatarDesfechoQuebra();
		System.out.println("{fim relatar quebra entrada}");
	}

	public void relatarInicioInvalido( IntervencaoBL intervencaoBL){
		System.out.println("{relatar inicio opcao invalida}");
		System.out.println("ValidacaoBL.relatar(): nivel 4");

		if( intervencaoBL.isPrimeiraTentativa()){
			System.out.println("ValidacaoBL.relatar(): nivel 5");

			if( !intervencaoBL.isExpressaoEncontrada() && !intervencaoBL.isAvancoOcorrido()){
				System.out.println("ValidacaoBL.relatar(): nivel 6");
				//getValidacaoDAO().complementarRelatorioEntradaAbandono();
				getReportagemDAO().iniciarRelatoEntradaAbandono();
				getReportagemDAO().iniciarTrajetoriaAbandono();
				getReportagemDAO().relatarDesfechoAbandono();
			}
			else {
				System.out.println( "intervencaoBL.isPosicaoMenuDiferente(): "+ intervencaoBL.isPosicaoMenuDiferente());

				if( intervencaoBL.isMenuDiferente() || intervencaoBL.isDigitoEmCadeia()){
					System.out.println("ValidacaoBL.relatar(): nivel 7");
					//getValidacaoDAO().complementarRelatorioExcedeu();
					getReportagemDAO().complementarRelatoExcedeu();
					getReportagemDAO().complementarTrajetoria();
				}
				else {
					System.out.println("ValidacaoBL.relatar(): nivel 8");
//					getValidacaoDAO().complementarRelatorioEntradaValida();
//					getValidacaoDAO().iniciarTrajetoriaEquivoco();
//					getValidacaoDAO().complementarRelatorioEquivoco();
					getReportagemDAO().iniciarRelatoEntradaInvalida();
					getReportagemDAO().iniciarTrajetoriaEquivoco();
				}

			}

		}
		else {
			System.out.println("ValidacaoBL.relatar(): nivel 12");
			intervencaoBL.interpretarValidacao();

			if( !intervencaoBL.isExpressaoEncontrada() && !intervencaoBL.isAvancoOcorrido() && intervencaoBL.isOpcaoValida()){
				System.out.println("ValidacaoBL.relatar(): nivel 13");
				//getValidacaoDAO().complementarRelatorio();
				getReportagemDAO().complementarRelato();
				getReportagemDAO().complementarTrajetoriaAbandono();
			}
			else {
				System.out.println( "intervencaoBL.isPosicaoMenuDiferente(): "+ intervencaoBL.isPosicaoMenuDiferente());

				if( intervencaoBL.isMenuDiferente() || intervencaoBL.isDigitoEmCadeia()){
					System.out.println("ValidacaoBL.relatar(): nivel 14");
					//getValidacaoDAO().complementarRelatorioExcedeu();
					getReportagemDAO().complementarRelatoExcedeu();
					getReportagemDAO().complementarTrajetoria();
				}
				else {
					System.out.println("ValidacaoBL.relatar(): nivel 15");
					//getValidacaoDAO().complementarRelatorioEquivoco();
					getReportagemDAO().complementarRelatoEquivoco();
					getReportagemDAO().complementarTrajetoriaEquivoco();
				}

			}

		}

		System.out.println("{fim relatar opcao invalida}");
	}

	public void escreverRelatorio(){
		getReportagemDAO().escreverRelatorio();
	}
	
	public void zerarRelatorio(){
		getReportagemDAO().zerarRelatorio();
		getReportagemDAO().zerarRelato();
		getReportagemDAO().zerarDelato();
		getReportagemDAO().zerarPlataforma();
		getReportagemDAO().zerarResumo();
		getReportagemDAO().zerarDerivacao();
	}

	public void reportar(){
		getReportagemDAO().relatarDataTeste();
		getReportagemDAO().relatarTerminoTeste();
		getReportagemDAO().relatarDuracao();
		getReportagemDAO().relatarRamalAlocado();
		getReportagemDAO().relatarSessao();
		getReportagemDAO().relatarMPP();
		getReportagemDAO().relatarChamador();
		getReportagemDAO().relatarNumeroChamado();
		getReportagemDAO().relatarResumo();
		getReportagemDAO().relatarPlataforma();
		getReportagemDAO().iniciarRelatoDerivacao();
	}
	
	public void terminar( IntervencaoBL intervencaoBL){
		getReportagemDAO().iniciarRelatorio();
		//getReportagemDAO().complementarRelatorioTrajetoria();
		//getReportagemDAO().complementarRelatorioDesfecho();
		getReportagemDAO().complementarRelatorioDerivacao();
		getReportagemDAO().complementarRelatorioRelato();

		if( intervencaoBL.isExcecaoReincidente()){
			getReportagemDAO().complementarRelatorioDelacao();
		}

		getReportagemDAO().completarRelatorio();
		getReportagemDAO().zerarTrajetoria();
	}

	public void definirCanal(){
		getReportagemDAO().definirCanal();
	}
	
	public void definirMPP(){
		getReportagemDAO().definirMPP();
	}

	public void definirANI(){
		getReportagemDAO().definirANI();
	}
	
	public void definirDNIS(){
		getReportagemDAO().definirDNIS();
	}
	
	public void iniciarDelatoExcecao(){
		getReportagemDAO().resgatarExcecao();
		getReportagemDAO().iniciarDelatoExcecao();
	}
	
	public void iniciarDelatoMensagemExcecao(){
		getReportagemDAO().resgatarMensagemExcecao();
		getReportagemDAO().iniciarDelatoMensagemExcecao();
	}
	
	public void complementarDelatoLocalExcecao(){
		getReportagemDAO().resgatarLocalExcecao();
		getReportagemDAO().complementarDelatoLocalExcecao();
	}

	public void complementarDelatoLinhaExcecao(){
		getReportagemDAO().resgatarLinhaExcecao();
		getReportagemDAO().complementarDelatoLinhaExcecao();
	}
	
	public void finalizarDelacao(){
		getReportagemDAO().finalizarDelacao();
	}

	public void complementarDelatoMensagemExcecao(){
		getReportagemDAO().resgatarMensagemExcecao();
		getReportagemDAO().complementarDelatoMensagemExcecao();
	}
	
	public void resgatarMensagemExcecao(){
		getReportagemDAO().resgatarMensagemExcecao();
	}

	public void resgatarVDNTransferencia(){
		getReportagemDAO().resgatarVDNTransferencia();
	}

	public void definirUUIEnvio( IntervencaoBL intervencaoBL){
		getReportagemDAO().resgatarUUISaida();
		getReportagemDAO().definirUUIEnvio();

	}

	public void definirUUIRecepcao(){
		getReportagemDAO().resgatarUUIEntrada();
		getReportagemDAO().definirUUIRecepcao();
		getReportagemDAO().relatarBilheteEntrada();
	}

	public void complementarRelatorioBilheteEntrada(){
		getReportagemDAO().complementarRelatorioBilheteEntrada();
	}

	public void iniciarMapas(){
		getReportagemDAO().iniciarMapas();
	}
	
}

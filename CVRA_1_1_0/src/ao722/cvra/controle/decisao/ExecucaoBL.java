package ao722.cvra.controle.decisao;

import ao722.cvra.controle.interpretacao.IntervencaoBL;
import ao722.cvra.modelo.estado.ExecucaoVO;
import ao722.cvra.modelo.transicao.ExecucaoDAO;
import ao722.cvra.visao.saida.VisorUI;

public class ExecucaoBL {

	// Propriedades
	private boolean preparada, concluido, ocorrendo, parada;
	private ExecucaoDAO execucaoDAO;
	private IntermediacaoBL intermediadorBL;

	// Construtores, Sobrecargas
	public ExecucaoBL( IntervencaoBL intervencaoBL, VisorUI visor) {
		setIntermediadorBL( new IntermediacaoBL( intervencaoBL));
		executar( intervencaoBL, visor);
	}

	// Getters and setters
	public boolean isPreparada() {
		return preparada;
	}
	public void setPreparada(boolean preparada) {
		this.preparada = preparada;
	}

	public boolean isConcluido() {
		return concluido;
	}
	public void setConcluido(boolean concluido) {
		this.concluido = concluido;
	}

	public boolean isOcorrendo() {
		return ocorrendo;
	}
	public void setOcorrendo(boolean ocorrendo) {
		this.ocorrendo = ocorrendo;
	}

	public boolean isParada() {
		return parada;
	}
	public void setParada(boolean parada) {
		this.parada = parada;
	}

//	public boolean isDocumento() {
//		return documento;
//	}
//	public void setDocumento(boolean documento) {
//		this.documento = documento;
//	}

	public ExecucaoDAO getExecucaoDAO() {
		return execucaoDAO;
	}
	public void setExecucaoDAO(ExecucaoDAO execucaoDAO) {
		this.execucaoDAO = execucaoDAO;
	}

	public IntermediacaoBL getIntermediadorBL() {
		return intermediadorBL;
	}
	public void setIntermediadorBL(IntermediacaoBL intermediadorBL) {
		this.intermediadorBL = intermediadorBL;
	}

	public void executar( IntervencaoBL intervencaoBL, VisorUI visor){

		if( intervencaoBL.isReportagem()){
			System.out.println("Apenas mais um reporte");
		}
		else {

			if( intervencaoBL.isOcioso()){
				System.out.println("Deu que estava ocioso");

				if( intervencaoBL.isExecutar() || intervencaoBL.isExecucao()){
					System.out.println("Comecar teste");
					comecarTeste( intervencaoBL, visor);
				}
				else if( intervencaoBL.isRetomar()){
					System.out.println( "Retomar teste");

					if( intervencaoBL.isSuspenso()){
						System.out.println("Deu que estava suspenso");
						comecarTeste( intervencaoBL, visor);
					}

				}

			}
			else if( intervencaoBL.isAtivo()){
				System.out.println("Deu que estava ativo");

				if( intervencaoBL.isAbortar()){
					setExecucaoDAO( new ExecucaoDAO());
					interromperTeste( intervencaoBL, visor);
				}
				else if( intervencaoBL.isAguardar()){
					suspenderTeste( intervencaoBL);
				}

			}
			else {
				System.out.println("Nem um, nem outro.");
				setExecucaoDAO( new ExecucaoDAO());
				getExecucaoDAO().obterSumario();
				validarSumario();
			}

		}

	}

	public void comecarTeste( IntervencaoBL intervencaoBL, VisorUI visor){

		if( !intervencaoBL.isExecutar() && !intervencaoBL.isExecucao() && !intervencaoBL.isRetomar()){ }
		else {
			getIntermediadorBL().prover( intervencaoBL);
		}

		setExecucaoDAO( new ExecucaoDAO());
		getExecucaoDAO().obterSumario();
		validarSumario();

		if( isPreparada()){
			getExecucaoDAO().sinalizarAtivo();
			getExecucaoDAO().definirCarga();
			getExecucaoDAO().iniciarRegistros();
			
			testarSuite( intervencaoBL, visor);
			
			if( !intervencaoBL.isSuspenso()){
				getIntermediadorBL().concluirProcesso();
				getExecucaoDAO().gravarHistorico();
				desalocarExecucao();
			}
			
		}

	}
	
	public void interromperTeste( IntervencaoBL intervencaoBL, VisorUI visor){
		desalocarExecucao();
		getIntermediadorBL().abortarProcesso( intervencaoBL);
		getExecucaoDAO().sinalizarInterrompido();
		visor.atualizarInterrupcao();
	}

	public void suspenderTeste( IntervencaoBL intervencaoBL){
		setExecucaoDAO( new ExecucaoDAO());
		getExecucaoDAO().sinalizarSuspenso();
	}

	public void executarProcedimento( IntervencaoBL intervencaoBL){
		engatilharProcedimento( intervencaoBL);

		if( intervencaoBL.isDesconexao()){
			procederAbandono( intervencaoBL);
		}
		else if( intervencaoBL.isDigito()){
			getExecucaoDAO().definirDigito();
			procederDigitacao(intervencaoBL);
		}
		else if( intervencaoBL.isCadeia()){
			getExecucaoDAO().obterMassa();
			getExecucaoDAO().obterCadeia();
			getExecucaoDAO().saltarEncadeado();
			getExecucaoDAO().definirEncadeados();
			getExecucaoDAO().desencadearDigito();
			procederDigitacao(intervencaoBL);
		}

		intervencaoBL.interpretarExecucao();

		while( !intervencaoBL.isProcedimentoConcluido() && !intervencaoBL.isAbortar()){
			intervencaoBL.interpretarExecucao();

			if( intervencaoBL.isProcedimentoConcluido()){ getExecucaoDAO().zerarIndicador(); break;}

			getExecucaoDAO().definirAguardo();
			getExecucaoDAO().desencadearDigito();
			procederDigitacao(intervencaoBL);
		}

		getExecucaoDAO().zerarIndicador();
	}

	public void prepararProcedimento( IntervencaoBL intervencaoBL){	}

	public void validarSumario(){
		setPreparada( ExecucaoVO.casos != null);
	}

	public void identificarEstagio(){
		setConcluido( ExecucaoVO.teste >= ExecucaoVO.carga);
		setOcorrendo( ExecucaoVO.teste > 0);
		setParada( ExecucaoVO.teste == 0);
	}

	public void desalocarExecucao(){
		getExecucaoDAO().zerarTeste();
		getExecucaoDAO().zerarEtapa();
		getExecucaoDAO().zerarAguardo();
		getExecucaoDAO().zerarIndicador();
		getExecucaoDAO().zerarCadeia();
		getExecucaoDAO().zerarComplexidade();
		getExecucaoDAO().zerarCronograma();
		getExecucaoDAO().zerarDigito();
		getExecucaoDAO().zerarEncadeados();
		getExecucaoDAO().zerarHistorico(); /* ATENCAO */
		getExecucaoDAO().zerarLinha();
		getExecucaoDAO().zerarMassa();
		getExecucaoDAO().zerarPassos();
		getExecucaoDAO().zerarProcedimento();
		getExecucaoDAO().zerarProcesso();
		getExecucaoDAO().zerarRoteiro();
		getExecucaoDAO().zerarSumario();
		getExecucaoDAO().zerarUltimoAguardo();
		//getExecucaoDAO().zerarCarga();
		//getExecucaoDAO().zerarPulos();
	}
	
	public void testarSuite( IntervencaoBL intervencaoBL, VisorUI visor){

		while( !isConcluido() && !intervencaoBL.isAbortar() && !intervencaoBL.isSuspenso()){
			getExecucaoDAO().definirLinha();
			intervencaoBL.interpretarExecucao();

			if( intervencaoBL.isLinhaConstituicao()){
				evitarConstituicao();
				continue;
			}

			prepararTeste( intervencaoBL, visor);
			executarProcedimento( intervencaoBL);
			intervencaoBL.interpretarExecucao();

			while( !intervencaoBL.isTesteConcluido() && !intervencaoBL.isAbortar()){
				getExecucaoDAO().saltarEtapa();
				intervencaoBL.interpretarExecucao();

				if( intervencaoBL.isEtapasEsgotadas()){	break;}

				executarProcedimento( intervencaoBL);
				intervencaoBL.interpretarExecucao();

				if( intervencaoBL.isTesteConcluido()){ break;}

			}

			getExecucaoDAO().saltarLinha();
			getExecucaoDAO().zerarEtapa();
			getExecucaoDAO().zerarAguardo();
			identificarEstagio();
			getIntermediadorBL().concluirProcedimento( intervencaoBL);
			getExecucaoDAO().registrarHorarioFim();
			//intervencaoBL.interpretarExecucao();
		}

	}

	public void evitarConstituicao(){
		getExecucaoDAO().saltarLinha();
		getExecucaoDAO().contarPulo();
		identificarEstagio();
	}

	public void prepararTeste( IntervencaoBL intervencaoBL, VisorUI visor){
		visor.atualizarExecucao();
		getExecucaoDAO().registrarTeste();
		getExecucaoDAO().definirRoteiro();
		getIntermediadorBL().fazerChamada( intervencaoBL);
		getIntermediadorBL().pegarReceptor( intervencaoBL);
		getExecucaoDAO().registrarHorarioInicio();
		getExecucaoDAO().definirCronograma();
		getExecucaoDAO().definirProcesso();
		getExecucaoDAO().definirPassos();
	}
	
	public void engatilharProcedimento( IntervencaoBL intervencaoBL){
		getExecucaoDAO().definirAguardo();
		getExecucaoDAO().definirUltimoAguardo();
		intervencaoBL.interpretarExecucao();

		if( intervencaoBL.isCadeia()){
			getExecucaoDAO().definirAguardoCompensado();
		}

		getExecucaoDAO().definirProcedimento();
		getExecucaoDAO().definirComplexidade();

		intervencaoBL.interpretarExecucao();		
	}
	
	public void procederAbandono( IntervencaoBL intervencaoBL){
		getExecucaoDAO().aguardarMomento();
		getIntermediadorBL().realizarDesconexao( intervencaoBL);
	}

	public void procederDigitacao( IntervencaoBL intervencaoBL){
		getExecucaoDAO().aguardarMomento();
		getIntermediadorBL().digitar( intervencaoBL);
		getExecucaoDAO().saltarIndicador();
	}

}

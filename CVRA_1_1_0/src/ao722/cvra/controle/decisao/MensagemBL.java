package ao722.cvra.controle.decisao;

import ao722.cvra.controle.interpretacao.InteracaoBL;
import ao722.cvra.modelo.transicao.MensagemDAO;

public class MensagemBL {

	// Propriedades
	MensagemDAO mensagemDAO;

	// Getters and setters
	public MensagemDAO getMensagemDAO() {
		return mensagemDAO;
	}
	public void setMensagemDAO( MensagemDAO mensagemDAO) {
		this.mensagemDAO = mensagemDAO;
	}

	public MensagemBL() {
		// TODO Auto-generated constructor stub
	}
	public MensagemBL( InteracaoBL interacaoBL){
		setMensagemDAO( new MensagemDAO());
		definirMensagem( interacaoBL);
	}

	// Metodos
	public void definirMensagem( InteracaoBL interacaoBL){

		if( interacaoBL.isPossivel()){

			if( interacaoBL.isIniciar() || interacaoBL.isChamar()){
				getMensagemDAO().obterReacao();
				getMensagemDAO().definirEntrada();
			}
			else {

				if( interacaoBL.isValida()){
					entregarMensagemValida(interacaoBL);
				}
				else {
					entregarMensagemInvalida(interacaoBL);
				}

			}

			getMensagemDAO().obterFrase();
			interacaoBL.identificarMensagem();
		}
		
	}
	
	public void entregarMensagemValida( InteracaoBL interacaoBL){
		
		getMensagemDAO().definirEco();
		
		if( interacaoBL.isNavegar()) {
			getMensagemDAO().definirNavegacao();
		}
		else if( interacaoBL.isRepeticao()){
			getMensagemDAO().definirRepeticao();
		}
		else if( interacaoBL.isRetorno()){
			getMensagemDAO().definirRetorno();
		}
		else if( interacaoBL.isTransferencia()){
			getMensagemDAO().definirTransferencia();
		}
		else if( interacaoBL.isDesconexao()){
			getMensagemDAO().definirDesconexao();
		}

	}
	
	public void entregarMensagemInvalida( InteracaoBL interacaoBL){

		if( interacaoBL.isInsucesso()){
			getMensagemDAO().definirInsucesso();
		}
		else {
			getMensagemDAO().definirDesacerto();
			getMensagemDAO().obterReacao();
		}

	}

}

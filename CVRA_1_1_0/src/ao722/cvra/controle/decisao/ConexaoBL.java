package ao722.cvra.controle.decisao;

import ao722.cvra.controle.interpretacao.IntervencaoBL;
import ao722.cvra.modelo.transicao.ConexaoDAO;

public class ConexaoBL {

	// Atributos
	private ConexaoDAO conexaoDAO;

	// Construtores
	public ConexaoBL() {}
	// Comportamentos
		public ConexaoBL( IntervencaoBL intervencaoBL) {
			setConexaoDAO( new ConexaoDAO());
	//		baixarLog();
		}
	// Encapsulamento
	public ConexaoDAO getConexaoDAO() {
		return conexaoDAO;
	}
	public void setConexaoDAO(ConexaoDAO conexaoDAO) {
		this.conexaoDAO = conexaoDAO;
	}

	

//	public void baixarLog(){
//		getConexaoDAO().obterEvidencias();
//	}

	public void obterLog( IntervencaoBL intervencaoBL){
		getConexaoDAO().criarLink();
		getConexaoDAO().acessarURL();
		getConexaoDAO().criarCanal();
		intervencaoBL.interpretarConexao();

		if( intervencaoBL.isConexaoValida()){
			getConexaoDAO().receptarBytes();
		}

//		getConexaoDAO().imprimirLog();
	}

	public void descartarLog(){
		getConexaoDAO().encerrarAcesso();
		getConexaoDAO().fecharCanal();
	}

}

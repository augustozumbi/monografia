package ao722.cvra.controle.decisao;

import ao722.cvra.controle.interpretacao.InteracaoBL;
import ao722.cvra.modelo.transicao.NavegacaoDAO;

public class NavegacaoBL {

	// Propriedades
	private LocalizacaoBL localizacaoBL;
	private NavegacaoDAO navegacaoDAO;

	// Getters and Setters
	public NavegacaoDAO getNavegacaoDAO() {
		return navegacaoDAO;
	}
	public void setNavegacaoDAO(NavegacaoDAO navegacaoDAO) {
		this.navegacaoDAO = navegacaoDAO;
	}

	public LocalizacaoBL getLocalizacaoBL() {
		return localizacaoBL;
	}
	public void setLocalizacaoBL(LocalizacaoBL localizacaoBL) {
		this.localizacaoBL = localizacaoBL;
	}

	// Construtores
	public NavegacaoBL( InteracaoBL interacaoBL) {
		setNavegacaoDAO( new NavegacaoDAO());
		decidirNavegacao( interacaoBL);
		setLocalizacaoBL( new LocalizacaoBL( interacaoBL));
	}

	// Metodos
	public void decidirNavegacao( InteracaoBL interacaoBL){

		// Possivel
		if( interacaoBL.isPossivel()){

			// Saudacao
			if( interacaoBL.isIniciar() || interacaoBL.isChamar()){
				getNavegacaoDAO().iniciar();
			}
			// Navegar
			else if( interacaoBL.isNavegar()){
				proverNavegacao( interacaoBL);
			}

		}

	}
	
	public void proverNavegacao( InteracaoBL interacaoBL){

		if( !interacaoBL.isEntrada()){	
			getNavegacaoDAO().definirNavegacao();
		}

		getNavegacaoDAO().obterNavegacao();
		interacaoBL.identificarNavegacao();

		if( interacaoBL.isDesacerto()){
			navegarDesacerto(interacaoBL);
		}
		else {
			navegarNormal( interacaoBL);
		}
		
	}
	
	public void navegarDesacerto( InteracaoBL interacaoBL) {

		getNavegacaoDAO().incrementarTentativas();
		interacaoBL.identificarInsucesso();

		if( interacaoBL.isInsucesso()){
			getNavegacaoDAO().definirInsucesso();
		}
		else {
			getNavegacaoDAO().definirDesacerto();
		}
		
	}

	public void navegarNormal( InteracaoBL interacaoBL){

		getNavegacaoDAO().reiniciarTentativas();

		if( interacaoBL.isNavegacao()){
			getNavegacaoDAO().definirNavegacao();
		}
		else if( interacaoBL.isRepeticao()){
			getNavegacaoDAO().definirRepeticao();
		}
		else if( interacaoBL.isRetorno()){
			getNavegacaoDAO().definirRetorno();
		}
		else if( interacaoBL.isDesfecho()){
		}

	}
}

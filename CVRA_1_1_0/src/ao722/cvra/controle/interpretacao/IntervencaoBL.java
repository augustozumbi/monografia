package ao722.cvra.controle.interpretacao;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.telephony.Connection;

import ao722.cvra.controle.decisao.ExecucaoBL;
import ao722.cvra.controle.decisao.ExportacaoBL;
import ao722.cvra.controle.decisao.ValidacaoBL;
import ao722.cvra.modelo.estado.ConexaoVO;
import ao722.cvra.modelo.estado.ConfiguracaoVO;
import ao722.cvra.modelo.estado.ConstituicaoVO;
import ao722.cvra.modelo.estado.ControladorVO;
import ao722.cvra.modelo.estado.ControleMaterialVO;
import ao722.cvra.modelo.estado.ExecucaoVO;
import ao722.cvra.modelo.estado.ExportacaoVO;
import ao722.cvra.modelo.estado.IntegracaoTelefonicaVO;
import ao722.cvra.modelo.estado.IntermediacaoVO;
import ao722.cvra.modelo.estado.IntervencaoVO;
import ao722.cvra.modelo.estado.ReportagemVO;
import ao722.cvra.modelo.estado.ValidacaoVO;
import ao722.cvra.visao.entrada.IntervencaoUI;

public class IntervencaoBL {

	// Propriedades
//	private boolean possivel, relatorioEscrito, dnisEncontrado;
	private boolean executar, aguardar, retomar, abortar, ocioso, ativo, execucao, limpeza, reportagem, exportacao, suspenso, interrompido; // INTERVENCAO 
	private boolean servicoEncontrado, portfolioEsgotado, aparelhoEncontrado, aparelhagemEsgotada, origemEncontrada, ligacaoEsgotada, alertasEsgotados, destinoEncontrado, segurancaDesligada, ligacaoValida, ligacaoAtendida, vinculacaoValida, dTMFValido, destinoExterno; // INTERMEDIACAO
	private boolean testeConcluido, procedimentoConcluido, etapasEsgotadas, linhaConstituicao, digito, cadeia, desconexao; // EXECUCAO
	private boolean conexaoValida; // CONEXAO
	private boolean dataInicialEncontrada, logEsgotado, aNIEncontrado, idSessaoEncontrada, evidenciaEncontrada, opcaoValida, digitoEncontrado, expressaoEncontrada, dossieEsgotado, primeiraTentativa, menuDiferente, linhaLog, apuracaoEsgotada, opcaoFornecida, avancoOcorrido, dataFinalEncontrada, repeticao, excecaoEncontrada, excecaoReincidente, linhaDeANI, posicaoMenuDiferente, digitoEmCadeia, uUIEntradaEncontrado, uUISaidaEncontrado; // VALIDACAO
	private boolean arquivoEscolhido, exportacaoHiperTexto, relatorioArmazenado, limpezaConfirmada;
	private ExecucaoBL execucaoBL;
	private ValidacaoBL validacaoBL;
	private ExportacaoBL exportacaoBL;

	// Construtores
	public IntervencaoBL( IntervencaoUI intervencaoUI) {
		identificarIntervencao();
		setExecucaoBL( new ExecucaoBL( this, intervencaoUI.getVisor()));
		setValidacaoBL( new ValidacaoBL( this));
		setExportacaoBL( new ExportacaoBL( this));
	}

	// Getters and setters
//	public boolean isPossivel() {
//		return possivel;
//	}
//	public void setPossivel( boolean possivel) {
//		this.possivel = possivel;
//	}

	public boolean isExecutar() {
		return executar;
	}
	public void setExecutar(boolean executar) {
		this.executar = executar;
	}

	public boolean isAguardar() {
		return aguardar;
	}
	public void setAguardar(boolean aguardar) {
		this.aguardar = aguardar;
	}

	public boolean isAbortar() {
		return abortar;
	}
	public void setAbortar(boolean abortar) {
		this.abortar = abortar;
	}

	public boolean isOcioso() {
		return ocioso;
	}
	public void setOcioso( boolean ocioso) {
		this.ocioso = ocioso;
	}

	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo( boolean ativo) {
		this.ativo = ativo;
	}

	public boolean isOrigemEncontrada() {
		return origemEncontrada;
	}
	public void setOrigemEncontrada(boolean origemEncontrada) {
		this.origemEncontrada = origemEncontrada;
	}

	public boolean isServicoEncontrado() {
		return servicoEncontrado;
	}
	public void setServicoEncontrado( boolean servicoEncontrado) {
		this.servicoEncontrado = servicoEncontrado;
	}

	public boolean isPortfolioEsgotado() {
		return portfolioEsgotado;
	}
	public void setPortfolioEsgotado(boolean portfolioEsgotado) {
		this.portfolioEsgotado = portfolioEsgotado;
	}

	public boolean isAparelhoEncontrado() {
		return aparelhoEncontrado;
	}
	public void setAparelhoEncontrado(boolean aparelhoEncontrado) {
		this.aparelhoEncontrado = aparelhoEncontrado;
	}

	public boolean isAparelhagemEsgotada() {
		return aparelhagemEsgotada;
	}
	public void setAparelhagemEsgotada(boolean aparelhagemEsgotada) {
		this.aparelhagemEsgotada = aparelhagemEsgotada;
	}

	public boolean isLigacaoEsgotada() {
		return ligacaoEsgotada;
	}
	public void setLigacaoEsgotada(boolean ligacaoEsgotada) {
		this.ligacaoEsgotada = ligacaoEsgotada;
	}

	public boolean isLigacaoValida() {
		return ligacaoValida;
	}
	public void setLigacaoValida(boolean ligacaoValida) {
		this.ligacaoValida = ligacaoValida;
	}

	public boolean isLigacaoAtendida() {
		return ligacaoAtendida;
	}
	public void setLigacaoAtendida(boolean ligacaoAtendida) {
		this.ligacaoAtendida = ligacaoAtendida;
	}

	public boolean isAlertasEsgotados() {
		return alertasEsgotados;
	}
	public void setAlertasEsgotados(boolean alertasEsgotados) {
		this.alertasEsgotados = alertasEsgotados;
	}

	public boolean isVinculacaoValida() {
		return vinculacaoValida;
	}
	public void setVinculacaoValida(boolean vinculacaoValida) {
		this.vinculacaoValida = vinculacaoValida;
	}

	public boolean isTesteConcluido() {
		return testeConcluido;
	}
	public void setTesteConcluido(boolean testeConcluido) {
		this.testeConcluido = testeConcluido;
	}

	public boolean isDigito() {
		return digito;
	}
	public void setDigito(boolean digito) {
		this.digito = digito;
	}

	public boolean isCadeia() {
		return cadeia;
	}
	public void setCadeia(boolean cadeia) {
		this.cadeia = cadeia;
	}

	public boolean isProcedimentoConcluido() {
		return procedimentoConcluido;
	}
	public void setProcedimentoConcluido(boolean procedimentoConcluido) {
		this.procedimentoConcluido = procedimentoConcluido;
	}

	public boolean isdTMFValido() {
		return dTMFValido;
	}
	public void setdTMFValido(boolean dTMFValido) {
		this.dTMFValido = dTMFValido;
	}

	public boolean isDestinoEncontrado() {
		return destinoEncontrado;
	}
	public void setDestinoEncontrado(boolean destinoEncontrado) {
		this.destinoEncontrado = destinoEncontrado;
	}

	public boolean isEtapasEsgotadas() {
		return etapasEsgotadas;
	}
	public void setEtapasEsgotadas(boolean etapasEsgotadas) {
		this.etapasEsgotadas = etapasEsgotadas;
	}

	public boolean isDesconexao() {
		return desconexao;
	}
	public void setDesconexao(boolean desconexao) {
		this.desconexao = desconexao;
	}

	public boolean isDataInicialEncontrada() {
		return dataInicialEncontrada;
	}
	public void setDataInicialEncontrada(boolean dataInicialEncontrada) {
		this.dataInicialEncontrada = dataInicialEncontrada;
	}

	public boolean isLogEsgotado() {
		return logEsgotado;
	}
	public void setLogEsgotado(boolean logEsgotado) {
		this.logEsgotado = logEsgotado;
	}

	public boolean isAniEncontrado() {
		return aNIEncontrado;
	}
	public void setAniEncontrado(boolean aniEncontrado) {
		this.aNIEncontrado = aniEncontrado;
	}

	public boolean isIdSessaoEncontrada() {
		return idSessaoEncontrada;
	}
	public void setIdSessaoEncontrada(boolean idSessaoEncontrada) {
		this.idSessaoEncontrada = idSessaoEncontrada;
	}

	public boolean isEvidenciaEncontrada() {
		return evidenciaEncontrada;
	}
	public void setEvidenciaEncontrada(boolean evidenciaEncontrada) {
		this.evidenciaEncontrada = evidenciaEncontrada;
	}

//	public boolean isRelatorioEscrito() {
//		return relatorioEscrito;
//	}
//	public void setRelatorioEscrito(boolean relatorioEscrito) {
//		this.relatorioEscrito = relatorioEscrito;
//	}

	public boolean isOpcaoValida() {
		return opcaoValida;
	}
	public void setOpcaoValida(boolean opcaoValida) {
		this.opcaoValida = opcaoValida;
	}

	public boolean isDigitoEncontrado() {
		return digitoEncontrado;
	}
	public void setDigitoEncontrado(boolean digitoEncontrado) {
		this.digitoEncontrado = digitoEncontrado;
	}

	public boolean isExpressaoEncontrada() {
		return expressaoEncontrada;
	}
	public void setExpressaoEncontrada(boolean expressaoEncontrada) {
		this.expressaoEncontrada = expressaoEncontrada;
	}
	
	public boolean isDossieEsgotado() {
		return dossieEsgotado;
	}
	public void setDossieEsgotado(boolean dossieEsgotado) {
		this.dossieEsgotado = dossieEsgotado;
	}

	public boolean isSegurancaDesligada() {
		return segurancaDesligada;
	}
	public void setSegurancaDesligada(boolean segurancaDesligada) {
		this.segurancaDesligada = segurancaDesligada;
	}

	public boolean isConexaoValida() {
		return conexaoValida;
	}
	public void setConexaoValida(boolean conexaoValida) {
		this.conexaoValida = conexaoValida;
	}
	
	public boolean isMenuDiferente() {
		return menuDiferente;
	}
	public void setMenuDiferente(boolean menuDiferente) {
		this.menuDiferente = menuDiferente;
	}

	public boolean isPrimeiraTentativa() {
		return primeiraTentativa;
	}
	public void setPrimeiraTentativa(boolean primeiraTentativa) {
		this.primeiraTentativa = primeiraTentativa;
	}

	public boolean isLinhaLog() {
		return linhaLog;
	}
	public void setLinhaLog(boolean linhaLog) {
		this.linhaLog = linhaLog;
	}

	public boolean isApuracaoEsgotada() {
		return apuracaoEsgotada;
	}
	public void setApuracaoEsgotada(boolean apuracaoEsgotada) {
		this.apuracaoEsgotada = apuracaoEsgotada;
	}
	
	public boolean isOpcaoFornecida() {
		return opcaoFornecida;
	}
	public void setOpcaoFornecida(boolean opcaoFornecida) {
		this.opcaoFornecida = opcaoFornecida;
	}

	public boolean isAvancoOcorrido() {
		return avancoOcorrido;
	}
	public void setAvancoOcorrido(boolean avancoOcorrido) {
		this.avancoOcorrido = avancoOcorrido;
	}

	public boolean isLinhaConstituicao() {
		return linhaConstituicao;
	}
	public void setLinhaConstituicao(boolean linhaConstituicao) {
		this.linhaConstituicao = linhaConstituicao;
	}

	public boolean isDataFinalEncontrada() {
		return dataFinalEncontrada;
	}
	public void setDataFinalEncontrada(boolean dataFinalEncontrada) {
		this.dataFinalEncontrada = dataFinalEncontrada;
	}

//	public boolean isDnisEncontrado() {
//		return dnisEncontrado;
//	}
//	public void setDnisEncontrado(boolean dnisEncontrado) {
//		this.dnisEncontrado = dnisEncontrado;
//	}

	public boolean isRepeticao() {
		return repeticao;
	}
	public void setRepeticao(boolean repeticao) {
		this.repeticao = repeticao;
	}

	public boolean isExcecaoEncontrada() {
		return excecaoEncontrada;
	}
	public void setExcecaoEncontrada(boolean excecaoEncontrada) {
		this.excecaoEncontrada = excecaoEncontrada;
	}

	public boolean isExcecaoReincidente() {
		return excecaoReincidente;
	}
	public void setExcecaoReincidente(boolean excecaoReincidente) {
		this.excecaoReincidente = excecaoReincidente;
	}
	
	public boolean isLinhaDeANI() {
		return linhaDeANI;
	}
	public void setLinhaDeANI(boolean linhaDeANI) {
		this.linhaDeANI = linhaDeANI;
	}

	public boolean isPosicaoMenuDiferente() {
		return posicaoMenuDiferente;
	}
	public void setPosicaoMenuDiferente(boolean posicaoMenuDiferente) {
		this.posicaoMenuDiferente = posicaoMenuDiferente;
	}

	public boolean isDigitoEmCadeia() {
		return digitoEmCadeia;
	}
	public void setDigitoEmCadeia(boolean digitoEmCadeia) {
		this.digitoEmCadeia = digitoEmCadeia;
	}

	public boolean isuUIEntradaEncontrado() {
		return uUIEntradaEncontrado;
	}
	public void setuUIEntradaEncontrado(boolean uUIEntradaEncontrado) {
		this.uUIEntradaEncontrado = uUIEntradaEncontrado;
	}

	public boolean isuUISaidaEncontrado() {
		return uUISaidaEncontrado;
	}
	public void setuUISaidaEncontrado(boolean uUISaidaEncontrado) {
		this.uUISaidaEncontrado = uUISaidaEncontrado;
	}

	public boolean isDestinoExterno() {
		return destinoExterno;
	}
	public void setDestinoExterno(boolean destinoExterno) {
		this.destinoExterno = destinoExterno;
	}

	public boolean isaNIEncontrado() {
		return aNIEncontrado;
	}
	public void setaNIEncontrado(boolean aNIEncontrado) {
		this.aNIEncontrado = aNIEncontrado;
	}

	public boolean isRetomar() {
		return retomar;
	}
	public void setRetomar(boolean retomar) {
		this.retomar = retomar;
	}

	public boolean isExecucao() {
		return execucao;
	}
	public void setExecucao(boolean execucao) {
		this.execucao = execucao;
	}

	public boolean isLimpeza() {
		return limpeza;
	}
	public void setLimpeza(boolean limpeza) {
		this.limpeza = limpeza;
	}

	public boolean isReportagem() {
		return reportagem;
	}
	public void setReportagem(boolean reportagem) {
		this.reportagem = reportagem;
	}

	public boolean isExportacao() {
		return exportacao;
	}
	public void setExportacao(boolean exportacao) {
		this.exportacao = exportacao;
	}

	public boolean isArquivoEscolhido() {
		return arquivoEscolhido;
	}
	public void setArquivoEscolhido(boolean arquivoEscolhido) {
		this.arquivoEscolhido = arquivoEscolhido;
	}
	
	public boolean isExportacaoHiperTexto() {
		return exportacaoHiperTexto;
	}
	public void setExportacaoHiperTexto(boolean exportacaoHiperTexto) {
		this.exportacaoHiperTexto = exportacaoHiperTexto;
	}

	public boolean isRelatorioArmazenado() {
		return relatorioArmazenado;
	}
	public void setRelatorioArmazenado(boolean relatorioArmazenado) {
		this.relatorioArmazenado = relatorioArmazenado;
	}
	
	public boolean isLimpezaConfirmada() {
		return limpezaConfirmada;
	}
	public void setLimpezaConfirmada(boolean limpezaConfirmada) {
		this.limpezaConfirmada = limpezaConfirmada;
	}

	public boolean isSuspenso() {
		return suspenso;
	}
	public void setSuspenso(boolean suspenso) {
		this.suspenso = suspenso;
	}

	public boolean isInterrompido() {
		return interrompido;
	}
	public void setInterrompido(boolean interrompido) {
		this.interrompido = interrompido;
	}

	public ExecucaoBL getExecucaoBL() {
		return execucaoBL;
	}
	public void setExecucaoBL( ExecucaoBL execucaoBL) {
		this.execucaoBL = execucaoBL;
	}

	public ValidacaoBL getValidacaoBL() {
		return validacaoBL;
	}
	public void setValidacaoBL(ValidacaoBL validacaoBL) {
		this.validacaoBL = validacaoBL;
	}

	public ExportacaoBL getExportacaoBL() {
		return exportacaoBL;
	}
	public void setExportacaoBL(ExportacaoBL exportacaoBL) {
		this.exportacaoBL = exportacaoBL;
	}

	// Metodos
	public void identificarIntervencao(){
		setExecutar( IntervencaoVO.acionador != null && IntervencaoVO.acionador == ControladorVO.EXECUCAO);
		setAguardar( IntervencaoVO.acionador != null && IntervencaoVO.acionador == ControladorVO.AGUARDO);
		setRetomar( IntervencaoVO.acionador != null && IntervencaoVO.acionador == ControladorVO.RETORNO);
		setAbortar( IntervencaoVO.acionador != null && IntervencaoVO.acionador == ControladorVO.ABORTO);
		setExecucao( IntervencaoVO.acionador != null && IntervencaoVO.acionador == ControladorVO.EXECUTAR);
		setReportagem( IntervencaoVO.acionador != null && IntervencaoVO.acionador == ControladorVO.VALIDAR);
		setExportacao( IntervencaoVO.acionador != null && IntervencaoVO.acionador == ControladorVO.EXPORTAR);
		setLimpeza( IntervencaoVO.acionador != null && IntervencaoVO.acionador == ControladorVO.LIMPAR);
		setOcioso( IntervencaoVO.acionador != null && 
			( ExecucaoVO.estagio == null || ExecucaoVO.estagio == ConstituicaoVO.SUSPENSO || ExecucaoVO.estagio == ConstituicaoVO.INTERROMPIDO)
		);
		setAtivo( IntervencaoVO.acionador != null && ExecucaoVO.estagio == ConstituicaoVO.INICIADO);
		setSuspenso( IntervencaoVO.acionador != null && ExecucaoVO.estagio == ConstituicaoVO.SUSPENSO);
		setInterrompido( IntervencaoVO.acionador != null && ExecucaoVO.estagio == ConstituicaoVO.INTERROMPIDO);
		System.out.println( "IntervencaoVO.acionador: "+ IntervencaoVO.acionador +", ExecucaoVO.estagio: "+ ExecucaoVO.estagio);
	}

	// NOVO INTERPRETAR INTERMEDIACAO
	public void interpretarIntermediacao(){
		setServicoEncontrado( IntermediacaoVO.nomeServico != null && IntermediacaoVO.nomeServico.equalsIgnoreCase( ConstituicaoVO.ctiLink));
		setPortfolioEsgotado( IntermediacaoVO.numeroServico >= IntermediacaoVO.servicos);
		setAparelhoEncontrado( IntegracaoTelefonicaVO.origem != null && ConfiguracaoVO.extensao.equals( IntegracaoTelefonicaVO.origem.getName()));
		setAparelhagemEsgotada( IntermediacaoVO.numeroAparelho >= IntermediacaoVO.aparelhos);
		setLigacaoEsgotada( IntegracaoTelefonicaVO.numeroLigado >= IntermediacaoVO.ligacoes);
		setAlertasEsgotados( IntermediacaoVO.numeroAlerta >= ConfiguracaoVO.limiteTentativas);
		setDestinoEncontrado( IntegracaoTelefonicaVO.numeroDestino != null && IntegracaoTelefonicaVO.numeroDestino != ConfiguracaoVO.extensao);
		setSegurancaDesligada( IntegracaoTelefonicaVO.aparelhagem == null);
		setLigacaoValida( IntermediacaoVO.ligacoes > 1);
		setOrigemEncontrada( IntegracaoTelefonicaVO.numeroOrigem != null && IntegracaoTelefonicaVO.numeroOrigem == ConfiguracaoVO.extensao);
		setLigacaoAtendida( IntegracaoTelefonicaVO.ligador != null && IntegracaoTelefonicaVO.ligador.getState() == Connection.CONNECTED);
		setVinculacaoValida( IntegracaoTelefonicaVO.vinculacao != null);
		setdTMFValido( 
				IntegracaoTelefonicaVO.dTMF != null && (
				( Integer.valueOf( IntegracaoTelefonicaVO.dTMF) > -1 && Integer.valueOf(IntegracaoTelefonicaVO.dTMF) < 10 )
				|| IntegracaoTelefonicaVO.dTMF == "*" 
				|| IntegracaoTelefonicaVO.dTMF == "#"
			)

		);
		setDestinoExterno( ConfiguracaoVO.destino != null && ConfiguracaoVO.destino.length()>9);
	}

	// NOVO INTERPRETAR EXECUCAO
	public void interpretarExecucao(){
		setTesteConcluido( ExecucaoVO.teste >= ExecucaoVO.carga);
		setProcedimentoConcluido( !( ExecucaoVO.indicador < ExecucaoVO.encadeados) || isDigito() || isDesconexao());
		setEtapasEsgotadas( ExecucaoVO.etapa >= ExecucaoVO.passos);
		setLinhaConstituicao( ExecucaoVO.linha != null && ExecucaoVO.linha.indexOf("CONSTITUICAO")!=-1);
		setDigito( ExecucaoVO.complexidade == 1);
		setCadeia( ExecucaoVO.complexidade > 1);
		setDesconexao( ExecucaoVO.procedimento!= null && ConstituicaoVO.chaveAbandono != null && ExecucaoVO.procedimento.equalsIgnoreCase( ConstituicaoVO.chaveAbandono));
		setSuspenso( IntervencaoVO.acionador != null && ExecucaoVO.estagio == ConstituicaoVO.SUSPENSO);
		setInterrompido( IntervencaoVO.acionador != null && ExecucaoVO.estagio == ConstituicaoVO.INTERROMPIDO);
	}

	public void interpretarConexao(){
		setConexaoValida( ConexaoVO.canal != null);
	}

	public void interpretarValidacao(){
		setExpressaoEncontrada( ValidacaoVO.posicao > -1);
		setApuracaoEsgotada( ( ValidacaoVO.teste + ExecucaoVO.pulos) >= ExecucaoVO.carga);
		setDossieEsgotado( isExpressaoEncontrada() && ValidacaoVO.posicao < ValidacaoVO.ultimaPosicao);
		setAvancoOcorrido( ValidacaoVO.posicao > ValidacaoVO.ultimaPosicao);
		setDigitoEmCadeia( ValidacaoVO.digito != null && ValidacaoVO.digito.length()>1);
		setLinhaLog( ControleMaterialVO.linha != null && ValidacaoVO.momentoInicial != null && ControleMaterialVO.linha.length() > 5 && ControleMaterialVO.linha.substring( 0, 5).equals( ValidacaoVO.momentoInicial.substring( 0, 5)));
		setDataInicialEncontrada( ControleMaterialVO.horario != null && ValidacaoVO.horario != null && ControleMaterialVO.horario.compareTo( ValidacaoVO.horario)>=0);
		setLogEsgotado( ControleMaterialVO.linha == null);
		setLinhaDeANI( ControleMaterialVO.linha != null && ControleMaterialVO.linha.indexOf( ConstituicaoVO.chaveANI)>-1);
		setAniEncontrado( ControleMaterialVO.linha != null && isLinhaDeANI() && ControleMaterialVO.linha.indexOf( IntegracaoTelefonicaVO.numeroOrigem)>-1);
		// setIdSessaoEncontrada( ValidacaoVO.identificadorSessao != null);
		setIdSessaoEncontrada( ControleMaterialVO.linha != null && ValidacaoVO.identificadorSessao != null && ControleMaterialVO.linha.indexOf( ValidacaoVO.identificadorSessao)>-1);
		setDataFinalEncontrada( ControleMaterialVO.horario != null && ControleMaterialVO.horario.compareTo( ValidacaoVO.horario)>=0);
		setEvidenciaEncontrada( ControleMaterialVO.linha != null && ConfiguracaoVO.evidenciador != null && ControleMaterialVO.linha.indexOf( ConfiguracaoVO.evidenciador)>-1);
		setOpcaoFornecida( ValidacaoVO.validador != null && ValidacaoVO.validador.equalsIgnoreCase("0"));
		setOpcaoValida( ValidacaoVO.validador != null && ValidacaoVO.validador.equalsIgnoreCase("0"));
		setPrimeiraTentativa( ValidacaoVO.ultimoMenu == null);
		setExcecaoEncontrada( ReportagemVO.mensagemExcecao != null);
		setExcecaoReincidente( ReportagemVO.delato != null);
		setDigitoEncontrado( ValidacaoVO.digito != null);
		setMenuDiferente( ValidacaoVO.menu != null && ValidacaoVO.ultimoMenu != null && !ValidacaoVO.menu.equalsIgnoreCase( ValidacaoVO.ultimoMenu));
		setPosicaoMenuDiferente( ValidacaoVO.posicaoMenu != ValidacaoVO.posicaoUltimoMenu );
		setRepeticao( ValidacaoVO.digito != null);
		setuUIEntradaEncontrado( ReportagemVO.uUIEntrada != null && ReportagemVO.uUIEntrada.length()>0);
		setuUISaidaEncontrado( ReportagemVO.uUISaida != null && ReportagemVO.uUISaida.length()>0);
	}
	
	public void interpretarReportagem(){
		setPrimeiraTentativa( ValidacaoVO.ultimoMenu == null);
		setMenuDiferente( ValidacaoVO.menu != null && ValidacaoVO.ultimoMenu != null && !ValidacaoVO.menu.equalsIgnoreCase( ValidacaoVO.ultimoMenu));
		setDigitoEmCadeia( ValidacaoVO.digito != null && ValidacaoVO.digito.length()>1);
		setRepeticao( ValidacaoVO.digito != null);
		setuUISaidaEncontrado( ReportagemVO.uUISaida != null && ReportagemVO.uUISaida.length()>0);
		setExcecaoReincidente( ReportagemVO.delato != null);
	}
	
	public void interpretarExportacao(){
		setArquivoEscolhido( ExportacaoVO.resposta == JFileChooser.APPROVE_OPTION);
		setExportacaoHiperTexto( ConfiguracaoVO.extensaoExportacao.toUpperCase().contains("HT"));
		setRelatorioArmazenado( ControleMaterialVO.relatorioArquivado != null && ControleMaterialVO.relatorioArquivado.exists());
		setLimpezaConfirmada( ExportacaoVO.resposta == JOptionPane.OK_OPTION);
	}

	
//	public void encontrarDataFinal(){
//		setDataFinalEncontrada( ControleMaterialVO.horario != null && ControleMaterialVO.horario.compareTo( ValidacaoVO.horario)>=0);
//	}

//	public void avaliarRelatorio(){	}
//
//	public void avaliarHistorico(){	}

//	public void avaliarEvidencia(){
//		setOpcaoValida( ValidacaoVO.evidencia != null);
//	}

//	public void avaliarExcecao(){
//		setExcecaoEncontrada( ReportagemVO.mensagemExcecao != null);
//		setExcecaoReincidente( ReportagemVO.delato != null);
//	}
	
}

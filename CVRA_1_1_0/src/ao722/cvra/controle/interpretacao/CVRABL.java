package ao722.cvra.controle.interpretacao;

import ao722.cvra.controle.decisao.ConfiguracaoBL;
import ao722.cvra.controle.decisao.ConstituicaoBL;
import ao722.cvra.modelo.estado.CVRAVO;

public class CVRABL {

	// Propriedades
	private boolean simulacao, certificacao;
	private ConfiguracaoBL configuracaoBL;
	private ConstituicaoBL constituicaoBL;

	// Construtor
	public CVRABL() {	
		reconhecer();
		setConfiguracaoBL( new ConfiguracaoBL( this));
		setConstituicaoBL( new ConstituicaoBL( this));
	}
	// Getters and Setters
	public boolean isSimulacao() {
		return simulacao;
	}
	public void setSimulacao(boolean simulacao) {
		this.simulacao = simulacao;
	}

	public boolean isCertificacao() {
		return certificacao;
	}
	public void setCertificacao(boolean certificacao) {
		this.certificacao = certificacao;
	}

	public ConfiguracaoBL getConfiguracaoBL() {
		return configuracaoBL;
	}
	public void setConfiguracaoBL( ConfiguracaoBL configuracaoBL) {
		this.configuracaoBL = configuracaoBL;
	}

	public ConstituicaoBL getConstituicaoBL() {
		return constituicaoBL;
	}
	public void setConstituicaoBL( ConstituicaoBL constituicaoBL) {
		this.constituicaoBL = constituicaoBL;
	}

	// Metodos
	public void reconhecer(){
		setSimulacao( CVRAVO.subsistema != null && CVRAVO.subsistema.equals( CVRAVO.simulacao));
		setCertificacao( CVRAVO.subsistema != null && CVRAVO.subsistema.equals( CVRAVO.certificacao));
		System.out.println("SimuladorVO.subsistema: "+ CVRAVO.subsistema);
	}
	
	public void configurar(){
		reconhecer();
		setConfiguracaoBL( new ConfiguracaoBL( this));
	}

	public void constituir(){
		reconhecer();
		setConstituicaoBL( new ConstituicaoBL( this));
	}

}

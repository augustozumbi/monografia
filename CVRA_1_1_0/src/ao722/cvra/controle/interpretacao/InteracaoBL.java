package ao722.cvra.controle.interpretacao;

import ao722.cvra.controle.decisao.MensagemBL;
import ao722.cvra.controle.decisao.NavegacaoBL;
import ao722.cvra.modelo.estado.ConfiguracaoVO;
import ao722.cvra.modelo.estado.ConstituicaoVO;
import ao722.cvra.modelo.estado.InteracaoVO;
import ao722.cvra.modelo.estado.LocalizacaoVO;
import ao722.cvra.modelo.estado.MensagemVO;
import ao722.cvra.modelo.estado.NavegacaoVO;
import ao722.cvra.modelo.estado.TecladoTelefonicoVO;

public class InteracaoBL {

	// Propriedades
	private boolean possivel; // Validacoes
	private boolean iniciar, chamar, desligar, navegar; // Acoes
	private boolean entrada, excedido, transferido, abandonou, desconectado; // Situacoes
	private boolean desacerto, valida, insucesso;
	private boolean repeticao, retorno, transferencia, desconexao;
	private boolean navegacao, desfecho; // Reacoes
	private boolean responsavel, verbalizavel, demonstravel; // Respostas 
	private NavegacaoBL navegacaoBL;
	private MensagemBL mensagemBL;

	// Construtores
	public InteracaoBL( ) {
		identificarInteracao( );
		identificarSituacao( );
		setNavegacaoBL( new NavegacaoBL( this));
		setMensagemBL( new MensagemBL( this));
	}

	// Getters and Setters
	public boolean isChamar() {
		return chamar;
	}
	public void setChamar(boolean chamar) {
		this.chamar = chamar;
	}

	public boolean isDesligar() {
		return desligar;
	}
	public void setDesligar(boolean desligar) {
		this.desligar = desligar;
	}

	public boolean isNavegar() {
		return navegar;
	}
	public void setNavegar(boolean navegar) {
		this.navegar = navegar;
	}

	public boolean isPossivel() {
		return possivel;
	}
	public void setPossivel(boolean possivel) {
		this.possivel = possivel;
	}

	public boolean isEntrada() {
		return entrada;
	}
	public void setEntrada(boolean entrada) {
		this.entrada = entrada;
	}

	public boolean isIniciar() {
		return iniciar;
	}
	public void setIniciar(boolean iniciar) {
		this.iniciar = iniciar;
	}

	public boolean isTransferido() {
		return transferido;
	}
	public void setTransferido(boolean transferido) {
		this.transferido = transferido;
	}

	public boolean isDesconectado() {
		return desconectado;
	}
	public void setDesconectado(boolean desconectado) {
		this.desconectado = desconectado;
	}

	public boolean isExcedido() {
		return excedido;
	}
	public void setExcedido(boolean excedido) {
		this.excedido = excedido;
	}

	public boolean isAbandonou() {
		return abandonou;
	}
	public void setAbandonou(boolean abandonou) {
		this.abandonou = abandonou;
	}

	public boolean isDesacerto() {
		return desacerto;
	}
	public void setDesacerto(boolean desacerto) {
		this.desacerto = desacerto;
	}

	public boolean isRepeticao() {
		return repeticao;
	}
	public void setRepeticao(boolean repeticao) {
		this.repeticao = repeticao;
	}

	public boolean isTransferencia() {
		return transferencia;
	}
	public void setTransferencia(boolean transferencia) {
		this.transferencia = transferencia;
	}

	public boolean isDesconexao() {
		return desconexao;
	}
	public void setDesconexao(boolean desconexao) {
		this.desconexao = desconexao;
	}

	public boolean isRetorno() {
		return retorno;
	}
	public void setRetorno(boolean retorno) {
		this.retorno = retorno;
	}

	public boolean isNavegacao() {
		return navegacao;
	}
	public void setNavegacao(boolean navegacao) {
		this.navegacao = navegacao;
	}

	public boolean isDesfecho() {
		return desfecho;
	}
	public void setDesfecho(boolean desfecho) {
		this.desfecho = desfecho;
	}

	public boolean isInsucesso() {
		return insucesso;
	}
	public void setInsucesso(boolean insucesso) {
		this.insucesso = insucesso;
	}

	public boolean isValida() {
		return valida;
	}
	public void setValida(boolean valida) {
		this.valida = valida;
	}

	public boolean isVerbalizavel() {
		return verbalizavel;
	}
	public void setVerbalizavel(boolean verbalizavel) {
		this.verbalizavel = verbalizavel;
	}

	public boolean isDemonstravel() {
		return demonstravel;
	}
	public void setDemonstravel(boolean demonstravel) {
		this.demonstravel = demonstravel;
	}

	public boolean isResponsavel() {
		return responsavel;
	}
	public void setResponsavel(boolean responsavel) {
		this.responsavel = responsavel;
	}
	
	public NavegacaoBL getNavegacaoBL() {
		return navegacaoBL;
	}
	public void setNavegacaoBL(NavegacaoBL navegacaoBL) {
		this.navegacaoBL = navegacaoBL;
	}

	public MensagemBL getMensagemBL() {
		return mensagemBL;
	}
	public void setMensagemBL(MensagemBL mensagemBL) {
		this.mensagemBL = mensagemBL;
	}

	// Metodos
	public void identificarInteracao( ){
		setIniciar( InteracaoVO.acionador != null && InteracaoVO.acionador.equalsIgnoreCase( ConstituicaoVO.saudacao));
		setChamar( InteracaoVO.acionador != null && InteracaoVO.acionador.equalsIgnoreCase( TecladoTelefonicoVO.CHAMAR));
		setDesligar( InteracaoVO.acionador != null && InteracaoVO.acionador.equalsIgnoreCase( TecladoTelefonicoVO.DESLIGAR));
		setNavegar( !isChamar() && !isDesligar() && !isIniciar());
	}

	public void identificarSituacao( ){
		setEntrada( InteracaoVO.acionador == null);
		setExcedido( LocalizacaoVO.trajetoria != null && LocalizacaoVO.trajetoria.indexOf( ConstituicaoVO.excedido)>-1);
		setTransferido( LocalizacaoVO.trajetoria != null && LocalizacaoVO.trajetoria.indexOf( ConstituicaoVO.transferido)>-1);
		setAbandonou( LocalizacaoVO.trajetoria != null && LocalizacaoVO.trajetoria.indexOf( ConstituicaoVO.abandonou)>-1);
		setDesconectado( LocalizacaoVO.trajetoria != null && LocalizacaoVO.trajetoria.indexOf( ConstituicaoVO.desconectado)>1);
		setPossivel( 
			isIniciar() &&
			(
				!isEntrada() &&
				!isExcedido() &&
				!isTransferido() &&
				!isAbandonou() &&
				!isDesconectado()
			) ||
			isChamar() && 
			(
				isExcedido() ||
				isTransferido() ||
				isAbandonou() ||
				isDesconectado() ||
				LocalizacaoVO.trajetoria == null
			) ||
			isDesligar() && 
			(
				!isExcedido() &&
				!isTransferido() &&
				!isAbandonou() &&
				!isDesconectado() &&
				LocalizacaoVO.trajetoria != null
			) ||
			isNavegar() &&
			(
				!isExcedido() &&
				!isTransferido() &&
				!isAbandonou() &&
				!isDesconectado() &&
				LocalizacaoVO.trajetoria != null
			)

		);

	}

	public void identificarNavegacao( ){
		setDesacerto( NavegacaoVO.destino.equalsIgnoreCase( ConstituicaoVO.desacerto));
		setRepeticao( !isDesacerto() && NavegacaoVO.destino.split(";")[0].equalsIgnoreCase( ConstituicaoVO.repeticao));
		setRetorno( !isDesacerto() && NavegacaoVO.destino.split(";")[0].equalsIgnoreCase( ConstituicaoVO.retorno));
		setTransferencia( !isDesacerto() && NavegacaoVO.destino.split(";")[0].equalsIgnoreCase( ConstituicaoVO.transferencia));
		setDesconexao( !isDesacerto() && NavegacaoVO.destino.split(";")[0].equalsIgnoreCase( ConstituicaoVO.desconexao));
		setNavegacao( !isDesacerto() && !isRepeticao() && !isRetorno() && !isTransferencia() && !isDesconexao());
		setDesfecho( isTransferencia() || isDesconexao());
		setValida( !isDesacerto());
	}

	public void identificarInsucesso(){
		setInsucesso( NavegacaoVO.tentativas >= ConfiguracaoVO.limiteTentativas);
	}

	public void identificarMensagem( ){
		setResponsavel( MensagemVO.frase != null);
		setVerbalizavel( MensagemVO.frase != null && MensagemVO.frase.indexOf(".wav")>0);
		setDemonstravel( MensagemVO.frase != null && MensagemVO.frase.indexOf(".wav")!=0);
	}
	
}

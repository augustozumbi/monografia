package ao722.cvra.controle.interpretacao;

import ao722.cvra.modelo.estado.ConstituicaoVO;

public class TecladoTelefonicoBL {
	
	// Propriedades
	private boolean extendido;
	private boolean lateralAtingida;
	private boolean existemComandos;
	private boolean alargamentoNecessario;
	private boolean baseAtingida;

	// Construtores
	public TecladoTelefonicoBL() {
		setExistemComandos( ConstituicaoVO.comandos != null && ConstituicaoVO.comandos.length()>0);
		setExtendido( isExistemComandos());
	}
	// Getters and Setters
	public boolean isExtendido() {
		return extendido;
	}
	public void setExtendido(boolean extendido) {
		this.extendido = extendido;
	}
	
	public boolean isLateralAtingida() {
		return lateralAtingida;
	}
	public void setLateralAtingida(boolean lateralAtingida) {
		this.lateralAtingida = lateralAtingida;
	}

	public boolean isExistemComandos() {
		return existemComandos;
	}
	public void setExistemComandos(boolean existemComandos) {
		this.existemComandos = existemComandos;
	}

	public boolean isAlargamentoNecessario() {
		return alargamentoNecessario;
	}
	public void setAlargamentoNecessario(boolean alargamentoNecessario) {
		this.alargamentoNecessario = alargamentoNecessario;
	}

	public boolean isBaseAtingida() {
		return baseAtingida;
	}
	public void setBaseAtingida(boolean baseAtingida) {
		this.baseAtingida = baseAtingida;
	}

}
